import { Component, Input, OnInit, Inject } from '@angular/core';
import { NB_WINDOW, NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserData } from '../../../@core/data/users';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';
import { Router } from '@angular/router';

import { filter, map } from 'rxjs/operators';
import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';

@ Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @ Input() position = 'normal';

  user = {};

  userMenu = [{ title: 'Profile' }, { title: 'Logout' }];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserData,
              private analyticsService: AnalyticsService,
              private layoutService: LayoutService,
              private router: Router,
              private authService: NbAuthService,
              @ Inject(NB_WINDOW) private window

  ) {


    this .authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {        
        if (token.isValid()) {
          this .user = token.getPayload();
        }

      });
  }

  ngOnInit() {
    // this .userService.getUsers()
    //  .subscribe((users: any) => this .user = users.abdul);

    this .menuService.onItemClick().subscribe((event) => {
      this .onItemSelection(event.item.title);
    })

  }

  onItemSelection(title) {
    if (title === 'Logout') {
      this .router.navigate(['/auth/logout']);
    } else if (title === 'Profile') {
     // go to profile
    }
  }

  toggleSidebar(): boolean {
    this .sidebarService.toggle(true, 'menu-sidebar');
    this .layoutService.changeLayoutSize();

    return false;
  }

  goToHome() {
    this .menuService.navigateHome();
  }

  startSearch() {
    this .analyticsService.trackEvent('startSearch');
  }


}
