import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Developed by <b><a href="https://voxcinemas.com" target="_blank">VOX IT</a></b> </span>
  `,
})
export class FooterComponent {
}
