import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NbAuthToken, NbAuthService } from '@nebular/auth';
import { HttpClient, HttpClientModule, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'ngx-form-layouts',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss'],
})


export class ViewUserComponent implements OnInit {

  updateUserForm: FormGroup;
  nameOfUser: FormControl;
  username: FormControl;
  merchantId: FormControl;
  
  loadingSave = false;
  isAlert = false;
  alertMessage = '';
  alertStatus = '';

    merchants: any[];
    merchantSearchList: any[];
    merchantListTmp: any[];
    merchantsearch: string = '';
    merchantUser: any;

  constructor(private http: HttpClient, private fb: FormBuilder, private router: Router, private r: ActivatedRoute
    , private authService: NbAuthService) {


      this.loadMerchants();
    
   
  }

  ngOnInit() {

      this.nameOfUser = this.fb.control('',
          [Validators.required, Validators.pattern('^[a-zA-Z0-9_ ]*$')]);
      this.username = this.fb.control('', [Validators.required, Validators.email]);
      this.merchantId = this.fb.control('', [Validators.required, Validators.pattern('^[1-9][0-9]*$')]);

    this.setForm();

  }

  private setForm() {

      this.updateUserForm = this.fb.group({
          nameOfUser: this.nameOfUser,
          username: this.username,
          merchantId: this.merchantId
    });

  }

    private loadUser() {

    let id = this.r.snapshot.paramMap.get('id');

    console.log(id)

    let token;
    this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
      token = tokenData.getValue();
    });

    let headers = new HttpHeaders().set("Authorization", "Bearer " +
      token).set("Content-Type", "application/json");

    this.http.get('api/user/get?id='+id, { headers: headers }).subscribe((res:any) => {

      //this.source.load(res);

     

        this.merchantUser = res;

        this.merchantListTmp = res.merchantList;

        this.nameOfUser = this.fb.control(res.nameOfUser,
                       [Validators.required, Validators.pattern('^[a-zA-Z0-9_ ]*$')]);
        this.username = this.fb.control(res.username, [Validators.required, Validators.email]);
        this.merchantId = this.fb.control(res.merchantId, [Validators.required, Validators.pattern('^[0-9]+$')]);
    

      this.setForm();

    },
      error => {
        console.log('api/user/get', error)
        if (error.status === 401) {
          this.router.navigate(['/auth/login']);
        }
      });
  }

  onUpdateUserSubmit() {

      this.updateUserForm.markAsDirty();
      if (this.updateUserForm.valid) {

          this.loadingSave = true;
         
          console.log(JSON.stringify(this.updateUserForm.value));


      let token;
      this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
        token = tokenData.getValue();
      });

      let headers = new HttpHeaders().set("Authorization", "Bearer " +
        token).set("Content-Type", "application/json");

      let id = this.r.snapshot.paramMap.get('id');

          var postObject = {
              "nameOfUser": this.nameOfUser.value, "username": this.username.value, "merchantId": this.merchantId.value,
              merchantList: this.merchantListTmp
          }

          this.http.put('api/user/update?id=' + id, postObject ,

        { headers: headers }).subscribe((res:any) => {

          if (res.actionStatus === 'SUCCESS') {

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'success';
            this.alertMessage = "Record updated successfully!";
            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';
            }, 3000);

           // this.router.navigate(['/pages/partners/partner-list']);
          }
          else {

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'danger';
            this.alertMessage = "Could not update record!";

            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';

            }, 3000);
          }

        },
          error => {
            console.log('api/user/update', error);

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'danger';
            this.alertMessage = error.statusText;
            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';

              if (error.status === 401) {
                this.router.navigate(['/auth/login']);
              }

            }, 3000);
          });

    }
    else {
      this.isAlert = true;
      this.alertStatus = 'danger';
      this.alertMessage = "The form is not valid!";
      setTimeout(() => {
        this.isAlert = false;
        this.alertMessage = '';
      }, 3000);
    }
  }


  private loadMerchants() {

        // console.log(JSON.stringify(this.authService.getToken()));

        let token;
        this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
            token = tokenData.getValue();
        });

        // let token = this.authService.getToken().value.token;

        let headers = new HttpHeaders().set("Authorization", "Bearer " +
            token).set("Content-Type", "application/json");

        this.http.get('api/merchant/get-list', { headers: headers }).subscribe((res: any[]) => {

           

            this.merchants = res;

            console.log(JSON.stringify(this.merchants));

            this.loadUser();

        },
            error => {
                console.log('api/merchant/get-list', error)
                if (error.status === 401) {
                    this.router.navigate(['/auth/login']);
                }
            });
    }

  merchantSearch() {

        
      if (this.merchantsearch.length == 0) {
          this.merchantSearchList = [];
      }
      else {

           

          var searchM = this.merchantsearch;
          this.merchantSearchList = this.merchants.filter(function (obj) {
              return obj.merchantName.toLowerCase().includes(searchM.toLowerCase());
          });

      }

       

  }

  selectMerchant(id) {

      var dupList = this.merchantListTmp.filter(function (obj) {
          return obj.id==id;
      });

      if (dupList.length == 0) {
          var searchRes = this.merchants.filter(function (obj) {
              return obj.id == id;
          });

          this.merchantListTmp.push(searchRes[0]);
      }
               

      this.merchantsearch = '';
      this.merchantSearchList = [];
  }

  removeMerchant(id) {

       
      
      this.merchantListTmp = this.merchantListTmp.filter(function (obj) {
          return obj.id != id;
      });

       

      this.merchantsearch = '';
      this.merchantSearchList = [];
  }
}
