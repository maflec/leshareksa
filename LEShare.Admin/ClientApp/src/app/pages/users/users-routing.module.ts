import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { UserListComponent } from './user-list/user-list.component';
import { NewUserComponent } from './new-user/new-user.component';
import { ViewUserComponent } from './view-user/view-user.component';

const routes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [{
    path: 'user-list',
    component: UserListComponent,
  }, {
      path: 'new-user',
      component: NewUserComponent,
    }, {
      path: 'view-user/:id',
      component: ViewUserComponent,
    }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule { }

export const routedComponents = [
  UsersComponent,
  UserListComponent,
  NewUserComponent,
  ViewUserComponent,
];
