import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { UsersRoutingModule, routedComponents } from './users-routing.module';
import { NbCardModule } from '@nebular/theme';


@NgModule({
  imports: [
    ThemeModule,
    UsersRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class UsersModule { }
