import { Component, ViewEncapsulation } from '@angular/core';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { NbAuthService, NbAuthJWTToken, NbAuthToken } from '@nebular/auth';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-smart-table',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class UserListComponent {

  settings = {
    actions: {
      add: false,
      edit:false,
      delete: false
    },
    columns: {
      id: {
        title: '#',
        type: 'number'
      },
      nameOfUser: {
        title: 'Name',
        type: 'html',
      },
      username: {
        title: 'Username',
        type: 'string',
      },
        defaultMerchant: {
        title: 'Default Merchant',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private http: HttpClient, private authService: NbAuthService, private router: Router) {

      this.loadUsers();

  }

  private loadUsers() {
      
   // console.log(JSON.stringify(this.authService.getToken()));

    let token;
    this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
      token = tokenData.getValue();
    });

   // let token = this.authService.getToken().value.token;

    let headers = new HttpHeaders().set("Authorization", "Bearer " +
      token).set("Content-Type", "application/json");

    this.http.get('api/user/get-list', { headers: headers }).subscribe((res: any[])=> {

      //this.source.load(res);

      

      // var result = res as partner[];
      var temp = new Array();
      for (let i = 0; i < res.length; i++) {
        var obj = {
          id: i+1,
          nameOfUser: '<a href="#/pages/users/view-user/' + res[i].id + '">' + res[i].nameOfUser+'</a>',
            username: res[i].username,
            defaultMerchant: res[i].defaultMerchant
         }
        temp.push(obj);
      }
      this.source = new LocalDataSource(temp); 

    },
      error => {
        console.log('api/user/get-list', error)
        if (error.status === 401) {
          this.router.navigate(['/auth/login']);
        }
      });
  }
}



