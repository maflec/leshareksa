import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MerchantsComponent } from './merchants.component';
import { MerchantListComponent } from './merchant-list/merchant-list.component';
import { NewMerchantComponent } from './new-merchant/new-merchant.component';
import { ViewMerchantComponent } from './view-merchant/view-merchant.component';

const routes: Routes = [{
  path: '',
  component: MerchantsComponent,
  children: [{
      path: 'merchant-list',
      component: MerchantListComponent,
  }, {
          path: 'new-merchant',
          component: NewMerchantComponent,
    }, {
      path: 'view-merchant/:id',
          component: ViewMerchantComponent,
    }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MerchantsRoutingModule { }

export const routedComponents = [
  MerchantsComponent,
  MerchantListComponent,
  NewMerchantComponent,
  ViewMerchantComponent,
];
