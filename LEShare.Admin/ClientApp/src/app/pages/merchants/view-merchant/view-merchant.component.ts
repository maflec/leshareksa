import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NbAuthToken, NbAuthService } from '@nebular/auth';
import { HttpClient, HttpClientModule, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'ngx-form-layouts',
    templateUrl: './view-merchant.component.html',
    styleUrls: ['./view-merchant.component.scss'],
})


export class ViewMerchantComponent implements OnInit {

  updateMerchantForm: FormGroup;
  merchantName: FormControl;
  email: FormControl;
  phone: FormControl;
  address: FormControl;
  currency: FormControl;
    cyb_transactionKey: FormControl;
    cyb_mid: FormControl;
  baseUrl: FormControl;
  bccEmailList: FormControl;

  loadingSave = false;
  isAlert = false;
  alertMessage = '';
  alertStatus = '';


  constructor(private http: HttpClient, private fb: FormBuilder, private router: Router, private r: ActivatedRoute
    , private authService: NbAuthService) {

      this.loadMerchant();
   
  }

  ngOnInit() {

      this.merchantName = this.fb.control('',
      [Validators.required, Validators.pattern('^[a-zA-Z0-9_ ]*$')]);
      this.email = this.fb.control('', [Validators.required, Validators.email]);
      this.phone = this.fb.control('', [Validators.required, Validators.pattern('^[0-9]+$')]);
      this.address = this.fb.control('');
      this.currency = this.fb.control('');
      this.cyb_transactionKey = this.fb.control('');
      this.cyb_mid = this.fb.control('');
      this.baseUrl = this.fb.control('');
      this.bccEmailList = this.fb.control('');

    this.setForm();

  }

  private setForm() {

      this.updateMerchantForm = this.fb.group({
          merchantName: this.merchantName,
          email: this.email,
          phone: this.phone,
          address: this.address,
          currency: this.currency,
          cyb_transactionKey: this.cyb_transactionKey,
          cyb_mid: this.cyb_mid,
          baseUrl: this.baseUrl,
          bccEmailList: this.bccEmailList
    });

  }

  private loadMerchant() {

    let id = this.r.snapshot.paramMap.get('id');

   

    let token;
    this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
      token = tokenData.getValue();
    });

    let headers = new HttpHeaders().set("Authorization", "Bearer " +
      token).set("Content-Type", "application/json");

    this.http.get('api/merchant/get?id='+id, { headers: headers }).subscribe((res:any) => {

        this.merchantName = this.fb.control(res.merchantName,
                       [Validators.required, Validators.pattern('^[a-zA-Z0-9_ ]*$')]);
        this.email = this.fb.control(res.email, [Validators.required, Validators.email]);
        this.phone = this.fb.control(res.phone, [Validators.required, Validators.pattern('^[0-9]+$')]);
        this.address = this.fb.control(res.address);

        this.currency = this.fb.control(res.currency);
        this.cyb_mid = this.fb.control(res.cyb_mid);
        this.cyb_transactionKey = this.fb.control(res.cyb_transactionKey);
        this.baseUrl = this.fb.control(res.baseUrl);
        this.bccEmailList = this.fb.control(res.bccEmailList);

      this.setForm();

    },
      error => {
        console.log('api/partner/get', error)
        if (error.status === 401) {
          this.router.navigate(['/auth/login']);
        }
      });
  }

  onUpdateMerchantSubmit() {

    this.updateMerchantForm.markAsDirty();
      if (this.updateMerchantForm.valid) {

      this.loadingSave = true;

          console.log(JSON.stringify(this.updateMerchantForm.value));


      let token;
      this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
        token = tokenData.getValue();
      });

      let headers = new HttpHeaders().set("Authorization", "Bearer " +
        token).set("Content-Type", "application/json");

      let id = this.r.snapshot.paramMap.get('id');

          this.http.put('api/merchant/update?id=' + id, this.updateMerchantForm.value,
        { headers: headers }).subscribe((res:any) => {

          if (res.actionStatus === 'SUCCESS') {

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'success';
            this.alertMessage = "Record updated successfully!";
            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';
            }, 3000);

          
          }
          else {

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'danger';
            this.alertMessage = "Could not update record!";

            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';

            }, 3000);
          }

        },
          error => {
            console.log('api/merchant/update', error);

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'danger';
            this.alertMessage = error.statusText;
            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';

              if (error.status === 401) {
                this.router.navigate(['/auth/login']);
              }

            }, 3000);
          });

    }
    else {
      this.isAlert = true;
      this.alertStatus = 'danger';
      this.alertMessage = "The form is not valid!";
      setTimeout(() => {
        this.isAlert = false;
        this.alertMessage = '';
      }, 3000);
    }
  }


}
