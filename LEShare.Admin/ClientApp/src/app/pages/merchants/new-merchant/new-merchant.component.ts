import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NbAuthService, NbAuthToken } from '@nebular/auth';
import { HttpClient, HttpClientModule, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'ngx-form-layouts',
    templateUrl: './new-merchant.component.html',
    styleUrls: ['./new-merchant.component.scss'],
})


export class NewMerchantComponent implements OnInit {

    updateMerchantForm: FormGroup;
    merchantName: FormControl;
    email: FormControl;
    phone: FormControl;
    address: FormControl;
    currency: FormControl;
    cyb_transactionKey: FormControl;
    cyb_mid: FormControl;
    baseUrl: FormControl;
    bccEmailList: FormControl;

  loadingSave = false;
  isAlert = false;  
  alertMessage = '';
  alertStatus = '';
  
  
  constructor(private http: HttpClient,private fb: FormBuilder, private router: Router,
    private r: ActivatedRoute, private authService: NbAuthService) {
    
    //this.partners = this.service.getData();
  }

    ngOnInit() {

      this.merchantName = this.fb.control('',
          [Validators.required, Validators.pattern('^[a-zA-Z0-9_ ]*$')]);
      this.email = this.fb.control('', [Validators.required, Validators.email]);
      this.phone = this.fb.control('', [Validators.required, Validators.pattern('^[0-9]+$')]);
      this.address = this.fb.control('');
      this.currency = this.fb.control('');
        this.cyb_transactionKey = this.fb.control('');
        this.cyb_mid = this.fb.control('');
      this.baseUrl = this.fb.control('');
      this.bccEmailList = this.fb.control('');

        this.updateMerchantForm = this.fb.group({
            merchantName: this.merchantName,
            email: this.email,
            phone: this.phone,
            address: this.address,
            currency: this.currency,
            cyb_transactionKey: this.cyb_transactionKey,
            cyb_mid: this.cyb_mid,
            baseUrl: this.baseUrl,
            bccEmailList: this.bccEmailList
        });

    

  }

    onUpdateMerchantSubmit() {
    
        this.updateMerchantForm.markAsDirty();
        if (this.updateMerchantForm.valid) {

      this.loadingSave = true;

            console.log(JSON.stringify(this.updateMerchantForm.value));


      let token;
      this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
        token = tokenData.getValue();
      });        

      let headers = new HttpHeaders().set("Authorization", "Bearer " +
        token).set("Content-Type", "application/json");

            this.http.post('api/merchant/create', this.updateMerchantForm.value,
        { headers: headers }).subscribe((res:any) => {

      if (res.actionStatus === 'SUCCESS') {
         // if (res) {
        this.loadingSave = false;
        this.isAlert = true;
        this.alertStatus = 'success';
        this.alertMessage = "Record created successfully!";
        setTimeout(() => {
          this.isAlert = false;
          this.alertMessage = '';
          this.router.navigate(['../merchant-list'], { relativeTo: this.r });
        }, 3000);
         
        }
        else {

        this.loadingSave = false;
        this.isAlert = true;
        this.alertStatus = 'danger';
        this.alertMessage = "Could not create record!";
        setTimeout(() => {
          this.isAlert = false;
          this.alertMessage = '';

        }, 3000);
        }

      },
        error => {
          console.log('api/merchant/create', error);

          this.loadingSave = false;
          this.isAlert = true;
          this.alertStatus = 'danger';
          this.alertMessage = error.statusText;
          setTimeout(() => {
            this.isAlert = false;
            this.alertMessage = '';

            if (error.status === 401) {
              this.router.navigate(['/auth/login']);
            }

          }, 3000);


        });


    }
    else {
      this.isAlert = true;
      this.alertStatus = 'danger';
      this.alertMessage = "The form is not valid!";
      setTimeout(() => {
        this.isAlert = false;
        this.alertMessage = '';
      }, 3000);
    }
  }

 
}

