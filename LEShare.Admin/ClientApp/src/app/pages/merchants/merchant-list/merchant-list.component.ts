import { Component, ViewEncapsulation } from '@angular/core';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { NbAuthService, NbAuthJWTToken, NbAuthToken } from '@nebular/auth';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-smart-table',
    templateUrl: './merchant-list.component.html',
    styleUrls: ['./merchant-list.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class MerchantListComponent {

  settings = {
    actions: {
      add: false,
      edit:false,
      delete: false
    },
    columns: {
      id: {
        title: '#',
        type: 'number'
      },
        merchantName: {
        title: 'Name',
        type: 'html',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private http: HttpClient, private authService: NbAuthService, private router: Router) {

    this.loadMerchants();

  }

    private loadMerchants() {
      
   // console.log(JSON.stringify(this.authService.getToken()));

    let token;
    this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
      token = tokenData.getValue();
    });

   // let token = this.authService.getToken().value.token;

    let headers = new HttpHeaders().set("Authorization", "Bearer " +
      token).set("Content-Type", "application/json");

    this.http.get('api/merchant/get-list', { headers: headers }).subscribe((res: any[])=> {

      
      // var result = res as partner[];
      var temp = new Array();
      for (let i = 0; i < res.length; i++) {
        var obj = {
          id: i+1,
          merchantName: '<a href="#/pages/merchants/view-merchant/' + res[i].id + '">' + res[i].merchantName+'</a>',
          email: res[i].email
         }
        temp.push(obj);
      }
      this.source = new LocalDataSource(temp); 

    },
      error => {
        console.log('api/merchant/get-list', error)
        if (error.status === 401) {
          this.router.navigate(['/auth/login']);
        }
      });
  }
}



