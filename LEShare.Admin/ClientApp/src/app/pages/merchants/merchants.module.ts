import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { MerchantsRoutingModule, routedComponents } from './merchants-routing.module';
import { NbCardModule } from '@nebular/theme';


@NgModule({
  imports: [
    ThemeModule,
    MerchantsRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class MerchantsModule { }
