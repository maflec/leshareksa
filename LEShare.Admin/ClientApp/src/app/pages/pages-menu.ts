import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'MENU',
    group: true,
  },
  {
    title: 'Payment Requests',
    icon: 'nb-arrow-retweet',
    link: '/pages/payment-requests/request-list',
    home: true,
  },
  {
    title: 'Partners',
    icon: 'nb-person',
    link: '/pages/partners/partner-list',
    home: true,
    },
    {
        title: 'Merchants',
        icon: 'nb-person',
        link: '/pages/merchants/merchant-list',
        home: true,
    },
  {
      title: 'Users',
      icon: 'nb-person',
      link: '/pages/users/user-list',
      home: true,
  }
];
