import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { NbAuthService, NbAuthJWTToken, NbAuthToken } from '@nebular/auth';

@Component({
  selector: 'ngx-form-layouts',
  templateUrl: './new-request.component.html',
  styleUrls: ['./new-request.component.scss'],
})


export class NewRequestComponent implements OnInit {

  newRequestForm: FormGroup;
  reference: FormControl;
  orderDetails: FormControl;
  orderTitle: FormControl;
  amount: FormControl;
  expiryHours: FormControl;
  partnerId: FormControl;
  attachments: FormControl;


  loadingSave = false;
  isAlert = false;
  alertMessage = '';
  alertStatus = '';

  partners: any[];

  constructor(private http: HttpClient, private authService: NbAuthService, private fb: FormBuilder,
    private cd: ChangeDetectorRef, private router: Router, private r: ActivatedRoute) {

    this.loadPartners();
  }

  ngOnInit() {

    this.reference = this.fb.control('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_]*$')]);
    this.orderTitle = this.fb.control('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_ ]*$')]);
    this.orderDetails = this.fb.control('', Validators.required);
    this.amount = this.fb.control('', [Validators.required, Validators.pattern('^[0-9]{1,9}([.][0-9]{1,4})?$')]);
    this.expiryHours = this.fb.control('', Validators.required);
    this.partnerId = this.fb.control('', [Validators.required, Validators.pattern('^[1-9][0-9]*$')]);

    this.newRequestForm = this.fb.group({
      reference: this.reference,
      orderTitle: this.orderTitle,
      orderDetails: this.orderDetails,
      amount: this.amount,
      expiryHours: this.expiryHours,
      partnerId: this.partnerId,
      attachments: null
    });



  }


  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.newRequestForm.get('attachments').setValue(file);
    }
  }

  onNewRequestSubmit() {
       
    this.newRequestForm.markAsDirty();

    if (this.newRequestForm.valid) {

      this.loadingSave = true;

      console.log(JSON.stringify(this.newRequestForm.value));


      let token;
      this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
        token = tokenData.getValue();
      });

      let input = new FormData();
      // Add your values in here
      input.append('reference', this.newRequestForm.value.reference);
      input.append('orderTitle', this.newRequestForm.value.orderTitle);
      input.append('orderDetails', this.newRequestForm.value.orderDetails);
      input.append('amount', this.newRequestForm.value.amount);
      input.append('expiryHours', this.newRequestForm.value.expiryHours);
      input.append('partnerId', this.newRequestForm.value.partnerId);
      input.append('attachments', this.newRequestForm.value.attachments);
   

      //let headers = new HttpHeaders().set("Authorization", "Bearer " +
      //  token).set("Content-Type", "application/json");

      const HttpUploadOptions = {
        headers: new HttpHeaders({ "Accept": "application/json" }).set("Authorization", "Bearer " +
          token)
      }

      this.http.post('api/paymentrequest/create', input,
        HttpUploadOptions).subscribe((res: any) => {

          if (res.actionStatus === 'SUCCESS') {
            // if (res) {
            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'success';
            this.alertMessage = "Record created successfully!";
            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';
              this.router.navigate(['../view-request', res.actionResult], { relativeTo: this.r });
            }, 3000);

          }
          else {

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'danger';
            this.alertMessage = "Could not create record!";
            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';

            }, 3000);
          }

        },
          error => {
            console.log('api/paymentrequest/create', error);

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'danger';
            this.alertMessage = error.statusText;
            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';

              if (error.status === 401) {
                this.router.navigate(['/auth/login']);
              }

            }, 3000);


          });


    }
    else {
      this.isAlert = true;
      this.alertStatus = 'danger';
      this.alertMessage = "The form is not valid!";
      setTimeout(() => {
        this.isAlert = false;
        this.alertMessage = '';
      }, 3000);
    }

  }

  private loadPartners() {

    // console.log(JSON.stringify(this.authService.getToken()));

    let token;
    this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
      token = tokenData.getValue();
    });

    // let token = this.authService.getToken().value.token;

    let headers = new HttpHeaders().set("Authorization", "Bearer " +
      token).set("Content-Type", "application/json");

    this.http.get('api/partner/get-list', { headers: headers }).subscribe((res: any[]) => {

      console.log(JSON.stringify(res));

      this.partners = res;

    },
      error => {
        console.log('api/partner/get-list', error)
        if (error.status === 401) {
          this.router.navigate(['/auth/login']);
        }
      });
  }


}


