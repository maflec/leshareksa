import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { PaymentRequstsRoutingModule, routedComponents } from './payment-requests-routing.module';
import { NbCardModule } from '@nebular/theme';


@NgModule({
  imports: [
    ThemeModule,
    PaymentRequstsRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class PaymentRequestsModule { }
