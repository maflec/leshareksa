import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NbAuthToken, NbAuthService } from '@nebular/auth';
import { HttpClient, HttpClientModule, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-form-layouts',
  templateUrl: 'view-request.component.html',
  styleUrls: ['view-request.component.scss'],
})


export class ViewRequestComponent implements OnInit {

  request: any;

  loadingDeactivate = false;
  loadingSend = false;
  isAlert = false;
  alertMessage = '';
  alertStatus = '';

  constructor(private http: HttpClient, private fb: FormBuilder, private router: Router, private r: ActivatedRoute
    , private authService: NbAuthService) {

    this.loadRequest();
   
  }

  ngOnInit() {
    //yghg

  }

  private loadRequest() {

    let id = this.r.snapshot.paramMap.get('id');

    console.log(id)

    let token;
    this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
      token = tokenData.getValue();
    });

    let headers = new HttpHeaders().set("Authorization", "Bearer " +
      token).set("Content-Type", "application/json");

    this.http.get('api/paymentrequest/get?id=' + id, { headers: headers }).subscribe((res: any) => {

     
      this.request = res;

    },
      error => {
        console.log('api/paymentrequest/get', error)
        if (error.status === 401) {
          this.router.navigate(['/auth/login']);
        }
      });
  }


  onDeactivateRequestSubmit() {

    this.loadingDeactivate = true;

    let token;
    this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
      token = tokenData.getValue();
    });

    let headers = new HttpHeaders().set("Authorization", "Bearer " +
      token).set("Content-Type", "application/json");

    let id = this.r.snapshot.paramMap.get('id');

    this.http.put('api/paymentrequest/deactivate?id=' + id , null,
      { headers: headers }).subscribe((res: any) => {

        if (res.actionStatus === 'SUCCESS') {

          this.loadRequest();

          this.loadingDeactivate = false;
          this.isAlert = true;
          this.alertStatus = 'success';
          this.alertMessage = "Request decativated successfully!";
          setTimeout(() => {
            this.isAlert = false;
            this.alertMessage = '';
          }, 3000);

          // this.router.navigate(['/pages/partners/partner-list']);
        }
        else {

          

          this.loadingDeactivate = false;
          this.isAlert = true;
          this.alertStatus = 'danger';
          this.alertMessage = "Could not update record!";

          setTimeout(() => {
            this.isAlert = false;
            this.alertMessage = '';

          }, 3000);
        }

      },
        error => {
          console.log('api/paymentrequest/deactivate', error);

          this.loadingDeactivate = false;
          this.isAlert = true;
          this.alertStatus = 'danger';
          this.alertMessage = error.statusText;
          setTimeout(() => {
            this.isAlert = false;
            this.alertMessage = '';

            if (error.status === 401) {
              this.router.navigate(['/auth/login']);
            }

          }, 3000);
        });

  }

  onSendRequestSubmit() {

    this.loadingSend = true;

    let token;
    this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
      token = tokenData.getValue();
    });

    let headers = new HttpHeaders().set("Authorization", "Bearer " +
      token).set("Content-Type", "application/json");

    let id = this.r.snapshot.paramMap.get('id');

    this.http.put('api/paymentrequest/send?id=' + id, null,
      { headers: headers }).subscribe((res: any) => {

        if (res.actionStatus === 'SUCCESS') {

          this.loadRequest();

          this.loadingSend = false;
          this.isAlert = true;
          this.alertStatus = 'success';
          this.alertMessage = "Request sent successfully!";
          setTimeout(() => {
            this.isAlert = false;
            this.alertMessage = '';
          }, 3000);

          // this.router.navigate(['/pages/partners/partner-list']);
        }
        else {



          this.loadingSend = false;
          this.isAlert = true;
          this.alertStatus = 'danger';
          this.alertMessage = "Could not send request!";

          setTimeout(() => {
            this.isAlert = false;
            this.alertMessage = '';

          }, 3000);
        }

      },
        error => {
          console.log('api/paymentrequest/send', error);

          this.loadingSend = false;
          this.isAlert = true;
          this.alertStatus = 'danger';
          this.alertMessage = error.statusText;
          setTimeout(() => {
            this.isAlert = false;
            this.alertMessage = '';

            if (error.status === 401) {
              this.router.navigate(['/auth/login']);
            }

          }, 3000);
        });

  }
 
}

