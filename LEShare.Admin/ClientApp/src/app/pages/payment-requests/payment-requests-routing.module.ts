import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentRequstsComponent } from './payment-requests.component';
import { RequestListComponent } from './request-list/request-list.component';
import { NewRequestComponent } from './new-request/new-request.component';
import { ViewRequestComponent } from './view-request/view-request.component';

const routes: Routes = [{
  path: '',
  component: PaymentRequstsComponent,
  children: [{
    path: 'request-list',
    component: RequestListComponent,
  }, {
      path: 'new-request',
      component: NewRequestComponent,
    }, {
      path: 'view-request/:id',
      component: ViewRequestComponent,
    }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentRequstsRoutingModule { }

export const routedComponents = [
  PaymentRequstsComponent,
  RequestListComponent,
  NewRequestComponent,
  ViewRequestComponent,
];
