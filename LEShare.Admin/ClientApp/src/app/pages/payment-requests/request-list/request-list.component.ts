import { Component, ViewEncapsulation } from '@angular/core';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { NbAuthService, NbAuthJWTToken, NbAuthToken } from '@nebular/auth';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-smart-table',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class RequestListComponent {

  settings = {
    actions: {
      add: false,
      edit:false,
      delete: false
    },
    columns: {
      id: {
        title: '#',
        type: 'number'
      },
      reference: {
        title: 'Reference',
        type: 'html',
      },
      orderTitle: {
        title: 'Title',
        type: 'string',
      },
      amount: {
        title: 'Amount',
        type: 'string',
      },
      partnerName: {
        title: 'Partner',
        type: 'html',
      },
      createdDate: {
        title: 'Date',
        type: 'string',
      },
      expiryDate: {
        title: 'Expiry Date',
        type: 'string',
      },     
      requestStatus: {
        title: 'Status',
        type: 'number',
        }, merchantName: {
            title: 'Merchant',
            type: 'html',
        },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  public dateRange;

  loadingSearch = false;

  constructor(private http: HttpClient, private authService: NbAuthService, private router: Router) {

    var dt = new Date();

    dt.setDate(1);

    this.dateRange = { start: dt, end: new Date() };

    this.loadPayRequests();

  }

  public loadPayRequests() {

    console.log(JSON.stringify(this.dateRange));

    if (this.dateRange) {

      this.loadingSearch = true;

      var date1 = this.dateRange.start.toString().split('GMT', 1)[0];
      var date2 = this.dateRange.end.toString().split('GMT', 1)[0];

      let params = new HttpParams().set('date1', date1).set('date2', date2);

      let token;
      this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
        token = tokenData.getValue();
      });

      let headers = new HttpHeaders().set("Authorization", "Bearer " +
        token).set("Content-Type", "application/json");

      this.http.get('api/paymentrequest/get-list',
        { headers: headers, params: params }).subscribe((res: any[]) => {

         // this.source.load(res);


          var temp = new Array();
          for (let i = 0; i < res.length; i++) {
            var obj = {
              id: i + 1,
              reference: '<a href="#/pages/payment-requests/view-request/' + res[i].id + '">'
                + res[i].reference + '</a>',
              orderTitle: res[i].orderTitle,
              amount: res[i].amount,
                partnerName: '<a href="#/pages/partners/view-partner/' + res[i].partnerId + '">'
                    + res[i].partnerName + '</a>',
              createdDate: res[i].createdDate,
              expiryDate: res[i].expiryDate,
              requestStatus: res[i].requestStatus,
                merchantName: '<a href="#/pages/merchants/view-merchant/' + res[i].merchantId + '">'
                    + res[i].merchantName + '</a>'
            }
            temp.push(obj);
          }
          this.source = new LocalDataSource(temp); 


          this.loadingSearch = false;
        },
          error => {
            console.log('api/paymentrequest/get-list', error);
            this.loadingSearch = false;
            if (error.status === 401) {
              this.router.navigate(['/auth/login']);
            }
          });

    }
  }
}

