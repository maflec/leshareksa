import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NbAuthService, NbAuthToken } from '@nebular/auth';
import { HttpClient, HttpClientModule, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'ngx-form-layouts',
  templateUrl: './new-partner.component.html',
  styleUrls: ['./new-partner.component.scss'],
})


export class NewPartnerComponent implements OnInit {

  newPartnerForm: FormGroup;
  partnerName: FormControl;
  email: FormControl;
  phone: FormControl;
  address: FormControl;
  loadingSave = false;
  isAlert = false;  
  alertMessage = '';
  alertStatus = '';
  
  
  constructor(private http: HttpClient,private fb: FormBuilder, private router: Router,
    private r: ActivatedRoute, private authService: NbAuthService) {
    
    //this.partners = this.service.getData();
  }

  ngOnInit() {
    this.partnerName = this.fb.control('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_ ]*$')]);
    this.email = this.fb.control('', [Validators.required, Validators.email]);
    this.phone = this.fb.control('', [Validators.required, Validators.pattern('^[0-9]+$')]);
    this.address = this.fb.control('');
    

    this.newPartnerForm = this.fb.group({
      partnerName: this.partnerName,
      email: this.email,
      phone: this.phone,
      address: this.address
    });

    

  }

  onNewPartnerSubmit() {
    
    this.newPartnerForm.markAsDirty();
    if (this.newPartnerForm.valid) {

      this.loadingSave = true;

      console.log(JSON.stringify(this.newPartnerForm.value));


      let token;
      this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
        token = tokenData.getValue();
      });        

      let headers = new HttpHeaders().set("Authorization", "Bearer " +
        token).set("Content-Type", "application/json");

      this.http.post('api/partner/create', this.newPartnerForm.value,
        { headers: headers }).subscribe((res:any) => {

      if (res.actionStatus === 'SUCCESS') {
         // if (res) {
        this.loadingSave = false;
        this.isAlert = true;
        this.alertStatus = 'success';
        this.alertMessage = "Record created successfully!";
        setTimeout(() => {
          this.isAlert = false;
          this.alertMessage = '';
          this.router.navigate(['../partner-list'], { relativeTo: this.r });
        }, 3000);
         
        }
        else {

        this.loadingSave = false;
        this.isAlert = true;
        this.alertStatus = 'danger';
        this.alertMessage = "Could not create record!";
        setTimeout(() => {
          this.isAlert = false;
          this.alertMessage = '';

        }, 3000);
        }

      },
        error => {
          console.log('api/partner/create', error);

          this.loadingSave = false;
          this.isAlert = true;
          this.alertStatus = 'danger';
          this.alertMessage = error.statusText;
          setTimeout(() => {
            this.isAlert = false;
            this.alertMessage = '';

            if (error.status === 401) {
              this.router.navigate(['/auth/login']);
            }

          }, 3000);


        });


    }
    else {
      this.isAlert = true;
      this.alertStatus = 'danger';
      this.alertMessage = "The form is not valid!";
      setTimeout(() => {
        this.isAlert = false;
        this.alertMessage = '';
      }, 3000);
    }
  }

 
}

