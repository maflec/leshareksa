import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PartnersComponent } from './partners.component';
import { PartnerListComponent } from './partner-list/partner-list.component';
import { NewPartnerComponent } from './new-partner/new-partner.component';
import { ViewPartnerComponent } from './view-partner/view-partner.component';

const routes: Routes = [{
  path: '',
  component: PartnersComponent,
  children: [{
    path: 'partner-list',
    component: PartnerListComponent,
  }, {
      path: 'new-partner',
      component: NewPartnerComponent,
    }, {
      path: 'view-partner/:id',
      component: ViewPartnerComponent,
    }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PartnersRoutingModule { }

export const routedComponents = [
  PartnersComponent,
  PartnerListComponent,
  NewPartnerComponent,
  ViewPartnerComponent,
];
