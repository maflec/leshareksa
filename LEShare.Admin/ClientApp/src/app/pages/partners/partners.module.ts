import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { PartnersRoutingModule, routedComponents } from './partners-routing.module';
import { NbCardModule } from '@nebular/theme';


@NgModule({
  imports: [
    ThemeModule,
    PartnersRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class PartnersModule { }
