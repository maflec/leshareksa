import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NbAuthToken, NbAuthService } from '@nebular/auth';
import { HttpClient, HttpClientModule, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'ngx-form-layouts',
  templateUrl: './view-partner.component.html',
  styleUrls: ['./view-partner.component.scss'],
})


export class ViewPartnerComponent implements OnInit {

  updatePartnerForm: FormGroup;
  partnerName: FormControl;
  email: FormControl;
  phone: FormControl;
    address: FormControl;
    merchantName: FormControl;

  loadingSave = false;
  isAlert = false;
  alertMessage = '';
  alertStatus = '';


  constructor(private http: HttpClient, private fb: FormBuilder, private router: Router, private r: ActivatedRoute
    , private authService: NbAuthService) {

    this.loadPartner();
   
  }

  ngOnInit() {

    this.partnerName = this.fb.control('',
      [Validators.required, Validators.pattern('^[a-zA-Z0-9_ ]*$')]);
    this.email = this.fb.control('', [Validators.required, Validators.email]);
    this.phone = this.fb.control('', [Validators.required, Validators.pattern('^[0-9]+$')]);
    this.address = this.fb.control('');
    this.merchantName = this.fb.control('');
    this.setForm();

  }

  private setForm() {

    this.updatePartnerForm = this.fb.group({
      partnerName: this.partnerName,
      email: this.email,
      phone: this.phone,
        address: this.address,
        merchantName: this.merchantName
    });

  }

  private loadPartner() {

    let id = this.r.snapshot.paramMap.get('id');

    console.log(id)

    let token;
    this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
      token = tokenData.getValue();
    });

    let headers = new HttpHeaders().set("Authorization", "Bearer " +
      token).set("Content-Type", "application/json");

    this.http.get('api/partner/get?id='+id, { headers: headers }).subscribe((res:any) => {

      //this.source.load(res);

      console.log(JSON.stringify(res));


      this.partnerName = this.fb.control(res.partnerName,
                       [Validators.required, Validators.pattern('^[a-zA-Z0-9_ ]*$')]);
      this.email = this.fb.control(res.email, [Validators.required, Validators.email]);
      this.phone = this.fb.control(res.phone, [Validators.required, Validators.pattern('^[0-9]+$')]);
        this.address = this.fb.control(res.address);
        this.merchantName = this.fb.control(res.merchantName);

      this.setForm();

    },
      error => {
        console.log('api/partner/get', error)
        if (error.status === 401) {
          this.router.navigate(['/auth/login']);
        }
      });
  }

  onUpdatePartnerSubmit() {

    this.updatePartnerForm.markAsDirty();
    if (this.updatePartnerForm.valid) {

      this.loadingSave = true;

      console.log(JSON.stringify(this.updatePartnerForm.value));


      let token;
      this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
        token = tokenData.getValue();
      });

      let headers = new HttpHeaders().set("Authorization", "Bearer " +
        token).set("Content-Type", "application/json");

      let id = this.r.snapshot.paramMap.get('id');

      this.http.put('api/partner/update?id=' + id, this.updatePartnerForm.value,
        { headers: headers }).subscribe((res:any) => {

          if (res.actionStatus === 'SUCCESS') {

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'success';
            this.alertMessage = "Record updated successfully!";
            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';
            }, 3000);

           // this.router.navigate(['/pages/partners/partner-list']);
          }
          else {

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'danger';
            this.alertMessage = "Could not update record!";

            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';

            }, 3000);
          }

        },
          error => {
            console.log('api/partner/update', error);

            this.loadingSave = false;
            this.isAlert = true;
            this.alertStatus = 'danger';
            this.alertMessage = error.statusText;
            setTimeout(() => {
              this.isAlert = false;
              this.alertMessage = '';

              if (error.status === 401) {
                this.router.navigate(['/auth/login']);
              }

            }, 3000);
          });

    }
    else {
      this.isAlert = true;
      this.alertStatus = 'danger';
      this.alertMessage = "The form is not valid!";
      setTimeout(() => {
        this.isAlert = false;
        this.alertMessage = '';
      }, 3000);
    }
  }


}
