import { Component, ViewEncapsulation } from '@angular/core';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { NbAuthService, NbAuthJWTToken, NbAuthToken } from '@nebular/auth';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-smart-table',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class PartnerListComponent {

  settings = {
    actions: {
      add: false,
      edit:false,
      delete: false
    },
    columns: {
      id: {
        title: '#',
        type: 'number'
      },
      partnerName: {
        title: 'Name',
        type: 'html',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
      phone: {
        title: 'Phone',
        type: 'string',
      },
      address: {
        title: 'Address',
        type: 'string',
        },
        merchantName: {
            title: 'Merchant',
            type: 'html',
        },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private http: HttpClient, private authService: NbAuthService, private router: Router) {

    this.loadPartners();

  }

  private loadPartners() {
      
   // console.log(JSON.stringify(this.authService.getToken()));

    let token;
    this.authService.getToken().subscribe((tokenData: NbAuthToken) => {
      token = tokenData.getValue();
    });

   // let token = this.authService.getToken().value.token;

    let headers = new HttpHeaders().set("Authorization", "Bearer " +
      token).set("Content-Type", "application/json");

    this.http.get('api/partner/get-list', { headers: headers }).subscribe((res: any[])=> {

      //this.source.load(res);

      console.log(JSON.stringify(res))

      // var result = res as partner[];
      var temp = new Array();
      for (let i = 0; i < res.length; i++) {
        var obj = {
          id: i+1,
          partnerName: '<a href="#/pages/partners/view-partner/' + res[i].id + '">' + res[i].partnerName+'</a>',
          email: res[i].email,
          phone: res[i].phone,
          address: res[i].address,
            merchantName: '<a href="#/pages/merchants/view-merchant/' + res[i].merchantId + '">'
                + res[i].merchantName + '</a>',

         }
        temp.push(obj);
      }
      this.source = new LocalDataSource(temp); 

    },
      error => {
        console.log('api/partner/get-list', error)
        if (error.status === 401) {
          this.router.navigate(['/auth/login']);
        }
      });
  }
}



