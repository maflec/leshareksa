﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Admin.Controllers
{
    public class LoginModel
    {
        public string email { get; set; }
        public string password { get; set; }
    }

    public class RequestPassModel
    {
        public string email { get; set; }
       
    }

    public class ResetPassModel
    {
        public string tokenKey { get; set; }
        public string password { get; set; }
    }

    public class PaymentRequest
    {
        public string id { get; set; }
        public string reference { get; set; }
        public string orderDetails { get; set; }
        public string orderTitle { get; set; }
        public string amount { get; set; }
        public string partnerName { get; set; }
        public string email { get; set; }
        public string createdDate { get; set; }
        public string expiryDate { get; set; }
        public string requestStatus { get; set; }
        public string history { get; set; }
        public string partnerId { get; set; }
        public string paymentLink { get; set; }
        public int expiryHours { get; set; }
        public string isExpired { get; set; }
        public string requestGuid { get; set; }
        public string currency { get; set; }
        public string merchantName { get; set; }
        public string merchantId { get; set; }



    }

    public class Partner
    {
        public string id { get; set; }
        public string merchantId { get; set; }
        public string partnerName { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string merchantName { get; set; }

    }

    public class MerchantUser
    {
        public string id { get; set; }
        public string nameOfUser { get; set; }
        public string username { get; set; }
        public string defaultMerchant { get; set; }
        public string merchantId { get; set; }        
        public Merchant[] merchantList { get; set; }

    }

    public class MerchantA
    {
        public string id { get; set; }
        public string merchantName { get; set; }
        public string address { get; set; }
        public string currency { get; set; }
        public string cyb_mid { get; set; }
        public string cyb_transactionKey { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string baseUrl { get; set; }
        public string bccEmailList { get; set; }

    }
    public class Merchant
    {
        public string id { get; set; }
        public string merchantName { get; set; }
        public string email { get; set; }

    }
}


