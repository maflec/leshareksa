using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using System.Security.Claims;

namespace LEShare.Admin.Controllers
{
   
    [Route("api/[controller]"), Authorize]
    public class UserController : Controller
    {
        [HttpGet, Route("get")]
        public MerchantUser GetUser(int id)
        {
            MerchantUser merchantUser = new MerchantUser();
            try
            {                
                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);              
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetMerchantUser");
               
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    merchantUser = new MerchantUser
                    {
                        id = r["id"].ToString(),
                        nameOfUser = r["nameOfUser"].ToString(),
                        username = r["username"].ToString(),
                        defaultMerchant = r["merchantName"].ToString(),
                        merchantId = r["merchantId"].ToString()

                    };

                }

                        
                sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, id);
                ds = sqlHelper.GetDatasetByCommand("SP_GetUserMerchantList");

                merchantUser.merchantList = new Merchant[ds.Tables[0].Rows.Count];
                int i = 0;
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    merchantUser.merchantList[i++]= new Merchant
                    {
                        id = r["id"].ToString(),
                        merchantName = r["merchantName"].ToString()

                    };

                }

            }
            catch (Exception ex)
            {

            }
            return merchantUser;
        }

        [HttpGet, Route("get-list")]
        public List<MerchantUser> GetList()
        {
            List<MerchantUser> merchantUsers = new List<MerchantUser>();
            try
            {
               
                SqlHelper sqlHelper = new SqlHelper();               
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetMerchantUserList");

                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    merchantUsers.Add(new MerchantUser
                    {
                        id = r["id"].ToString(),
                        nameOfUser = r["nameOfUser"].ToString(),
                        username = r["username"].ToString(),
                        defaultMerchant = r["merchantName"].ToString(),
                        merchantId = r["merchantId"].ToString()

                    });

                }
            }
            catch (Exception ex)
            {

            }
            return merchantUsers;
        }

        [HttpPost, Route("create")]
        public ApiResponse CreateUser([FromBody]MerchantUser model)
        {
            ApiResponse response = new ApiResponse();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@nameOfUser", SqlDbType.VarChar, model.nameOfUser);
                sqlHelper.AddSetParameterToSQLCommand("@username", SqlDbType.VarChar, model.username);
                sqlHelper.AddSetParameterToSQLCommand("@merchantId", SqlDbType.VarChar, model.merchantId);
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_CreateUser");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";

                        var id = ds.Tables[0].Rows[0]["id"].ToString();

                        foreach (Merchant merchant in model.merchantList)
                        {
                            sqlHelper = new SqlHelper();
                            sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, id);
                            sqlHelper.AddSetParameterToSQLCommand("@merchantId", SqlDbType.Int, merchant.id);
                            ds = sqlHelper.GetDatasetByCommand("SP_AddUserMerchant");
                        }

                    }
                }

                return response;

            }
            catch (Exception ex)
            {

            }

            return response;

        }

        [HttpPut, Route("update")]
        public ApiResponse UpdateUser([FromBody]MerchantUser model, int id)
        {
            ApiResponse response = new ApiResponse();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);
                sqlHelper.AddSetParameterToSQLCommand("@nameOfUser", SqlDbType.VarChar, model.nameOfUser);
                sqlHelper.AddSetParameterToSQLCommand("@username", SqlDbType.VarChar, model.username);
                sqlHelper.AddSetParameterToSQLCommand("@merchantId", SqlDbType.VarChar, model.merchantId);
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_UpdateUser");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";
                        
                        sqlHelper = new SqlHelper();
                        sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, id);                       
                        ds = sqlHelper.GetDatasetByCommand("SP_DeleteAllUserMerchant");

                        foreach (Merchant merchant in model.merchantList)
                        { 
                            sqlHelper = new SqlHelper();
                            sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, id);                           
                            sqlHelper.AddSetParameterToSQLCommand("@merchantId", SqlDbType.Int, merchant.id);
                            ds = sqlHelper.GetDatasetByCommand("SP_AddUserMerchant");
                        }

                    }
                }

                return response;

            }
            catch (Exception ex)
            {

            }

            return response;

        }

        [HttpDelete, Route("delete")]
        public ApiResponse DeletePartner(int id)
        {
            ApiResponse response = new ApiResponse();
            try
            {
                IEnumerable<Claim> claims = User.Claims;
                var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;

                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);
                sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_DeletePartner");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";

                    }
                }

                return response;

            }
            catch (Exception ex)
            {

            }

            return response;

        }
    }
}
