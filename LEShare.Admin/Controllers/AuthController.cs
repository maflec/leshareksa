﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
//using System.DirectoryServices;
//using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using Microsoft.AspNetCore.Authorization;
using System.IO;

namespace LEShare.Admin.Controllers
{
    [Route("api/auth")]
    public class AuthController : Controller
    {
        Encryption enc = new Encryption();

        [HttpPost, Route("sign-in")]
        public IActionResult Login([FromBody]LoginModel model)
        {
            if (model == null)
            {
                return BadRequest("Invalid client request");
            }

            bool isValid = false;

         

            SqlHelper sqlHelper = new SqlHelper();
            sqlHelper.AddSetParameterToSQLCommand("@username", SqlDbType.NVarChar, model.email);
            sqlHelper.AddSetParameterToSQLCommand("@passwordEnc", SqlDbType.VarChar, enc.Encrypt(model.password));
            DataSet ds = sqlHelper.GetDatasetByCommand("SP_ValidateUserEncAdmin");

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    if (ds.Tables[0].Rows[0][0].ToString().ToUpper() == "SUCCESS")
                    {
                        isValid = true;

                    }

                }

            }

            if (isValid)
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("paymentsecretkey@345"));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
                var claims = new List<Claim>
                {
                    new Claim("userName", model.email),
                    new Claim("userRole", "AdminAdmin"),
                    new Claim("id", ds.Tables[0].Rows[0]["id"].ToString().ToUpper()),
                    new Claim("nameOfUser", ds.Tables[0].Rows[0]["nameOfUser"].ToString().ToUpper()),
                    
                    //
                };
                var tokeOptions = new JwtSecurityToken(
                    issuer: "http://localhost:62003",
                    audience: "http://localhost:62003",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(5),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);

                return Ok(new { Token = tokenString });
            }
            else
            {
                return Unauthorized();
            }
        }

        
        [HttpDelete, Route("sign-out")]
        public IActionResult Logout()
        {
            Response.Cookies.Delete("token");
            return Ok();
        }

        [HttpPost, Route("request-pass")]
        public IActionResult RequestPass([FromBody]RequestPassModel model)
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                var tokenKey=Guid.NewGuid().ToString();
                sqlHelper.AddSetParameterToSQLCommand("@username", SqlDbType.VarChar, model.email);
                sqlHelper.AddSetParameterToSQLCommand("@passwordToken", SqlDbType.VarChar, tokenKey);
                

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_RequestPasswordAdmin");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["queryStatus"].ToString() == "SUCCESS")
                    {
                        string emailSubject = "";
                        emailSubject = "Password reset";

                        //StreamReader sr = new StreamReader("passwordreset_email_template.html");
                        //string body = sr.ReadToEnd();
                        //sr.Close();

                        string body = General.GetSettingsValue("passwordreset_email_template");

                        body = body.Replace("#USER_NAME#", ds.Tables[0].Rows[0]["nameOfUser"].ToString());
                        body = body.Replace("#RESET_LINK#", General.GetSettingsValue("adminPasswordResetBaseUrl") + "?tokenKey="+ tokenKey);
                 

                        string fromEmail = General.GetSettingsValue("fromEmail");
                        string fromDisplayName = General.GetSettingsValue("fromDisplayName");

                        if (General.SendEmail(emailSubject, body, fromEmail, fromDisplayName, ds.Tables[0].Rows[0]["email"].ToString()))
                        {
                            return Ok();
                        }

                    }
                }
            }
            catch(Exception ex)
            {

            }

            return Unauthorized();
           

        }

        [HttpPut, Route("reset-pass")]
        public IActionResult ResetPass([FromBody]ResetPassModel model)
        {
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                var tokenKey = Guid.NewGuid().ToString();
                sqlHelper.AddSetParameterToSQLCommand("@passwordEnc", SqlDbType.VarChar, enc.Encrypt(model.password));
                sqlHelper.AddSetParameterToSQLCommand("@passwordToken", SqlDbType.VarChar, model.tokenKey);


                DataSet ds = sqlHelper.GetDatasetByCommand("SP_ResetPasswordEncAdmin");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["queryStatus"].ToString() == "SUCCESS")
                    {
                        return Ok();

                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Unauthorized();

        }
    }
}