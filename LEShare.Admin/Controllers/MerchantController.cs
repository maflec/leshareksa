using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using System.Security.Claims;

namespace LEShare.Admin.Controllers
{
   
    [Route("api/[controller]"), Authorize]
    public class MerchantController : Controller
    {
        Encryption enc = new Encryption();

        [HttpGet, Route("get-list")]
        public List<Merchant> GetList()
        {
            List<Merchant> merchants = new List<Merchant>();
            try
            {
              
                SqlHelper sqlHelper = new SqlHelper();                
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetMerchantList");

                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    merchants.Add(new Merchant
                    {
                        id = r["id"].ToString(),
                        merchantName = r["merchantName"].ToString(),
                        email = r["email"].ToString()

                    });

                }
            }
            catch (Exception ex)
            {

            }
            return merchants;
        }

        [HttpGet, Route("get")]
        public MerchantA GetMerchant(int id)
        {
            MerchantA merchant = new MerchantA();
            try
            {
               

                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);               
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetMerchant");

                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    merchant = new MerchantA
                    {
                        merchantName = r["merchantName"].ToString(),
                        address = r["address"].ToString(),
                        email = r["email"].ToString(),
                        currency = r["currency"].ToString(),
                        cyb_transactionKey = enc.Decrypt(r["cyb_transactionKeyEnc"].ToString()),
                        baseUrl = r["baseUrl"].ToString(),
                        bccEmailList = r["bccEmailList"].ToString(),
                        phone = r["phone"].ToString(),
                        cyb_mid = r["cyb_mid"].ToString()

                    };

                }
            }
            catch (Exception ex)
            {

            }
            return merchant;
        }

        [HttpPost, Route("create")]
        public ApiResponse CreateMerchant([FromBody]MerchantA model)
        {
            ApiResponse response = new ApiResponse();
            try
            {
                IEnumerable<Claim> claims = User.Claims;
                var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;                

                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@merchantName", SqlDbType.VarChar, model.merchantName);
                sqlHelper.AddSetParameterToSQLCommand("@email", SqlDbType.VarChar, model.email);
                sqlHelper.AddSetParameterToSQLCommand("@phone", SqlDbType.VarChar, model.phone);
                sqlHelper.AddSetParameterToSQLCommand("@address", SqlDbType.VarChar, model.address);
                sqlHelper.AddSetParameterToSQLCommand("@currency", SqlDbType.VarChar, model.currency);
                sqlHelper.AddSetParameterToSQLCommand("@cyb_mid", SqlDbType.VarChar, model.cyb_mid);                
                sqlHelper.AddSetParameterToSQLCommand("@cyb_transactionKeyEnc", SqlDbType.VarChar, enc.Encrypt(model.cyb_transactionKey) );
                sqlHelper.AddSetParameterToSQLCommand("@baseUrl", SqlDbType.VarChar, model.baseUrl);
                sqlHelper.AddSetParameterToSQLCommand("@bccEmailList", SqlDbType.VarChar, model.bccEmailList);
                sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_CreateMerchant");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";

                    }
                }

                return response;

            }
            catch (Exception ex)
            {

            }

            return response;

        }

        [HttpPut, Route("update")]
        public ApiResponse UpdateMerchant([FromBody]MerchantA model, int id)
        {
            ApiResponse response = new ApiResponse();
            try
            {

                IEnumerable<Claim> claims = User.Claims;
                var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;

                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);
                sqlHelper.AddSetParameterToSQLCommand("@merchantName", SqlDbType.VarChar, model.merchantName);
                sqlHelper.AddSetParameterToSQLCommand("@email", SqlDbType.VarChar, model.email);
                sqlHelper.AddSetParameterToSQLCommand("@phone", SqlDbType.VarChar, model.phone);
                sqlHelper.AddSetParameterToSQLCommand("@address", SqlDbType.VarChar, model.address);
                sqlHelper.AddSetParameterToSQLCommand("@currency", SqlDbType.VarChar, model.currency);
                sqlHelper.AddSetParameterToSQLCommand("@cyb_mid", SqlDbType.VarChar, model.cyb_mid);
                sqlHelper.AddSetParameterToSQLCommand("@cyb_transactionKeyEnc", SqlDbType.VarChar, enc.Encrypt(model.cyb_transactionKey));
                sqlHelper.AddSetParameterToSQLCommand("@baseUrl", SqlDbType.VarChar, model.baseUrl);
                sqlHelper.AddSetParameterToSQLCommand("@bccEmailList", SqlDbType.VarChar, model.bccEmailList);
                sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_UpdateMerchant");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";

                    }
                }

                return response;

            }
            catch (Exception ex)
            {

            }

            return response;

        }

        [HttpDelete, Route("delete")]
        public ApiResponse DeletePartner(int id)
        {
            ApiResponse response = new ApiResponse();
            try
            {
                IEnumerable<Claim> claims = User.Claims;
                var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;

                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);
                sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_DeleteMerchant");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";

                    }
                }

                return response;

            }
            catch (Exception ex)
            {

            }

            return response;

        }
    }
}
