using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using System.Security.Claims;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;

namespace LEShare.Admin.Controllers
{
   
    [Route("api/[controller]"), Authorize]
    public class PaymentRequestController : Controller
    {


        [HttpGet, Route("get-list")]
        public List<PaymentRequest> GetList(string date1,string date2)
        {
            List<PaymentRequest> requests = new List<PaymentRequest>();
            try
            {                                
                SqlHelper sqlHelper = new SqlHelper();                
                sqlHelper.AddSetParameterToSQLCommand("@date1", SqlDbType.DateTime, DateTime.Parse(date1));
                sqlHelper.AddSetParameterToSQLCommand("@date2", SqlDbType.DateTime, DateTime.Parse(date2));
               
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetRequestListA");

                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    requests.Add(new PaymentRequest
                    {
                        id = r["id"].ToString(),
                        reference = r["reference"].ToString(),
                        orderDetails = r["orderDetails"].ToString(),
                        orderTitle = r["orderTitle"].ToString(),
                        expiryDate = r["expiryDate"].ToString(),
                        createdDate = r["createdOn"].ToString(),
                        requestStatus = r["requestStatus"].ToString(),
                        history = r["history"].ToString(),
                        amount = r["amount"].ToString(),
                        partnerName = r["partnerName"].ToString(),
                        email = r["email"].ToString(),
                        merchantId = r["merchantId"].ToString(),
                        merchantName = r["merchantName"].ToString(),
                        partnerId = r["partnerId"].ToString(),
                        requestGuid = r["requestGuid"].ToString()
                    });

                }
            }
            catch (Exception ex)
            {

            }
            return requests;
        }

        [HttpGet, Route("get")]
        public PaymentRequest GetRequest(int id)
        {
            PaymentRequest request = new PaymentRequest();
            try
            {
         

                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);
               
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetRequestA");

                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    request = new PaymentRequest
                    {
                        id = r["id"].ToString(),
                        reference = r["reference"].ToString(),
                        orderDetails = r["orderDetails"].ToString(),
                        orderTitle = r["orderTitle"].ToString(),
                        createdDate = r["createdOn"].ToString(),
                        expiryDate = r["expiryDate"].ToString(),
                        requestStatus = r["requestStatus"].ToString(),
                        history = r["history"].ToString(),
                        amount = r["amount"].ToString(),
                        partnerName = r["partnerName"].ToString(),
                        merchantName = r["merchantName"].ToString(),                        
                        email = r["email"].ToString(),
                        isExpired = r["isExpired"].ToString(),
                        requestGuid = r["requestGuid"].ToString(),
                        currency = r["currency"].ToString(),
                        merchantId = r["merchantId"].ToString(), 
                        partnerId = r["partnerId"].ToString(),
                        paymentLink =General.GetSettingsValue("paymentBaseUrl")+ r["requestKey"].ToString()

                    };

                }
            }
            catch (Exception ex)
            {

            }
            return request;
        }

        //[HttpPost, Route("create1")]
        //public ApiResponse CreateRequest1([FromBody]PaymentRequest model)
        //{
        //    ApiResponse response = new ApiResponse();
        //    try
        //    {
        //        IEnumerable<Claim> claims = User.Claims;
        //        var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;

        //        SqlHelper sqlHelper = new SqlHelper();

        //        sqlHelper.AddSetParameterToSQLCommand("@reference", SqlDbType.VarChar, model.reference);
        //        sqlHelper.AddSetParameterToSQLCommand("@orderDetails", SqlDbType.VarChar, model.orderDetails);
        //        sqlHelper.AddSetParameterToSQLCommand("@orderTitle", SqlDbType.VarChar, model.orderTitle);               
        //        sqlHelper.AddSetParameterToSQLCommand("@amount", SqlDbType.Float, model.amount);
        //        sqlHelper.AddSetParameterToSQLCommand("@partnerId", SqlDbType.Int, model.partnerId);
        //        sqlHelper.AddSetParameterToSQLCommand("@expiryDate", SqlDbType.DateTime, DateTime.Now.AddHours(model.expiryHours));
        //        sqlHelper.AddSetParameterToSQLCommand("@requestKey", SqlDbType.VarChar, Guid.NewGuid().ToString());
        //        sqlHelper.AddSetParameterToSQLCommand("@requestGuid", SqlDbType.VarChar, Guid.NewGuid().ToString());
        //        sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);
                

        //        DataSet ds = sqlHelper.GetDatasetByCommand("SP_CreateRequest");
        //        if (ds.Tables.Count > 0)
        //        {
        //            if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
        //            {
        //                response.ActionStatus = "SUCCESS";
        //                response.ActionResult = ds.Tables[0].Rows[0]["id"].ToString();


        //            }
        //        }

        //        return response;

        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return response;

        //}

        private IHostingEnvironment _hostingEnvironment;

        public PaymentRequestController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost, Route("create"), DisableRequestSizeLimit]
        public ApiResponse CreateRequest()
        {
            ApiResponse response = new ApiResponse();
            try
            {
                IEnumerable<Claim> claims = User.Claims;
                var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;
                var merchantId = claims.Where(c => c.Type == "merchantId").FirstOrDefault().Value;

                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@reference", SqlDbType.VarChar, Request.Form["reference"].ToString());
                sqlHelper.AddSetParameterToSQLCommand("@orderDetails", SqlDbType.VarChar, Request.Form["orderDetails"].ToString());
                sqlHelper.AddSetParameterToSQLCommand("@orderTitle", SqlDbType.VarChar, Request.Form["orderTitle"].ToString());
                sqlHelper.AddSetParameterToSQLCommand("@amount", SqlDbType.Float, Request.Form["amount"].ToString());
                sqlHelper.AddSetParameterToSQLCommand("@partnerId", SqlDbType.Int, Request.Form["partnerId"].ToString());
                sqlHelper.AddSetParameterToSQLCommand("@expiryDate", SqlDbType.DateTime, 
                    DateTime.Now.AddHours(Convert.ToInt32(Request.Form["expiryHours"].ToString())));
                sqlHelper.AddSetParameterToSQLCommand("@requestKey", SqlDbType.VarChar, Guid.NewGuid().ToString());
                sqlHelper.AddSetParameterToSQLCommand("@requestGuid", SqlDbType.VarChar, Guid.NewGuid().ToString());
                sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);
                sqlHelper.AddSetParameterToSQLCommand("@merchantId", SqlDbType.Int, merchantId);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_CreateRequest");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";
                        response.ActionResult = ds.Tables[0].Rows[0]["id"].ToString();
                        try
                        {
                            foreach (var file in Request.Form.Files)
                            {
                                string folderName = "attachments";
                                string webRootPath = _hostingEnvironment.WebRootPath;
                                string newPath = Path.Combine(webRootPath, folderName);
                                if (!Directory.Exists(newPath))
                                {
                                    Directory.CreateDirectory(newPath);
                                }
                                if (file.Length > 0)
                                {
                                    string fileGuid = Guid.NewGuid().ToString();
                                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                                    string savedFileName = fileGuid + "_" + fileName;
                                    string fullPath = Path.Combine(newPath, savedFileName);
                                    using (var stream = new FileStream(fullPath, FileMode.Create))
                                    {
                                        file.CopyTo(stream);

                                        SqlHelper sqlHelperA = new SqlHelper();
                                        sqlHelperA.AddSetParameterToSQLCommand("@requestId", SqlDbType.Int, ds.Tables[0].Rows[0]["id"].ToString());
                                        sqlHelperA.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);
                                        sqlHelperA.AddSetParameterToSQLCommand("@attachmentDisplayName", SqlDbType.VarChar, fileName);
                                        sqlHelperA.AddSetParameterToSQLCommand("@attachmentName", SqlDbType.VarChar, savedFileName);
                                        DataSet dsA = sqlHelperA.GetDatasetByCommand("SP_CreateAttachment");
                                    }
                                }
                            }
                        }
                        catch
                        {

                        }
                    }
                }

               
              

            }
            catch (Exception ex)
            {
                response.ActionStatus="EXCEPTION";
            }
            return response;
        }


        [HttpPut, Route("deactivate")]
        public ApiResponse DeactivateRequest(int id)
        {
            ApiResponse response = new ApiResponse();
            try
            {

                IEnumerable<Claim> claims = User.Claims;
                var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;

                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);               
                sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_DeactivateRequest");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";

                    }
                }

                return response;

            }
            catch (Exception ex)
            {

            }

            return response;

        }

        [HttpPut, Route("send")]
        public ApiResponse SendRequest(int id)
        {
            ApiResponse response = new ApiResponse();
            try
            {
                //fetch request details, partner details from db
                PaymentRequest request = new PaymentRequest();
                request = this.GetRequest(id);

                if ((request.requestStatus != "PAID") && (request.requestStatus != "DEACTIVATED")
                    &&(DateTime.Now<DateTime.Parse(request.expiryDate)))
                {
                    //send request as email to partner
                    string emailSubject = "";
                    emailSubject = request.orderTitle;                                    

                    string body = General.GetSettingsValue("paymentrequest_email_template");
                    body = body.Replace("#PARTNER_NAME#", request.partnerName);
                    body = body.Replace("#PAYMENT_LINK#", request.paymentLink);
                    body = body.Replace("#INVOICE_REFERENCE#", request.reference);
                    body = body.Replace("#DETAILS#", request.orderDetails);
                    body = body.Replace("#AMOUNT#", request.amount);
                    body = body.Replace("#CURRENCY#", request.currency);
                    body = body.Replace("#MERCHANT_NAME#", request.merchantName);

                    string fromEmail = General.GetSettingsValue("fromEmail");
                    string fromDisplayName = General.GetSettingsValue("fromDisplayName");

                    string folderName = "attachments";
                    string webRootPath = _hostingEnvironment.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName);

                    List<MailAttachment> maList = new List<MailAttachment>();
                    SqlHelper sqlHelperA = new SqlHelper();
                    sqlHelperA.AddSetParameterToSQLCommand("@requestId", SqlDbType.Int, id);
                    DataSet dsA = sqlHelperA.GetDatasetByCommand("SP_GetAttachmentList");

                    foreach (DataRow r in dsA.Tables[0].Rows)
                    {
                        string fullPath = Path.Combine(newPath, r["attachmentName"].ToString());

                        maList.Add(new MailAttachment { AttachmentDisplayName = r["attachmentDisplayName"].ToString()
                            , AttachmentName = r["attachmentName"].ToString(), FullPath=fullPath
                        });;                     

                    }


                    if (General.SendEmail(emailSubject, body, fromEmail, fromDisplayName, request.email, maList))
                    {
                        //update sent request in db
                        IEnumerable<Claim> claims = User.Claims;
                        var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;

                        SqlHelper sqlHelper = new SqlHelper();

                        sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);
                        sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);

                        DataSet ds = sqlHelper.GetDatasetByCommand("SP_UpdateSentRequest");
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                            {
                                response.ActionStatus = "SUCCESS";

                            }
                        }
                    }
                }
               

               

                return response;

            }
            catch (Exception ex)
            {
               
            }

            return response;

        }
    }
}
