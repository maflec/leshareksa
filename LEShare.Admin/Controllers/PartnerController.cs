using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using System.Security.Claims;

namespace LEShare.Admin.Controllers
{
   
    [Route("api/[controller]"), Authorize]
    public class PartnerController : Controller
    {
        [HttpGet, Route("get")]
        public Partner GetPartner(int id)
        {
            Partner partner = new Partner();
            try
            {
                

                SqlHelper sqlHelper = new SqlHelper();
                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);           
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetPartnerA");
               
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    partner=new Partner
                    {
                        id = r["id"].ToString(),
                        partnerName = r["partnerName"].ToString(),
                        email = r["email"].ToString(),
                        phone = r["phone"].ToString(),
                        address = r["address"].ToString(),
                        merchantName = r["merchantName"].ToString(),
                        merchantId = r["merchantId"].ToString()

                    };

                }
            }
            catch (Exception ex)
            {

            }
            return partner;
        }

        [HttpGet, Route("get-list")]
        public List<Partner> GetList()
        {
            List<Partner> partners = new List<Partner>();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();             
                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetPartnerListA");

                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    partners.Add(new Partner
                    {
                        id = r["id"].ToString(),
                        partnerName =r["partnerName"].ToString(),
                        email = r["email"].ToString(),
                        phone = r["phone"].ToString(),
                        address = r["address"].ToString(),
                        merchantName = r["merchantName"].ToString(),
                        merchantId = r["merchantId"].ToString()

                    });

                }
            }
            catch (Exception ex)
            {

            }
            return partners;
        }

        [HttpPost, Route("create")]
        public ApiResponse CreatePartner([FromBody]Partner model)
        {
            ApiResponse response = new ApiResponse();
            try
            {
                IEnumerable<Claim> claims = User.Claims;
                var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;
                var merchantId = claims.Where(c => c.Type == "merchantId").FirstOrDefault().Value;

                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@partnerName", SqlDbType.VarChar, model.partnerName);
                sqlHelper.AddSetParameterToSQLCommand("@email", SqlDbType.VarChar, model.email);
                sqlHelper.AddSetParameterToSQLCommand("@phone", SqlDbType.VarChar, model.phone);
                sqlHelper.AddSetParameterToSQLCommand("@address", SqlDbType.VarChar, model.address);
                sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);
                sqlHelper.AddSetParameterToSQLCommand("@merchantId", SqlDbType.Int, merchantId);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_CreatePartner");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";

                    }
                }

                return response;

            }
            catch (Exception ex)
            {

            }

            return response;

        }

        [HttpPut, Route("update")]
        public ApiResponse UpdatePartner([FromBody]Partner model, int id)
        {
            ApiResponse response = new ApiResponse();
            try
            {

                IEnumerable<Claim> claims = User.Claims;
                var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;

                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);
                sqlHelper.AddSetParameterToSQLCommand("@partnerName", SqlDbType.VarChar, model.partnerName);
                sqlHelper.AddSetParameterToSQLCommand("@email", SqlDbType.VarChar, model.email);
                sqlHelper.AddSetParameterToSQLCommand("@phone", SqlDbType.VarChar, model.phone);
                sqlHelper.AddSetParameterToSQLCommand("@address", SqlDbType.VarChar, model.address);
                sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_UpdatePartner");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";

                    }
                }

                return response;

            }
            catch (Exception ex)
            {

            }

            return response;

        }

        [HttpDelete, Route("delete")]
        public ApiResponse DeletePartner(int id)
        {
            ApiResponse response = new ApiResponse();
            try
            {
                IEnumerable<Claim> claims = User.Claims;
                var userId = claims.Where(c => c.Type == "id").FirstOrDefault().Value;

                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@id", SqlDbType.Int, id);
                sqlHelper.AddSetParameterToSQLCommand("@userId", SqlDbType.Int, userId);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_DeletePartner");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.ActionStatus = "SUCCESS";

                    }
                }

                return response;

            }
            catch (Exception ex)
            {

            }

            return response;

        }
    }
}
