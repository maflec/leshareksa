﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;


namespace LEShare.Client
{
    public partial class frmLogin : Form
    {
        string  filePath;
        string applicationName;
        string conStr;
        private bool ignoreClick = false;
        public frmLogin()
        {
            InitializeComponent();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            // to start the screen on right top
            //OpenWindowRightTOp();
            // to start the screen on right top

        }

        private void OpenWindowRightTOp()
        {
            try
            {
                this.StartPosition = FormStartPosition.Manual;
                foreach (var scrn in Screen.AllScreens)
                {
                    if (scrn.Bounds.Contains(this.Location))
                    {
                        this.Location = new Point(scrn.Bounds.Right - this.Width, scrn.Bounds.Top);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.Message);
                Common.LogException(ex);
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {

            try
            {
                if (txt_password.Text.Trim().Length > 0)
                {
                    //get connection string and configs
                    //get application id
                    filePath = ConfigurationManager.AppSettings["ScriptsPath"].ToString()
                      + "Get_Application_Id.mp";
                    var script = System.IO.File.ReadAllText
                        (filePath);
                    script = script.Replace("#APPLICATION_NAME#", applicationName);

                    SqlHelper sqlHelper = new SqlHelper(Common.conStr);
                    DataSet ds = sqlHelper.GetDatasetBySQL(script);
                    if (ds.Tables.Count > 0)
                    {
                        DataRow r = ds.Tables[0].Rows[0];
                        Common.applicationId = Convert.ToInt32(r["Application_Id"].ToString());

                        //get location id
                        filePath = ConfigurationManager.AppSettings["ScriptsPath"].ToString()
                          + "Get_Location_Id.mp";
                        script = System.IO.File.ReadAllText
                            (filePath);
                        sqlHelper = new SqlHelper(conStr);
                        ds = sqlHelper.GetDatasetBySQL(script);
                        if (ds.Tables.Count > 0)
                        {
                            r = ds.Tables[0].Rows[0];
                            Common.locationId = Convert.ToInt32(r["Location_Id"].ToString());
                            Common.locationName = r["Location_Description"].ToString();
                            //get script
                            filePath = ConfigurationManager.AppSettings["ScriptsPath"].ToString()
                            + "Get_User_ID.mp";
                            script = System.IO.File.ReadAllText
                                (filePath);

                            //compute pwd hash
                            script = script.Replace("#PASSWORD_HASH#", Common.CreateHash(txt_password.Text));
                            sqlHelper = new SqlHelper(conStr);
                            ds = sqlHelper.GetDatasetBySQL(script);
                            if (ds != null)
                            {
                                if (ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        r = ds.Tables[0].Rows[0];
                                        if (r["Allow_Acces"].ToString() == "1")
                                        {
                                            Common.userId = Convert.ToInt32(r["User_Id"].ToString());
                                            Common.cashierName = r["User_Name"].ToString();
                                            Common.isLoggedIn = true;
                                            if (r["Elevated"].ToString() == "1")
                                            {
                                                Common.isElevatedUser = true;
                                            }
                                            else
                                            {
                                                Common.isElevatedUser = false;
                                            }


                                            this.Close();
                                            frmShare frm = new frmShare();
                                            frm.Show(this.Parent);
                                            Common.frmShareParked = this;

                                        }
                                        else
                                        {
                                            
                                            MessageBoxEx.Show("Password error");

                                        }
                                    }
                                    else
                                    {
                                        MessageBoxEx.Show("Password error");

                                    }
                                }
                                else
                                {
                                    MessageBoxEx.Show("Password error");

                                }
                            }
                            else
                            {
                                MessageBoxEx.Show("Password error");

                            }
                            
                        }
                        else
                        {
                            MessageBoxEx.Show("Location error");
                        }
                    }
                    else
                    {
                        MessageBoxEx.Show("Application Id error");

                    }
                }
                else
                {
                    MessageBoxEx.Show("Please enter password");
                  
                }
              

            }
            catch(Exception ex)
            {
                MessageBoxEx.Show(ex.Message);
                Common.LogException(ex);
            }
            finally
            {
                txt_password.Focus();

            }

        }

        private void Login_Load(object sender, EventArgs e)
        {
            try
            {
                lblVersion.Text = "V"+Application.ProductVersion;
                

                filePath = ConfigurationManager.AppSettings["EmbedConfigPath"].ToString()
                 + "Database.loc";
                string[] confLines = System.IO.File.ReadAllLines
                    (filePath);

                string str = confLines[0].ToString();
                if (str[str.Length - 1].ToString() == ";")
                {
                    conStr = confLines[0] + "Persist Security Info=True;Integrated Security=SSPI;";
                }
                else
                {
                    conStr = confLines[0] + ";Persist Security Info=True;Integrated Security=SSPI;";
                }
                
                
                Common.conStr = conStr;
                var application = confLines[1];
                applicationName = application.Split('=')[1];
                Common.applicationName = applicationName;

                if (ConfigurationManager.AppSettings["Currency"]!=null)
                {
                    Common.country = ConfigurationManager.AppSettings["Currency"].ToString();
                }
                else
                {
                    Common.country = "AED";
                }

                lbl_posname.Text = applicationName;

                txt_password.Focus();
                txt_password.Select();
            }
            catch(Exception ex)
            {
                MessageBoxEx.Show(ex.Message);
                Common.LogException(ex);
            }
           
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lbl_header_close_Click(object sender, EventArgs e)
        {

            if (!ignoreClick) {
                this.Close();
            }
            
        }

        private void pnl_toolbar_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ignoreClick = true;
                var Screendim = Screen.FromControl(this).Bounds;
                var MaxX = Screendim.Width - this.Width;
                var MaxY = Screendim.Height - this.Height;

                var X = Cursor.Position.X;
                var Y = Cursor.Position.Y;

                if (Cursor.Position.X >= MaxX)
                {
                    X = MaxX;
                }
                if (Cursor.Position.Y >= MaxY)
                {
                    Y = MaxY;
                }

                this.Location = new Point(X, Y);
            }
        }
        private void btn_MouseUp(object sender, MouseEventArgs e)
        {
            ignoreClick = false;
        }

        private void LblVersion_Click(object sender, EventArgs e)
        {

        }
    }
}
