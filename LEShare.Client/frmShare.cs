﻿using System;

using System.Drawing;

using System.Windows.Forms;

using LEShare.Client.MPlanet;
using LEShare.Client.MPlanet.Member;
using LEShare.Client.MPlanet.Receipt;
using LEShare.Client.MPlanet.Burn;
using LEShare.Client.MPlanet.Earn;
using System.Net;
using System.Configuration;
using System.Data;

namespace LEShare.Client
{
    public partial class frmShare : Form
    {
        MPlanetManager mpm = new MPlanetManager();
        MPTransResponse burnResponse ;
        MPTransResponse earnResponse;
        MPTransResponse refundResponse;
        MemberResponse memberResponse ;
        ReceiptResponse receiptResponse;
        int maxReceiptNumber;
        string shareId;
        private bool ignoreClick = false;
        void StartOver()
        {
             mpm = new MPlanetManager();
             burnResponse = null;
             earnResponse =null;
             memberResponse=null;
             receiptResponse = null;
             refundResponse = null;
             maxReceiptNumber =0;
             shareId = string.Empty ;
            txt_burnAmunt.Text = string.Empty;
            txt_barcode.Text = string.Empty;
            txt_receiptnumber.Text = string.Empty;
            lbl_result_receipt.Text = string.Empty;
            txt_password_approval.Text = string.Empty;
            timer.Enabled = false;
            lbl_resultBurn.Text = "0.00";
            lbl_resultEarn.Text = "0.00";
            ShowScanBarcode();
            ShowPanel(pnl_earn_burn);
            pnl_hg_tab_ref.Visible = false;
            pnl_hg_tab_eb.Visible = true;
        }

        public frmShare()
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => { return true; };

            InitializeComponent();
            lbl_member.ForeColor = Color.FromArgb(171, 146, 88);
            pnl_hg_tab_eb.BackColor = Color.FromArgb(171, 146, 88);
            pnl_hg_tab_ref.BackColor = Color.FromArgb(171, 146, 88);
            this.ShowInTaskbar = false;
            ShowScanBarcode();

            // to start the screen on right top
            OpenWindowRightTOp();
            // to start the screen on right top

        }

        private void OpenWindowRightTOp()
        {
            try
            {
                this.StartPosition = FormStartPosition.Manual;
                foreach (var scrn in Screen.AllScreens)
                {
                    if (scrn.Bounds.Contains(this.Location))
                    {
                        this.Location = new Point(scrn.Bounds.Right - this.Width, scrn.Bounds.Top);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(ex.Message);
                Common.LogException(ex);
            }
        }

        private void ShowScanBarcode()
        {
            shareId = string.Empty;
            ShowPanel(pnl_scan_barcode);
            //txt_barcode.Clear();
            txt_barcode.Focus();
            txt_barcode.Select();
         
        }

        //void InitBrowser()
        //{


        //    try
        //    {

        //        //resizePanels();
               
        //       // this.pnlBrowser.Controls.Clear();
        //        browser = new ChromiumWebBrowser(
        //           wConfiguration.WebAppBaseAddress

        //            )
        //        {
        //            Dock = DockStyle.Fill,

        //        };



        //        CefSharpSettings.LegacyJavascriptBindingEnabled = true;
        //        CefSharpSettings.WcfEnabled = true;
        //        browser.JavascriptObjectRepository.Register("bound", new BoundObject(), isAsync: false, options: BindingOptions.DefaultBinder);

        //        browser.MenuHandler = new MenuHandler();
        //        browser.BrowserSettings.WebSecurity = CefState.Disabled;
        //        browser.LoadingStateChanged += Browser_LoadingStateChanged;



        //      //  this.pnlBrowser.Controls.Add(browser);

        //        //browser.Refresh();

        //    }
        //    catch (Exception ex)
        //    {
        //        Common.LogException(ex);
        //    }



        //}

        //private void Browser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        //{


        //    if (!e.IsLoading)
        //    {


        //        //this.pnlBrowser.Visible = true;

        //        wConfiguration.ApiBaseAddress = ConfigurationManager.AppSettings["ApiBaseAddress"].ToString();
        //        wConfiguration.ApiToken = ConfigurationManager.AppSettings["ApiToken"].ToString();



        //        frmShare.browser.GetMainFrame().ExecuteJavaScriptAsync
        //             (String.Format("setConfig('{0}')", JsonConvert.SerializeObject(wConfiguration)));


        //    }
        //    else
        //    {


        //        //this.pnlBrowser.Visible = false;
        //    }
        //}
        private void FrmShare_Load(object sender, EventArgs e)
        {
            try
            {
                lblVersion.Text = "V" + Application.ProductVersion;
                lbl_cashierName.Text = Common.cashierName;
                
                ShowScanBarcode();
            }
            catch (Exception ex)
            {
                Common.LogException(ex);
            }
            finally {
               
            }
        }

      

        private void lbl_cashierName_Click(object sender, EventArgs e)
        {
            if (!ignoreClick) {
                var res = MessageBoxEx.Show(this, "Are you sure to logout from session ?", "Confirm Logout",
            MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (res == DialogResult.Yes)
                {
                    Common.ClearAccess();
                    this.Close();
                }
            }
            

          
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
           
        }

        private void panel2_Click(object sender, EventArgs e)
        {
            pnl_earn_burn.Visible = true;
            pnl_refund.Visible = false;

            lbl_member.ForeColor = Color.FromArgb(171, 146, 88);
            lbl_refund.ForeColor = Color.FromArgb(255, 255, 255);
            pnl_hg_tab_ref.Visible = false; 
            pnl_hg_tab_eb.Visible = true;
        }


        private void panel3_Click(object sender, EventArgs e)
        {
            lbl_pointsBurned.Text = Common.country+" 0.00";
            pnl_earn_burn.Visible = false;
            pnl_refund.Visible = true;
            lbl_member.ForeColor = Color.FromArgb(255, 255, 255);
            lbl_refund.ForeColor = Color.FromArgb(171, 146, 88);
            txt_receiptnumber.Focus();
            txt_receiptnumber.Select();
            pnl_hg_tab_ref.Visible = true;
            pnl_hg_tab_eb.Visible = false;
        }

        private void pnl_memberProfile_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnl_wait_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lbl_wait_Click(object sender, EventArgs e)
        {

        }

        private void pnl_balance_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lbl_do_you_Click(object sender, EventArgs e)
        {

        }

        private void btn_yes_Click(object sender, EventArgs e)
        {
            HidePanel(pnl_Burn);
            ShowPanel(pnl_Burn_amount);
            txt_burnAmunt.Focus();
        }

        private void pnl_approval_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }

       

        private void txt_barcode_KeyDown(object sender, KeyEventArgs e)   
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    maxReceiptNumber=0;
                    if (getMaxReceipt())
                    {
                        //MessageBoxEx.Show(txt_barcode.Text);
                        shareId = txt_barcode.Text;
                        LoadMember();
                    }
                    

                }
            }
            catch(Exception ex)
            {
                Common.LogException(ex);
            }
            
        }

      

        void DisplayMember()
        {
            try 
            { 
                lbl_shareid.Text = memberResponse.data.shareId;
                lbl_name.Text = memberResponse.data.firstName + " " + memberResponse.data.lastName;
                lbl_points.Text = memberResponse.data.pointBalance.ToString();
                label4.Text = memberResponse.data.amountBalance.ToString()
                    + " " + memberResponse.data.balanceCurrency;
            }
            catch (Exception ex)
            {
                Common.LogException(ex);//
            }
        }
        private void ShowPanel( Panel pnl)
        {
            if (pnl.Name == "pnl_Burn")
            {
                lbl_burn_success.Text = "";
            }

            pnl.BringToFront();
            pnl.Visible = true;
        }
        private void HidePanel(Panel pnl)
        {
            pnl.SendToBack();
            pnl.Visible = false; ;
        }
        private void ShowLoader()
        {
            pnl_loader.BringToFront();
            pnl_loader.Visible = true; 
        }
        private void HideLoader()
        {
            pnl_loader.SendToBack();
            pnl_loader.Visible = false;
        }

        private void btn_no_Click(object sender, EventArgs e)
        {
            waitForEmbed();
        }

        private void btn_burn_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_burnAmunt.Text.Trim().Length > 0)
                {
                    if (float.Parse(txt_burnAmunt.Text) <= memberResponse.data.amountBalance)
                    {
                        Burn();
                    }
                    else
                    {
                        MessageBoxEx.Show("You cannot burn more than "+
                            memberResponse.data.balanceCurrency+" "+memberResponse.data.amountBalance.ToString());
                    }


                }
                else
                {
                    MessageBoxEx.Show("Please enter a valid amount");
                }
               

                
            }
            catch (Exception ex)
            {
                Common.LogException(ex);
            }

            
        }

        private void pic_wait_Click(object sender, EventArgs e)
        {
            HidePanel(pnl_wait);
            ShowPanel(pnl_result);
        }

        private void btn_refund_Click(object sender, EventArgs e)
        {
            if (Common.isElevatedUser)
            {
                Refund(Common.userId,Common.cashierName);
            }
            else
            {
                //show approval
                ShowPanel(pnl_approval);
                txt_password_approval.Text = string.Empty;
                txt_password_approval.Focus();
                txt_password_approval.Select();
            }
            
        }

        private void btn_loginApproval_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_password_approval.Text.Trim().Length > 0)
                {
                    var filePath = ConfigurationManager.AppSettings["ScriptsPath"].ToString()
                      + "Get_User_ID.mp";
                    var script = System.IO.File.ReadAllText
                        (filePath);

                    //compute pwd hash

                    script = script.Replace("#PASSWORD_HASH#", Common.CreateHash(txt_password_approval.Text));

                    SqlHelper sqlHelper = new SqlHelper(Common.conStr);
                    DataSet ds = sqlHelper.GetDatasetBySQL(script);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow r = ds.Tables[0].Rows[0];
                                if (r["Allow_Acces"].ToString() == "1")
                                {
                                    var userId = Convert.ToInt32(r["User_Id"].ToString());
                                    var cashierName = r["User_Name"].ToString();


                                    if (r["Elevated"].ToString() == "1")
                                    {
                                        Refund(userId, cashierName);
                                        HidePanel(pnl_approval);
                                    }
                                    else
                                    {
                                        MessageBoxEx.Show("Password error");

                                    }



                                }
                                else
                                {
                                    MessageBoxEx.Show("Password error");

                                }

                            }
                            else
                            {
                                MessageBoxEx.Show("Password error");

                            }
                        }
                        else
                        {
                            MessageBoxEx.Show("Password error");

                        }
                    }
                    else
                    {
                        MessageBoxEx.Show("Password error");

                    }
                }
                else
                {
                    MessageBoxEx.Show("Please enter password");
                }
                txt_password_approval.Text = string.Empty;
            }
            catch(Exception ex)
            {
                Common.LogException(ex);
            }
            finally
            {
                txt_password_approval.Focus();

            }
        }

        private void txt_burnAmunt_KeyPress(object sender, KeyPressEventArgs e)
            {
                // Verify that the pressed key isn't CTRL or any non-numeric digit
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

                // If you want, you can allow decimal (float) numbers
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }
            }

        private void txt_receiptnumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verify that the pressed key isn't CTRL or any non-numeric digit
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void FrmShare_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (Common.isLoggedIn)
            //{
            //    var res = MessageBoxEx.Show(this, "Are you sure to close ?", "Confirm Closing",
            //MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            //    if (res != DialogResult.Yes)
            //    {
            //        e.Cancel = true;
            //        return;
            //    }
            //}

            
            e.Cancel = true;
            this.Hide();
            Common.frmShareParked = this;


        }

        private void Btn_bunCancel_Click(object sender, EventArgs e)
        {
            waitForEmbed();
        }

        private void Btn_abort_Click(object sender, EventArgs e)
        {
            var res = MessageBoxEx.Show(this, "Are you sure to abort waiting ?", "Confirm Abort",
          MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (res == DialogResult.Yes)
            {
                timer.Enabled = false ;
                HidePanel(pnl_wait);
                ShowPanel(pnl_result);
            }

        }

        private void ResetControls()
        {          
            HidePanel(pnl_profile);
            ShowPanel(pnl_scan_barcode);
            txt_barcode.Text = "9478400000598132";
            txt_barcode.Select();
            txt_barcode.Focus();

        }

        private void waitForEmbed()
        {
            HidePanel(pnl_Burn);
            ShowPanel(pnl_wait);
            timer.Enabled = true ;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {

                var tmp_maxReceipt = maxReceiptNumber;
                if (getMaxReceipt())
                {

                    if (maxReceiptNumber>tmp_maxReceipt)
                    {
                        timer.Enabled = false;
                        //MessageBoxEx.Show(tmp_maxReceipt + ":" + maxReceiptNumber.ToString());
                       
                        Earn(maxReceiptNumber);
                        //earn
                    }
                }
            }
            catch(Exception ex)
            {
                Common.LogException(ex);
            }
        }

        int i = 0;
        private bool getMaxReceipt()
        {
            try
            {
                var filePath = ConfigurationManager.AppSettings["ScriptsPath"].ToString()
                   + "Get_MaxReceiptByPosID.mp";
                var script = System.IO.File.ReadAllText
                    (filePath);
                //script = script.Replace("#RECEIPT_NO#", maxReceiptNumber.ToString());
                script = script.Replace("#APPLICATION_ID#", Common.applicationId.ToString());
                script = script.Replace("#LOCATION_ID#", Common.locationId.ToString());

                //if (i++ <= 2)
                //{
                //    script = script.Replace("#RECEIPT_NO#", "7039004"); 
                //}
                //else
                //{
                //    script = script.Replace("#RECEIPT_NO#", "7041058");
                //}


                SqlHelper sqlHelper = new SqlHelper(Common.conStr);
                DataSet ds = sqlHelper.GetDatasetBySQL(script);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow r = ds.Tables[0].Rows[0];
                        maxReceiptNumber = Convert.ToInt32(r["Receipt_No"].ToString());

                    }
                    else
                    {
                        maxReceiptNumber = 0;
                    }
                   
                    return true;
                }
            }
            catch (Exception ex)
            {
                Common.LogException(ex);
            }
            return false;
        }


        async void Earn(int receiptNumber)
        {
            try
            {
                ShowLoader();
                lbl_result_receipt.Text = "Receipt #" + receiptNumber.ToString();
                //get last receipt header and lines
                var filePath = ConfigurationManager.AppSettings["ScriptsPath"].ToString()
                  + "Get_Receipt.mp";
                var script = System.IO.File.ReadAllText
                    (filePath);
               
                script = script.Replace("#RECEIPT_NO#", receiptNumber.ToString());


                SqlHelper sqlHelper = new SqlHelper(Common.conStr);
                DataSet ds = sqlHelper.GetDatasetBySQL(script);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow r = ds.Tables[0].Rows[0];

                        var payment_Amount = float.Parse(r["Payment_Amount"].ToString());
                        //earn
                        if (payment_Amount > 0)
                        {
                            EarnRequest request = new EarnRequest();
                            request.receipt_No = receiptNumber;
                            request.payment_Amount = payment_Amount;
                            request.machine_Id = Common.applicationId;
                            request.cashier_Id = Common.userId;
                            request.machine_Name = Common.applicationName;
                            request.cashier_Name = Common.cashierName;
                            request.share_Id = shareId;
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                if (ds.Tables.Count > 1)
                                {
                                    request.lines = new EarnLineItem[ds.Tables[1].Rows.Count];
                                    //request.payments = new Payment_Details[ds.Tables[1].Rows.Count];

                                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                                    {
                                        request.lines[i] = new EarnLineItem
                                        {
                                            earn_Line_Id = ds.Tables[1].Rows[i]["Earn_Line_Id"].ToString(),
                                            receipt_No = Convert.ToInt32(ds.Tables[1].Rows[i]["Receipt_No"].ToString()),
                                            sequence_Number = Convert.ToInt32(ds.Tables[1].Rows[i]["Sequence_Number"].ToString()),
                                            transaction_DateTime = DateTime.Parse(ds.Tables[1].Rows[i]["Transaction_Datetime"].ToString()),
                                            card_Barcode = ds.Tables[1].Rows[i]["Card_Barcode"].ToString(),
                                            new_Card_Flag = bool.Parse(ds.Tables[1].Rows[i]["New_Card_Flag"].ToString()),
                                            card_Type = ds.Tables[1].Rows[i]["Card_Type"].ToString(),
                                            product_Category = ds.Tables[1].Rows[i]["Product_Category"].ToString(),
                                            product_Code = ds.Tables[1].Rows[i]["Product_Code"].ToString(),
                                            product_Name = ds.Tables[1].Rows[i]["Product_Name"].ToString(),
                                            unit_Price = float.Parse(ds.Tables[1].Rows[i]["Unit_Price"].ToString()),
                                            tax = float.Parse(ds.Tables[1].Rows[i]["Tax"].ToString()),
                                            quantity = Convert.ToInt32(ds.Tables[1].Rows[i]["Quantity"].ToString()),
                                            payment_Type = ds.Tables[1].Rows[i]["Payment_Type"].ToString()

                                        };

                                        ////request.payments[i] = new Payment_Details
                                        ////{
                                        ////    payment_method = ds.Tables[1].Rows[i]["Payment_Type"].ToString(),
                                        ////    amount = Convert.ToInt32(ds.Tables[1].Rows[i]["Quantity"].ToString())*
                                        ////                float.Parse(ds.Tables[1].Rows[i]["Unit_Price"].ToString())


                                        ////};
                                    }

                                    if (ds.Tables.Count > 2)
                                    {
                                        if (ds.Tables[2].Rows.Count > 0)
                                        {
                                            request.payments = new Payment_Details[ds.Tables[2].Rows.Count];// new Payment_Details[ds.Tables[2].Rows.Count];
                                                                                                            //add payment lines

                                            for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                                            {
                                                request.payments[i] = new Payment_Details
                                                {
                                                    payment_method = ds.Tables[2].Rows[i]["Payment_Type"].ToString(),
                                                    amount = float.Parse(ds.Tables[2].Rows[i]["Payment_Amount"].ToString())

                                                };
                                            }
                                        }
                                    }

                                }

                            }


                            earnResponse = await mpm.Earn(request);

                            if (earnResponse.status == "SUCCESS")
                            {

                                memberResponse = await mpm.GetMember(shareId);
                                HidePanel(pnl_wait);
                                ShowPanel(pnl_result);
                                lbl_resultEarn.Text = "Earned " + earnResponse.data.rewarded + " Points";
                                lbl_oldBalance.Text = earnResponse.data.old_balance.ToString();
                                lbl_newBalance.Text = earnResponse.data.new_balance.ToString();

                                DisplayMember();

                                HideLoader();
                                //MessageBoxEx.Show("EARN BIT : " + earnResponse.data.bit_id);

                            }
                            else
                            {
                                HideLoader();
                            }
                        }
                        else
                        {
                            HidePanel(pnl_wait);
                            ShowPanel(pnl_result);
                        }
                       


                    }
                    else
                    {
                        HidePanel(pnl_wait);
                        ShowPanel(pnl_result);
                    }


                }
                else
                {
                    HidePanel(pnl_wait);
                    ShowPanel(pnl_result);
                }
                UpdateBurn(receiptNumber);

                //add receipt to burn
                //display final result




            }
            catch (Exception ex)
            {
                
                Common.LogException(ex);
            }


            HideLoader();

        }

        async void UpdateBurn(int receiptNumber)
        {
            try
            {
                if (burnResponse != null)
                {
                    UpdateBurnRequest request = new UpdateBurnRequest();

                    request.Receipt_No = receiptNumber;
                    request.mpTrans_id = burnResponse.data.mpTrans_id;

                    burnResponse = await mpm.UpdateBurn(request);
                }
                
            }
            catch (Exception ex)
            {
                HideLoader();
                Common.LogException(ex);
            }
        }

        async void Burn()
        {
            try
            {
                ShowLoader();
              
                BurnRequest request = new BurnRequest();

                request.Burn_Amount = float.Parse(txt_burnAmunt.Text);
                request.machine_Id = Common.applicationId;
                request.cashier_Id = Common.userId;
                request.machine_Name = Common.applicationName;
                request.cashier_Name = Common.cashierName;
                request.share_Id = shareId;
                request.Payment_Type = "CASH";
                request.Card_Barcode = "";

                burnResponse = await mpm.Burn(request);

                if (burnResponse.status == "SUCCESS")
                {
                    lbl_resultBurn.Text = "Burned "+ memberResponse.data.balanceCurrency 
                        + " "+ request.Burn_Amount.ToString();
                    lbl_oldBalance.Text = burnResponse.data.old_balance.ToString();
                    lbl_newBalance.Text = burnResponse.data.new_balance.ToString();
                    pnl_Burn_amount.SendToBack();
                    memberResponse = await mpm.GetMember(shareId);

                    lbl_burn_success.Text = "BURNED "+Common.country+" " + request.Burn_Amount.ToString();
                    DisplayMember();
                    waitForEmbed();

                    //MessageBoxEx.Show("BURN BIT : "+burnResponse.data.bit_id);
                   
                }
                else
                {
                    MessageBoxEx.Show(burnResponse.message, burnResponse.status);
                }
                



            }
            catch (Exception ex)
            {
                HideLoader();
                Common.LogException(ex);
            }

            HideLoader();


        }

        async void Refund(int userId,string cashierName)
        {
            try
            {
                ShowLoader();
                if (receiptResponse != null)
                {
                    RefundRequest request = new RefundRequest();

                    request.receipt_No = Convert.ToInt32(txt_receiptnumber.Text.Trim());
                    request.machine_Id = Common.applicationId;
                    request.cashier_Id = userId;
                    request.machine_Name = Common.applicationName;
                    request.cashier_Name = cashierName;


                    refundResponse = await mpm.Refund(request);

                    if (refundResponse.status == "SUCCESS")
                    {

                        MessageBoxEx.Show(refundResponse.message);

                        ClearReceipt();
                        txt_receiptnumber.Text = string.Empty;

                    }
                    else
                    {
                        MessageBoxEx.Show(refundResponse.message, refundResponse.status);
                    }

                }
                else
                {
                    MessageBoxEx.Show("Please select a receipt");
                }



            }
            catch (Exception ex)
            {
                HideLoader();
                Common.LogException(ex);
            }

            HideLoader();


        }

        async void LoadMember()
        {
            try
            {
                MPlanetManager mpm = new MPlanetManager();
                //GetMember(txt_barcode.Text.Trim());
                ShowLoader();
                memberResponse = await mpm.GetMember(txt_barcode.Text.Trim());

                

                if (memberResponse.status == "SUCCESS")
                {
                    //copy to clipboard


                    if(memberResponse.data.membershipStage== "Active")
                    {
                        DisplayMember();
                        shareId = memberResponse.data.shareId;
                        HidePanel(pnl_scan_barcode);
                        ShowPanel(pnl_memberProfile);
                        ShowPanel(pnl_balance);
                        ShowPanel(pnl_Burn);                        
                        ShowPanel(pnl_profile);

                        try
                        {
                            Clipboard.SetText("SHARE         " + shareId);
                        }
                        catch
                        {

                        }
                    }
                    else
                    {
                        MessageBoxEx.Show("Not an active member");
                        txt_barcode.Text = string.Empty;
                    }

                }
                else
                {
                    MessageBoxEx.Show("Could not load member details");
                }

                txt_barcode.Text = string.Empty;



            }
            catch (Exception ex)
            {
                txt_barcode.Text = string.Empty;
                MessageBoxEx.Show(ex.Message);
                Common.LogException(ex);
            }
            
            HideLoader();
            
        }

        void ClearReceipt()
        {
            txt_password_approval.Text = string.Empty;
            lbl_receiptHeader.Text = "RECEIPT #";
            lbl_pointsEarned.Text = "0.00";
            lbl_pointsBurned.Text = "0.00";
            receiptResponse = null;
            refundResponse = null;
        }
        async void LoadReceipt()
        {
            try
            {
                MPlanetManager mpm = new MPlanetManager();
                //GetMember(txt_barcode.Text.Trim());
                ShowLoader();
                receiptResponse = await mpm.GetReceipt(txt_receiptnumber.Text.Trim());

                if (receiptResponse.status == "SUCCESS")
                {
                    lbl_receiptHeader.Text = "RECEIPT #"+ txt_receiptnumber.Text.Trim();
                    lbl_pointsEarned.Text = receiptResponse.data.earnpoints.ToString();
                    lbl_pointsBurned.Text = receiptResponse.currency.ToString() + " " + receiptResponse.data.burnAmount.ToString();
              
                }
                else
                {
                    MessageBoxEx.Show("Could not load receipt details");
                }


            }
            catch (Exception ex)
            {

                Common.LogException(ex);
            }
            HideLoader();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

            if (!ignoreClick) {
                var res = MessageBoxEx.Show(this, "Are you sure to start over ?", "Confirm Start Over",
         MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (res == DialogResult.Yes)
                {
                    StartOver();
                }
            }
            
        }

        private void BtnStartOver_Click(object sender, EventArgs e)
        {
            StartOver();
            this.Hide();
            Common.frmShareParked = this;
        }

        private void Lbl_pointsEarnedtitle_Click(object sender, EventArgs e)
        {

        }

        private void Btn_loadReceipt_Click(object sender, EventArgs e)
        {
            try
            {
                ClearReceipt();
                if (txt_receiptnumber.Text.Length > 0)
                {
                    LoadReceipt();
                }
                else
                {
                    MessageBoxEx.Show("Please enter a receipt number","Warning");
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void Btn_CloseApproval_Click(object sender, EventArgs e)
        {
            HidePanel(pnl_approval);
        }

        private void lbl_header_close_Click(object sender, EventArgs e)
        {

            if (!ignoreClick)
            {
                this.Hide();
                Common.frmShareParked = this;
            }

        }

        private void pnl_toolbar_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ignoreClick = true;
                var Screendim = Screen.FromControl(this).Bounds;
                var MaxX = Screendim.Width - this.Width;
                var MaxY = Screendim.Height - this.Height;

                var X = Cursor.Position.X;
                var Y = Cursor.Position.Y;

                if (Cursor.Position.X >= MaxX)
                {
                    X = MaxX;
                }
                if (Cursor.Position.Y >= MaxY)
                {
                    Y = MaxY;
                }

                this.Location = new Point(X, Y);
            }
        }

        private void pnl_refundForm_Paint(object sender, PaintEventArgs e)
        {

        }
        private void btn_MouseUp(object sender, MouseEventArgs e)
        {
            ignoreClick = false;
        }
    }

   
}
