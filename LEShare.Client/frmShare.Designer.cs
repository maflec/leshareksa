﻿namespace LEShare.Client
{
    partial class frmShare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmShare));
            this.lbl_cashierName = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnl_earn_burn = new System.Windows.Forms.Panel();
            this.pnl_profile = new System.Windows.Forms.Panel();
            this.pnl_memberProfile = new System.Windows.Forms.Panel();
            this.pnl_wait = new System.Windows.Forms.Panel();
            this.lbl_burn_success = new System.Windows.Forms.Label();
            this.pic_wait = new System.Windows.Forms.PictureBox();
            this.btn_abort = new System.Windows.Forms.Button();
            this.lbl_wait = new System.Windows.Forms.Label();
            this.pnl_memberName = new System.Windows.Forms.Panel();
            this.lbl_shareid = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.pnl_Burn = new System.Windows.Forms.Panel();
            this.btn_no = new System.Windows.Forms.Button();
            this.btn_yes = new System.Windows.Forms.Button();
            this.lbl_do_you = new System.Windows.Forms.Label();
            this.pnl_balance = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_balance_cash = new System.Windows.Forms.Label();
            this.lbl_points = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnl_result = new System.Windows.Forms.Panel();
            this.pnl_result_points = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_newBalance = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_oldBalance = new System.Windows.Forms.Label();
            this.pn_result_info = new System.Windows.Forms.Panel();
            this.lbl_result_receipt = new System.Windows.Forms.Label();
            this.btnStartOver = new System.Windows.Forms.Button();
            this.lbl_resultBurn = new System.Windows.Forms.Label();
            this.lbl_resultEarn = new System.Windows.Forms.Label();
            this.pic_success = new System.Windows.Forms.PictureBox();
            this.pnl_Burn_amount = new System.Windows.Forms.Panel();
            this.btn_bunCancel = new System.Windows.Forms.Button();
            this.btn_burn = new System.Windows.Forms.Button();
            this.txt_burnAmunt = new System.Windows.Forms.TextBox();
            this.lbl_Amount = new System.Windows.Forms.Label();
            this.pnl_scan_barcode = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.txt_barcode = new System.Windows.Forms.TextBox();
            this.pnl_refund = new System.Windows.Forms.Panel();
            this.pnl_refundForm = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pnl_refundSummary = new System.Windows.Forms.Panel();
            this.lbl_pointsBurned = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_pointsEarned = new System.Windows.Forms.Label();
            this.lbl_pointsEarnedtitle = new System.Windows.Forms.Label();
            this.btn_refund = new System.Windows.Forms.Button();
            this.lbl_receiptHeader = new System.Windows.Forms.Label();
            this.lbl_receiptNumber = new System.Windows.Forms.Label();
            this.txt_receiptnumber = new System.Windows.Forms.TextBox();
            this.btn_loadReceipt = new System.Windows.Forms.Button();
            this.pnl_refundResult = new System.Windows.Forms.Panel();
            this.btn_refundBack = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.bl_refundResult = new System.Windows.Forms.Label();
            this.pic_refundSuccess = new System.Windows.Forms.PictureBox();
            this.pic_refundFailed = new System.Windows.Forms.PictureBox();
            this.pnl_approval = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txt_password_approval = new System.Windows.Forms.TextBox();
            this.PASSWORD = new System.Windows.Forms.Label();
            this.btn_CloseApproval = new System.Windows.Forms.Button();
            this.lbl_approval = new System.Windows.Forms.Label();
            this.btn_loginApproval = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnl_hg_tab_ref = new System.Windows.Forms.Panel();
            this.lbl_refund = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnl_hg_tab_eb = new System.Windows.Forms.Panel();
            this.lbl_member = new System.Windows.Forms.Label();
            this.pnl_loader = new System.Windows.Forms.Panel();
            this.pic_loader = new System.Windows.Forms.PictureBox();
            this.pnl_window = new System.Windows.Forms.Panel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.pnl_toolbar = new System.Windows.Forms.Panel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.pic_toolbar_logo = new System.Windows.Forms.PictureBox();
            this.pnl_Toolbar_close = new System.Windows.Forms.Panel();
            this.lbl_header_close = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnl_earn_burn.SuspendLayout();
            this.pnl_profile.SuspendLayout();
            this.pnl_memberProfile.SuspendLayout();
            this.pnl_wait.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_wait)).BeginInit();
            this.pnl_memberName.SuspendLayout();
            this.pnl_Burn.SuspendLayout();
            this.pnl_balance.SuspendLayout();
            this.pnl_result.SuspendLayout();
            this.pnl_result_points.SuspendLayout();
            this.pn_result_info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_success)).BeginInit();
            this.pnl_Burn_amount.SuspendLayout();
            this.pnl_scan_barcode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.pnl_refund.SuspendLayout();
            this.pnl_refundForm.SuspendLayout();
            this.panel5.SuspendLayout();
            this.pnl_refundSummary.SuspendLayout();
            this.pnl_refundResult.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_refundSuccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_refundFailed)).BeginInit();
            this.pnl_approval.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnl_loader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_loader)).BeginInit();
            this.pnl_window.SuspendLayout();
            this.pnl_toolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_toolbar_logo)).BeginInit();
            this.pnl_Toolbar_close.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_cashierName
            // 
            this.lbl_cashierName.AutoEllipsis = true;
            this.lbl_cashierName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_cashierName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cashierName.ForeColor = System.Drawing.Color.White;
            this.lbl_cashierName.Location = new System.Drawing.Point(388, 24);
            this.lbl_cashierName.Name = "lbl_cashierName";
            this.lbl_cashierName.Size = new System.Drawing.Size(135, 20);
            this.lbl_cashierName.TabIndex = 2;
            this.lbl_cashierName.Text = "CASHIER NAME";
            this.lbl_cashierName.Click += new System.EventHandler(this.lbl_cashierName_Click);
            this.lbl_cashierName.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_toolbar_MouseMove);
            this.lbl_cashierName.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_MouseUp);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::LEShare.Client.Properties.Resources.logout;
            this.pictureBox2.Location = new System.Drawing.Point(365, 17);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 30);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.lbl_cashierName_Click);
            this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_toolbar_MouseMove);
            this.pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_MouseUp);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.pnl_earn_burn);
            this.panel1.Controls.Add(this.pnl_refund);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pnl_loader);
            this.panel1.Location = new System.Drawing.Point(0, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(582, 401);
            this.panel1.TabIndex = 4;
            // 
            // pnl_earn_burn
            // 
            this.pnl_earn_burn.BackColor = System.Drawing.SystemColors.Control;
            this.pnl_earn_burn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_earn_burn.Controls.Add(this.pnl_profile);
            this.pnl_earn_burn.Controls.Add(this.pnl_scan_barcode);
            this.pnl_earn_burn.Location = new System.Drawing.Point(0, 62);
            this.pnl_earn_burn.Name = "pnl_earn_burn";
            this.pnl_earn_burn.Size = new System.Drawing.Size(582, 339);
            this.pnl_earn_burn.TabIndex = 2;
            this.pnl_earn_burn.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // pnl_profile
            // 
            this.pnl_profile.Controls.Add(this.pnl_memberProfile);
            this.pnl_profile.Location = new System.Drawing.Point(3, 4);
            this.pnl_profile.Name = "pnl_profile";
            this.pnl_profile.Size = new System.Drawing.Size(582, 332);
            this.pnl_profile.TabIndex = 1;
            // 
            // pnl_memberProfile
            // 
            this.pnl_memberProfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_memberProfile.Controls.Add(this.pnl_wait);
            this.pnl_memberProfile.Controls.Add(this.pnl_memberName);
            this.pnl_memberProfile.Controls.Add(this.pnl_Burn);
            this.pnl_memberProfile.Controls.Add(this.pnl_balance);
            this.pnl_memberProfile.Controls.Add(this.pnl_result);
            this.pnl_memberProfile.Controls.Add(this.pnl_Burn_amount);
            this.pnl_memberProfile.Location = new System.Drawing.Point(35, 4);
            this.pnl_memberProfile.Name = "pnl_memberProfile";
            this.pnl_memberProfile.Size = new System.Drawing.Size(506, 320);
            this.pnl_memberProfile.TabIndex = 1;
            this.pnl_memberProfile.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_memberProfile_Paint);
            // 
            // pnl_wait
            // 
            this.pnl_wait.Controls.Add(this.lbl_burn_success);
            this.pnl_wait.Controls.Add(this.pic_wait);
            this.pnl_wait.Controls.Add(this.btn_abort);
            this.pnl_wait.Controls.Add(this.lbl_wait);
            this.pnl_wait.Location = new System.Drawing.Point(0, 136);
            this.pnl_wait.Name = "pnl_wait";
            this.pnl_wait.Size = new System.Drawing.Size(505, 183);
            this.pnl_wait.TabIndex = 5;
            // 
            // lbl_burn_success
            // 
            this.lbl_burn_success.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_burn_success.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_burn_success.Location = new System.Drawing.Point(-3, 4);
            this.lbl_burn_success.Name = "lbl_burn_success";
            this.lbl_burn_success.Size = new System.Drawing.Size(508, 39);
            this.lbl_burn_success.TabIndex = 4;
            this.lbl_burn_success.Text = "BURNED " + Common.country + " 100";
            this.lbl_burn_success.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pic_wait
            // 
            this.pic_wait.BackColor = System.Drawing.Color.Transparent;
            this.pic_wait.Image = global::LEShare.Client.Properties.Resources.loading;
            this.pic_wait.Location = new System.Drawing.Point(133, 114);
            this.pic_wait.Name = "pic_wait";
            this.pic_wait.Size = new System.Drawing.Size(256, 16);
            this.pic_wait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_wait.TabIndex = 2;
            this.pic_wait.TabStop = false;
            this.pic_wait.Click += new System.EventHandler(this.pic_wait_Click);
            // 
            // btn_abort
            // 
            this.btn_abort.BackColor = System.Drawing.Color.DimGray;
            this.btn_abort.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_abort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_abort.ForeColor = System.Drawing.Color.White;
            this.btn_abort.Location = new System.Drawing.Point(18, 123);
            this.btn_abort.Name = "btn_abort";
            this.btn_abort.Size = new System.Drawing.Size(100, 45);
            this.btn_abort.TabIndex = 3;
            this.btn_abort.Text = "ABORT";
            this.btn_abort.UseVisualStyleBackColor = false;
            this.btn_abort.Click += new System.EventHandler(this.Btn_abort_Click);
            // 
            // lbl_wait
            // 
            this.lbl_wait.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_wait.Location = new System.Drawing.Point(50, 37);
            this.lbl_wait.Name = "lbl_wait";
            this.lbl_wait.Size = new System.Drawing.Size(393, 73);
            this.lbl_wait.TabIndex = 1;
            this.lbl_wait.Text = "Waiting for the transaction to complete in POS.";
            this.lbl_wait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnl_memberName
            // 
            this.pnl_memberName.BackColor = System.Drawing.Color.Maroon;
            this.pnl_memberName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_memberName.Controls.Add(this.lbl_shareid);
            this.pnl_memberName.Controls.Add(this.lbl_name);
            this.pnl_memberName.Location = new System.Drawing.Point(0, 0);
            this.pnl_memberName.Name = "pnl_memberName";
            this.pnl_memberName.Size = new System.Drawing.Size(505, 68);
            this.pnl_memberName.TabIndex = 0;
            // 
            // lbl_shareid
            // 
            this.lbl_shareid.AutoSize = true;
            this.lbl_shareid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_shareid.ForeColor = System.Drawing.Color.Goldenrod;
            this.lbl_shareid.Location = new System.Drawing.Point(9, 37);
            this.lbl_shareid.Name = "lbl_shareid";
            this.lbl_shareid.Size = new System.Drawing.Size(180, 20);
            this.lbl_shareid.TabIndex = 1;
            this.lbl_shareid.Text = "1234567891234567889";
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.ForeColor = System.Drawing.Color.White;
            this.lbl_name.Location = new System.Drawing.Point(6, 2);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(168, 26);
            this.lbl_name.TabIndex = 0;
            this.lbl_name.Text = "Member Name";
            // 
            // pnl_Burn
            // 
            this.pnl_Burn.Controls.Add(this.btn_no);
            this.pnl_Burn.Controls.Add(this.btn_yes);
            this.pnl_Burn.Controls.Add(this.lbl_do_you);
            this.pnl_Burn.Location = new System.Drawing.Point(0, 133);
            this.pnl_Burn.Name = "pnl_Burn";
            this.pnl_Burn.Size = new System.Drawing.Size(505, 187);
            this.pnl_Burn.TabIndex = 2;
            // 
            // btn_no
            // 
            this.btn_no.BackColor = System.Drawing.Color.DimGray;
            this.btn_no.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_no.ForeColor = System.Drawing.Color.White;
            this.btn_no.Location = new System.Drawing.Point(17, 90);
            this.btn_no.Name = "btn_no";
            this.btn_no.Size = new System.Drawing.Size(100, 45);
            this.btn_no.TabIndex = 2;
            this.btn_no.Text = "NO";
            this.btn_no.UseVisualStyleBackColor = false;
            this.btn_no.Click += new System.EventHandler(this.btn_no_Click);
            // 
            // btn_yes
            // 
            this.btn_yes.BackColor = System.Drawing.Color.Maroon;
            this.btn_yes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_yes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_yes.ForeColor = System.Drawing.Color.White;
            this.btn_yes.Location = new System.Drawing.Point(360, 90);
            this.btn_yes.Name = "btn_yes";
            this.btn_yes.Size = new System.Drawing.Size(100, 45);
            this.btn_yes.TabIndex = 1;
            this.btn_yes.Text = "YES";
            this.btn_yes.UseVisualStyleBackColor = false;
            this.btn_yes.Click += new System.EventHandler(this.btn_yes_Click);
            // 
            // lbl_do_you
            // 
            this.lbl_do_you.AutoSize = true;
            this.lbl_do_you.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_do_you.Location = new System.Drawing.Point(12, 40);
            this.lbl_do_you.Name = "lbl_do_you";
            this.lbl_do_you.Size = new System.Drawing.Size(261, 24);
            this.lbl_do_you.TabIndex = 0;
            this.lbl_do_you.Text = "Do you want to BURN points ?";
            this.lbl_do_you.Click += new System.EventHandler(this.lbl_do_you_Click);
            // 
            // pnl_balance
            // 
            this.pnl_balance.Controls.Add(this.label4);
            this.pnl_balance.Controls.Add(this.lbl_balance_cash);
            this.pnl_balance.Controls.Add(this.lbl_points);
            this.pnl_balance.Controls.Add(this.label3);
            this.pnl_balance.Location = new System.Drawing.Point(0, 68);
            this.pnl_balance.Name = "pnl_balance";
            this.pnl_balance.Size = new System.Drawing.Size(505, 68);
            this.pnl_balance.TabIndex = 1;
            this.pnl_balance.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_balance_Paint);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(251, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 24);
            this.label4.TabIndex = 3;
            this.label4.Text = "1322.00 "+Common.country;
            // 
            // lbl_balance_cash
            // 
            this.lbl_balance_cash.AutoSize = true;
            this.lbl_balance_cash.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_balance_cash.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbl_balance_cash.Location = new System.Drawing.Point(250, 6);
            this.lbl_balance_cash.Name = "lbl_balance_cash";
            this.lbl_balance_cash.Size = new System.Drawing.Size(98, 24);
            this.lbl_balance_cash.TabIndex = 2;
            this.lbl_balance_cash.Text = "BALANCE";
            // 
            // lbl_points
            // 
            this.lbl_points.AutoSize = true;
            this.lbl_points.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_points.Location = new System.Drawing.Point(7, 33);
            this.lbl_points.Name = "lbl_points";
            this.lbl_points.Size = new System.Drawing.Size(85, 24);
            this.lbl_points.TabIndex = 1;
            this.lbl_points.Text = "13220.00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(9, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "POINTS";
            // 
            // pnl_result
            // 
            this.pnl_result.Controls.Add(this.pnl_result_points);
            this.pnl_result.Controls.Add(this.pn_result_info);
            this.pnl_result.Location = new System.Drawing.Point(0, 68);
            this.pnl_result.Name = "pnl_result";
            this.pnl_result.Size = new System.Drawing.Size(505, 253);
            this.pnl_result.TabIndex = 3;
            // 
            // pnl_result_points
            // 
            this.pnl_result_points.Controls.Add(this.label8);
            this.pnl_result_points.Controls.Add(this.lbl_newBalance);
            this.pnl_result_points.Controls.Add(this.label6);
            this.pnl_result_points.Controls.Add(this.lbl_oldBalance);
            this.pnl_result_points.Location = new System.Drawing.Point(0, -1);
            this.pnl_result_points.Name = "pnl_result_points";
            this.pnl_result_points.Size = new System.Drawing.Size(505, 69);
            this.pnl_result_points.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label8.Location = new System.Drawing.Point(8, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 24);
            this.label8.TabIndex = 0;
            this.label8.Text = "OLD BALANCE";
            // 
            // lbl_newBalance
            // 
            this.lbl_newBalance.AutoSize = true;
            this.lbl_newBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_newBalance.Location = new System.Drawing.Point(255, 34);
            this.lbl_newBalance.Name = "lbl_newBalance";
            this.lbl_newBalance.Size = new System.Drawing.Size(45, 24);
            this.lbl_newBalance.TabIndex = 3;
            this.lbl_newBalance.Text = "0.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(255, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 24);
            this.label6.TabIndex = 2;
            this.label6.Text = "NEW BALANCE";
            // 
            // lbl_oldBalance
            // 
            this.lbl_oldBalance.AutoSize = true;
            this.lbl_oldBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_oldBalance.Location = new System.Drawing.Point(8, 34);
            this.lbl_oldBalance.Name = "lbl_oldBalance";
            this.lbl_oldBalance.Size = new System.Drawing.Size(45, 24);
            this.lbl_oldBalance.TabIndex = 1;
            this.lbl_oldBalance.Text = "0.00";
            // 
            // pn_result_info
            // 
            this.pn_result_info.Controls.Add(this.lbl_result_receipt);
            this.pn_result_info.Controls.Add(this.btnStartOver);
            this.pn_result_info.Controls.Add(this.lbl_resultBurn);
            this.pn_result_info.Controls.Add(this.lbl_resultEarn);
            this.pn_result_info.Controls.Add(this.pic_success);
            this.pn_result_info.Location = new System.Drawing.Point(0, 62);
            this.pn_result_info.Name = "pn_result_info";
            this.pn_result_info.Size = new System.Drawing.Size(505, 190);
            this.pn_result_info.TabIndex = 3;
            // 
            // lbl_result_receipt
            // 
            this.lbl_result_receipt.AutoSize = true;
            this.lbl_result_receipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_result_receipt.Location = new System.Drawing.Point(116, 3);
            this.lbl_result_receipt.Name = "lbl_result_receipt";
            this.lbl_result_receipt.Size = new System.Drawing.Size(89, 24);
            this.lbl_result_receipt.TabIndex = 5;
            this.lbl_result_receipt.Text = "Receipt #";
            // 
            // btnStartOver
            // 
            this.btnStartOver.BackColor = System.Drawing.Color.Maroon;
            this.btnStartOver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartOver.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartOver.ForeColor = System.Drawing.Color.White;
            this.btnStartOver.Location = new System.Drawing.Point(176, 129);
            this.btnStartOver.Name = "btnStartOver";
            this.btnStartOver.Size = new System.Drawing.Size(165, 45);
            this.btnStartOver.TabIndex = 4;
            this.btnStartOver.Text = "START OVER";
            this.btnStartOver.UseVisualStyleBackColor = false;
            this.btnStartOver.Click += new System.EventHandler(this.BtnStartOver_Click);
            // 
            // lbl_resultBurn
            // 
            this.lbl_resultBurn.AutoSize = true;
            this.lbl_resultBurn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_resultBurn.ForeColor = System.Drawing.Color.Firebrick;
            this.lbl_resultBurn.Location = new System.Drawing.Point(113, 76);
            this.lbl_resultBurn.Name = "lbl_resultBurn";
            this.lbl_resultBurn.Size = new System.Drawing.Size(156, 24);
            this.lbl_resultBurn.TabIndex = 2;
            this.lbl_resultBurn.Text = "Burned "+Common.country+" 0.00";
            // 
            // lbl_resultEarn
            // 
            this.lbl_resultEarn.AutoSize = true;
            this.lbl_resultEarn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_resultEarn.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.lbl_resultEarn.Location = new System.Drawing.Point(113, 40);
            this.lbl_resultEarn.Name = "lbl_resultEarn";
            this.lbl_resultEarn.Size = new System.Drawing.Size(173, 24);
            this.lbl_resultEarn.TabIndex = 1;
            this.lbl_resultEarn.Text = "Earned 0.00 Points.";
            // 
            // pic_success
            // 
            this.pic_success.Image = global::LEShare.Client.Properties.Resources.Green_Round_Tick;
            this.pic_success.Location = new System.Drawing.Point(11, 31);
            this.pic_success.Name = "pic_success";
            this.pic_success.Size = new System.Drawing.Size(87, 87);
            this.pic_success.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_success.TabIndex = 0;
            this.pic_success.TabStop = false;
            // 
            // pnl_Burn_amount
            // 
            this.pnl_Burn_amount.Controls.Add(this.btn_bunCancel);
            this.pnl_Burn_amount.Controls.Add(this.btn_burn);
            this.pnl_Burn_amount.Controls.Add(this.txt_burnAmunt);
            this.pnl_Burn_amount.Controls.Add(this.lbl_Amount);
            this.pnl_Burn_amount.Location = new System.Drawing.Point(0, 136);
            this.pnl_Burn_amount.Name = "pnl_Burn_amount";
            this.pnl_Burn_amount.Size = new System.Drawing.Size(505, 183);
            this.pnl_Burn_amount.TabIndex = 3;
            // 
            // btn_bunCancel
            // 
            this.btn_bunCancel.BackColor = System.Drawing.Color.DimGray;
            this.btn_bunCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_bunCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_bunCancel.ForeColor = System.Drawing.Color.White;
            this.btn_bunCancel.Location = new System.Drawing.Point(233, 67);
            this.btn_bunCancel.Name = "btn_bunCancel";
            this.btn_bunCancel.Size = new System.Drawing.Size(100, 45);
            this.btn_bunCancel.TabIndex = 4;
            this.btn_bunCancel.Text = "CANCEL";
            this.btn_bunCancel.UseVisualStyleBackColor = false;
            this.btn_bunCancel.Click += new System.EventHandler(this.Btn_bunCancel_Click);
            // 
            // btn_burn
            // 
            this.btn_burn.BackColor = System.Drawing.Color.Maroon;
            this.btn_burn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_burn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_burn.ForeColor = System.Drawing.Color.White;
            this.btn_burn.Location = new System.Drawing.Point(390, 67);
            this.btn_burn.Name = "btn_burn";
            this.btn_burn.Size = new System.Drawing.Size(100, 45);
            this.btn_burn.TabIndex = 3;
            this.btn_burn.Text = "BURN";
            this.btn_burn.UseVisualStyleBackColor = false;
            this.btn_burn.Click += new System.EventHandler(this.btn_burn_Click);
            // 
            // txt_burnAmunt
            // 
            this.txt_burnAmunt.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_burnAmunt.Location = new System.Drawing.Point(16, 67);
            this.txt_burnAmunt.Name = "txt_burnAmunt";
            this.txt_burnAmunt.Size = new System.Drawing.Size(182, 37);
            this.txt_burnAmunt.TabIndex = 2;
            this.txt_burnAmunt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_burnAmunt_KeyPress);
            // 
            // lbl_Amount
            // 
            this.lbl_Amount.AutoSize = true;
            this.lbl_Amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Amount.Location = new System.Drawing.Point(13, 20);
            this.lbl_Amount.Name = "lbl_Amount";
            this.lbl_Amount.Size = new System.Drawing.Size(239, 24);
            this.lbl_Amount.TabIndex = 1;
            this.lbl_Amount.Text = "Enter the Amount to BURN.";
            // 
            // pnl_scan_barcode
            // 
            this.pnl_scan_barcode.Controls.Add(this.label1);
            this.pnl_scan_barcode.Controls.Add(this.pictureBox3);
            this.pnl_scan_barcode.Controls.Add(this.txt_barcode);
            this.pnl_scan_barcode.Location = new System.Drawing.Point(0, 0);
            this.pnl_scan_barcode.Name = "pnl_scan_barcode";
            this.pnl_scan_barcode.Size = new System.Drawing.Size(582, 339);
            this.pnl_scan_barcode.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-2, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(583, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "SCAN SHARE BARCODE";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::LEShare.Client.Properties.Resources.scan_barcode;
            this.pictureBox3.Location = new System.Drawing.Point(143, 107);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(296, 177);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // txt_barcode
            // 
            this.txt_barcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_barcode.Location = new System.Drawing.Point(143, 154);
            this.txt_barcode.Name = "txt_barcode";
            this.txt_barcode.ShortcutsEnabled = false;
            this.txt_barcode.Size = new System.Drawing.Size(296, 37);
            this.txt_barcode.TabIndex = 0;
            this.txt_barcode.UseWaitCursor = true;
            this.txt_barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_barcode_KeyDown);
            // 
            // pnl_refund
            // 
            this.pnl_refund.BackColor = System.Drawing.SystemColors.Control;
            this.pnl_refund.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_refund.Controls.Add(this.pnl_refundForm);
            this.pnl_refund.Controls.Add(this.pnl_refundResult);
            this.pnl_refund.Controls.Add(this.pnl_approval);
            this.pnl_refund.Location = new System.Drawing.Point(0, 62);
            this.pnl_refund.Name = "pnl_refund";
            this.pnl_refund.Size = new System.Drawing.Size(582, 339);
            this.pnl_refund.TabIndex = 3;
            this.pnl_refund.Visible = false;
            // 
            // pnl_refundForm
            // 
            this.pnl_refundForm.BackColor = System.Drawing.SystemColors.Control;
            this.pnl_refundForm.Controls.Add(this.panel5);
            this.pnl_refundForm.Controls.Add(this.lbl_receiptNumber);
            this.pnl_refundForm.Controls.Add(this.txt_receiptnumber);
            this.pnl_refundForm.Controls.Add(this.btn_loadReceipt);
            this.pnl_refundForm.Location = new System.Drawing.Point(0, 0);
            this.pnl_refundForm.Name = "pnl_refundForm";
            this.pnl_refundForm.Size = new System.Drawing.Size(581, 339);
            this.pnl_refundForm.TabIndex = 2;
            this.pnl_refundForm.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_refundForm_Paint);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.pnl_refundSummary);
            this.panel5.Controls.Add(this.btn_refund);
            this.panel5.Controls.Add(this.lbl_receiptHeader);
            this.panel5.Location = new System.Drawing.Point(290, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(292, 339);
            this.panel5.TabIndex = 6;
            // 
            // pnl_refundSummary
            // 
            this.pnl_refundSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_refundSummary.Controls.Add(this.lbl_pointsBurned);
            this.pnl_refundSummary.Controls.Add(this.label5);
            this.pnl_refundSummary.Controls.Add(this.lbl_pointsEarned);
            this.pnl_refundSummary.Controls.Add(this.lbl_pointsEarnedtitle);
            this.pnl_refundSummary.Location = new System.Drawing.Point(3, 41);
            this.pnl_refundSummary.Name = "pnl_refundSummary";
            this.pnl_refundSummary.Size = new System.Drawing.Size(285, 139);
            this.pnl_refundSummary.TabIndex = 5;
            // 
            // lbl_pointsBurned
            // 
            this.lbl_pointsBurned.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pointsBurned.Location = new System.Drawing.Point(182, 68);
            this.lbl_pointsBurned.Name = "lbl_pointsBurned";
            this.lbl_pointsBurned.Size = new System.Drawing.Size(101, 68);
            this.lbl_pointsBurned.TabIndex = 3;
            this.lbl_pointsBurned.Text = Common.country+" 0.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Firebrick;
            this.label5.Location = new System.Drawing.Point(2, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "AMOUNT BURNED";
            // 
            // lbl_pointsEarned
            // 
            this.lbl_pointsEarned.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pointsEarned.Location = new System.Drawing.Point(182, 14);
            this.lbl_pointsEarned.Name = "lbl_pointsEarned";
            this.lbl_pointsEarned.Size = new System.Drawing.Size(98, 54);
            this.lbl_pointsEarned.TabIndex = 1;
            this.lbl_pointsEarned.Text = "0.00";
            // 
            // lbl_pointsEarnedtitle
            // 
            this.lbl_pointsEarnedtitle.AutoSize = true;
            this.lbl_pointsEarnedtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_pointsEarnedtitle.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.lbl_pointsEarnedtitle.Location = new System.Drawing.Point(2, 16);
            this.lbl_pointsEarnedtitle.Name = "lbl_pointsEarnedtitle";
            this.lbl_pointsEarnedtitle.Size = new System.Drawing.Size(139, 20);
            this.lbl_pointsEarnedtitle.TabIndex = 0;
            this.lbl_pointsEarnedtitle.Text = "POINTS EARNED";
            this.lbl_pointsEarnedtitle.Click += new System.EventHandler(this.Lbl_pointsEarnedtitle_Click);
            // 
            // btn_refund
            // 
            this.btn_refund.BackColor = System.Drawing.Color.Maroon;
            this.btn_refund.FlatAppearance.BorderSize = 0;
            this.btn_refund.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_refund.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_refund.ForeColor = System.Drawing.Color.White;
            this.btn_refund.Location = new System.Drawing.Point(165, 273);
            this.btn_refund.Margin = new System.Windows.Forms.Padding(2);
            this.btn_refund.Name = "btn_refund";
            this.btn_refund.Size = new System.Drawing.Size(100, 45);
            this.btn_refund.TabIndex = 4;
            this.btn_refund.Text = "REFUND";
            this.btn_refund.UseVisualStyleBackColor = false;
            this.btn_refund.Click += new System.EventHandler(this.btn_refund_Click);
            // 
            // lbl_receiptHeader
            // 
            this.lbl_receiptHeader.BackColor = System.Drawing.Color.DarkGray;
            this.lbl_receiptHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_receiptHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_receiptHeader.Location = new System.Drawing.Point(3, 9);
            this.lbl_receiptHeader.Name = "lbl_receiptHeader";
            this.lbl_receiptHeader.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.lbl_receiptHeader.Size = new System.Drawing.Size(285, 32);
            this.lbl_receiptHeader.TabIndex = 0;
            this.lbl_receiptHeader.Text = "RECEIPT #";
            this.lbl_receiptHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_receiptNumber
            // 
            this.lbl_receiptNumber.AutoSize = true;
            this.lbl_receiptNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_receiptNumber.Location = new System.Drawing.Point(12, 52);
            this.lbl_receiptNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_receiptNumber.Name = "lbl_receiptNumber";
            this.lbl_receiptNumber.Size = new System.Drawing.Size(124, 20);
            this.lbl_receiptNumber.TabIndex = 5;
            this.lbl_receiptNumber.Text = "Receipt Number";
            // 
            // txt_receiptnumber
            // 
            this.txt_receiptnumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_receiptnumber.Location = new System.Drawing.Point(16, 82);
            this.txt_receiptnumber.Margin = new System.Windows.Forms.Padding(2);
            this.txt_receiptnumber.Name = "txt_receiptnumber";
            this.txt_receiptnumber.Size = new System.Drawing.Size(256, 37);
            this.txt_receiptnumber.TabIndex = 4;
            this.txt_receiptnumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_receiptnumber_KeyPress);
            // 
            // btn_loadReceipt
            // 
            this.btn_loadReceipt.BackColor = System.Drawing.Color.Maroon;
            this.btn_loadReceipt.FlatAppearance.BorderSize = 0;
            this.btn_loadReceipt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_loadReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_loadReceipt.ForeColor = System.Drawing.Color.White;
            this.btn_loadReceipt.Location = new System.Drawing.Point(172, 155);
            this.btn_loadReceipt.Margin = new System.Windows.Forms.Padding(2);
            this.btn_loadReceipt.Name = "btn_loadReceipt";
            this.btn_loadReceipt.Size = new System.Drawing.Size(100, 45);
            this.btn_loadReceipt.TabIndex = 3;
            this.btn_loadReceipt.Text = "SUBMIT";
            this.btn_loadReceipt.UseVisualStyleBackColor = false;
            this.btn_loadReceipt.Click += new System.EventHandler(this.Btn_loadReceipt_Click);
            // 
            // pnl_refundResult
            // 
            this.pnl_refundResult.BackColor = System.Drawing.SystemColors.Control;
            this.pnl_refundResult.Controls.Add(this.btn_refundBack);
            this.pnl_refundResult.Controls.Add(this.panel7);
            this.pnl_refundResult.Location = new System.Drawing.Point(0, 0);
            this.pnl_refundResult.Name = "pnl_refundResult";
            this.pnl_refundResult.Size = new System.Drawing.Size(582, 339);
            this.pnl_refundResult.TabIndex = 2;
            // 
            // btn_refundBack
            // 
            this.btn_refundBack.BackColor = System.Drawing.Color.Maroon;
            this.btn_refundBack.FlatAppearance.BorderSize = 0;
            this.btn_refundBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_refundBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_refundBack.ForeColor = System.Drawing.Color.White;
            this.btn_refundBack.Location = new System.Drawing.Point(444, 267);
            this.btn_refundBack.Margin = new System.Windows.Forms.Padding(2);
            this.btn_refundBack.Name = "btn_refundBack";
            this.btn_refundBack.Size = new System.Drawing.Size(100, 45);
            this.btn_refundBack.TabIndex = 1;
            this.btn_refundBack.Text = "BACK";
            this.btn_refundBack.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.bl_refundResult);
            this.panel7.Controls.Add(this.pic_refundSuccess);
            this.panel7.Controls.Add(this.pic_refundFailed);
            this.panel7.Location = new System.Drawing.Point(36, 83);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(508, 152);
            this.panel7.TabIndex = 0;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // bl_refundResult
            // 
            this.bl_refundResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bl_refundResult.Location = new System.Drawing.Point(173, 27);
            this.bl_refundResult.Name = "bl_refundResult";
            this.bl_refundResult.Size = new System.Drawing.Size(293, 98);
            this.bl_refundResult.TabIndex = 2;
            this.bl_refundResult.Text = "Refund of Receipt Success. lorem ipsium dolar sit amet.";
            this.bl_refundResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pic_refundSuccess
            // 
            this.pic_refundSuccess.Image = global::LEShare.Client.Properties.Resources.Green_Round_Tick;
            this.pic_refundSuccess.Location = new System.Drawing.Point(25, 24);
            this.pic_refundSuccess.Name = "pic_refundSuccess";
            this.pic_refundSuccess.Size = new System.Drawing.Size(100, 100);
            this.pic_refundSuccess.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pic_refundSuccess.TabIndex = 0;
            this.pic_refundSuccess.TabStop = false;
            // 
            // pic_refundFailed
            // 
            this.pic_refundFailed.Image = global::LEShare.Client.Properties.Resources.error_icon;
            this.pic_refundFailed.Location = new System.Drawing.Point(23, 25);
            this.pic_refundFailed.Name = "pic_refundFailed";
            this.pic_refundFailed.Size = new System.Drawing.Size(100, 100);
            this.pic_refundFailed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pic_refundFailed.TabIndex = 1;
            this.pic_refundFailed.TabStop = false;
            this.pic_refundFailed.Visible = false;
            // 
            // pnl_approval
            // 
            this.pnl_approval.BackColor = System.Drawing.SystemColors.Control;
            this.pnl_approval.Controls.Add(this.panel4);
            this.pnl_approval.Location = new System.Drawing.Point(-1, -1);
            this.pnl_approval.Name = "pnl_approval";
            this.pnl_approval.Size = new System.Drawing.Size(582, 339);
            this.pnl_approval.TabIndex = 0;
            this.pnl_approval.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_approval_Paint);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::LEShare.Client.Properties.Resources.login_bg;
            this.panel4.Controls.Add(this.txt_password_approval);
            this.panel4.Controls.Add(this.PASSWORD);
            this.panel4.Controls.Add(this.btn_CloseApproval);
            this.panel4.Controls.Add(this.lbl_approval);
            this.panel4.Controls.Add(this.btn_loginApproval);
            this.panel4.ForeColor = System.Drawing.Color.White;
            this.panel4.Location = new System.Drawing.Point(59, 44);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(464, 239);
            this.panel4.TabIndex = 1;
            // 
            // txt_password_approval
            // 
            this.txt_password_approval.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_password_approval.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_password_approval.Location = new System.Drawing.Point(194, 74);
            this.txt_password_approval.Margin = new System.Windows.Forms.Padding(2, 2, 2, 6);
            this.txt_password_approval.Name = "txt_password_approval";
            this.txt_password_approval.PasswordChar = '●';
            this.txt_password_approval.Size = new System.Drawing.Size(215, 37);
            this.txt_password_approval.TabIndex = 1;
            // 
            // PASSWORD
            // 
            this.PASSWORD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(146)))), ((int)(((byte)(88)))));
            this.PASSWORD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PASSWORD.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PASSWORD.ForeColor = System.Drawing.Color.Black;
            this.PASSWORD.Location = new System.Drawing.Point(39, 74);
            this.PASSWORD.Name = "PASSWORD";
            this.PASSWORD.Size = new System.Drawing.Size(155, 37);
            this.PASSWORD.TabIndex = 5;
            this.PASSWORD.Text = "PASSWORD";
            this.PASSWORD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_CloseApproval
            // 
            this.btn_CloseApproval.BackColor = System.Drawing.Color.Maroon;
            this.btn_CloseApproval.FlatAppearance.BorderSize = 0;
            this.btn_CloseApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_CloseApproval.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_CloseApproval.ForeColor = System.Drawing.Color.White;
            this.btn_CloseApproval.Location = new System.Drawing.Point(413, 0);
            this.btn_CloseApproval.Margin = new System.Windows.Forms.Padding(2);
            this.btn_CloseApproval.Name = "btn_CloseApproval";
            this.btn_CloseApproval.Size = new System.Drawing.Size(50, 50);
            this.btn_CloseApproval.TabIndex = 4;
            this.btn_CloseApproval.Text = "X";
            this.btn_CloseApproval.UseVisualStyleBackColor = false;
            this.btn_CloseApproval.Click += new System.EventHandler(this.Btn_CloseApproval_Click);
            // 
            // lbl_approval
            // 
            this.lbl_approval.AutoSize = true;
            this.lbl_approval.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_approval.Location = new System.Drawing.Point(39, 13);
            this.lbl_approval.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_approval.Name = "lbl_approval";
            this.lbl_approval.Size = new System.Drawing.Size(186, 24);
            this.lbl_approval.TabIndex = 3;
            this.lbl_approval.Text = "APPRVOVAL LOGIN";
            // 
            // btn_loginApproval
            // 
            this.btn_loginApproval.BackColor = System.Drawing.Color.Maroon;
            this.btn_loginApproval.FlatAppearance.BorderSize = 0;
            this.btn_loginApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_loginApproval.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_loginApproval.ForeColor = System.Drawing.Color.White;
            this.btn_loginApproval.Location = new System.Drawing.Point(300, 153);
            this.btn_loginApproval.Margin = new System.Windows.Forms.Padding(2);
            this.btn_loginApproval.Name = "btn_loginApproval";
            this.btn_loginApproval.Size = new System.Drawing.Size(114, 45);
            this.btn_loginApproval.TabIndex = 0;
            this.btn_loginApproval.Text = "APPROVE";
            this.btn_loginApproval.UseVisualStyleBackColor = false;
            this.btn_loginApproval.Click += new System.EventHandler(this.btn_loginApproval_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Maroon;
            this.panel3.Controls.Add(this.pnl_hg_tab_ref);
            this.panel3.Controls.Add(this.lbl_refund);
            this.panel3.Location = new System.Drawing.Point(291, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(291, 62);
            this.panel3.TabIndex = 1;
            this.panel3.Click += new System.EventHandler(this.panel3_Click);
            // 
            // pnl_hg_tab_ref
            // 
            this.pnl_hg_tab_ref.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pnl_hg_tab_ref.Location = new System.Drawing.Point(0, 57);
            this.pnl_hg_tab_ref.Name = "pnl_hg_tab_ref";
            this.pnl_hg_tab_ref.Size = new System.Drawing.Size(291, 5);
            this.pnl_hg_tab_ref.TabIndex = 2;
            this.pnl_hg_tab_ref.Visible = false;
            // 
            // lbl_refund
            // 
            this.lbl_refund.AutoSize = true;
            this.lbl_refund.BackColor = System.Drawing.Color.Transparent;
            this.lbl_refund.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_refund.ForeColor = System.Drawing.Color.White;
            this.lbl_refund.Location = new System.Drawing.Point(95, 20);
            this.lbl_refund.Name = "lbl_refund";
            this.lbl_refund.Size = new System.Drawing.Size(83, 20);
            this.lbl_refund.TabIndex = 1;
            this.lbl_refund.Text = "REFUND";
            this.lbl_refund.Click += new System.EventHandler(this.panel3_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Maroon;
            this.panel2.Controls.Add(this.pnl_hg_tab_eb);
            this.panel2.Controls.Add(this.lbl_member);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(291, 62);
            this.panel2.TabIndex = 0;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            // 
            // pnl_hg_tab_eb
            // 
            this.pnl_hg_tab_eb.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pnl_hg_tab_eb.Location = new System.Drawing.Point(0, 57);
            this.pnl_hg_tab_eb.Name = "pnl_hg_tab_eb";
            this.pnl_hg_tab_eb.Size = new System.Drawing.Size(291, 5);
            this.pnl_hg_tab_eb.TabIndex = 1;
            // 
            // lbl_member
            // 
            this.lbl_member.AutoSize = true;
            this.lbl_member.BackColor = System.Drawing.Color.Transparent;
            this.lbl_member.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_member.ForeColor = System.Drawing.Color.White;
            this.lbl_member.Location = new System.Drawing.Point(19, 20);
            this.lbl_member.Name = "lbl_member";
            this.lbl_member.Size = new System.Drawing.Size(205, 20);
            this.lbl_member.TabIndex = 0;
            this.lbl_member.Text = "MEMBER EARN / BURN";
            this.lbl_member.Click += new System.EventHandler(this.panel2_Click);
            // 
            // pnl_loader
            // 
            this.pnl_loader.BackColor = System.Drawing.Color.Transparent;
            this.pnl_loader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnl_loader.Controls.Add(this.pic_loader);
            this.pnl_loader.Location = new System.Drawing.Point(0, 0);
            this.pnl_loader.Name = "pnl_loader";
            this.pnl_loader.Size = new System.Drawing.Size(582, 402);
            this.pnl_loader.TabIndex = 1;
            this.pnl_loader.Visible = false;
            // 
            // pic_loader
            // 
            this.pic_loader.Image = global::LEShare.Client.Properties.Resources.loader;
            this.pic_loader.Location = new System.Drawing.Point(202, 143);
            this.pic_loader.Name = "pic_loader";
            this.pic_loader.Size = new System.Drawing.Size(147, 98);
            this.pic_loader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pic_loader.TabIndex = 0;
            this.pic_loader.TabStop = false;
            // 
            // pnl_window
            // 
            this.pnl_window.BackColor = System.Drawing.Color.Transparent;
            this.pnl_window.Controls.Add(this.panel1);
            this.pnl_window.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnl_window.Location = new System.Drawing.Point(0, 53);
            this.pnl_window.Name = "pnl_window";
            this.pnl_window.Size = new System.Drawing.Size(582, 465);
            this.pnl_window.TabIndex = 5;
            // 
            // timer
            // 
            this.timer.Interval = 5000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // pnl_toolbar
            // 
            this.pnl_toolbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(38)))), ((int)(((byte)(29)))));
            this.pnl_toolbar.Controls.Add(this.lblVersion);
            this.pnl_toolbar.Controls.Add(this.pic_toolbar_logo);
            this.pnl_toolbar.Controls.Add(this.pictureBox2);
            this.pnl_toolbar.Controls.Add(this.lbl_cashierName);
            this.pnl_toolbar.Controls.Add(this.pnl_Toolbar_close);
            this.pnl_toolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_toolbar.Location = new System.Drawing.Point(0, 0);
            this.pnl_toolbar.Name = "pnl_toolbar";
            this.pnl_toolbar.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.pnl_toolbar.Size = new System.Drawing.Size(582, 60);
            this.pnl_toolbar.TabIndex = 5;
            this.pnl_toolbar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_toolbar_MouseMove);
            this.pnl_toolbar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_MouseUp);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.Color.White;
            this.lblVersion.Location = new System.Drawing.Point(152, 24);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(59, 13);
            this.lblVersion.TabIndex = 5;
            this.lblVersion.Text = "V 2020.0.0";
            // 
            // pic_toolbar_logo
            // 
            this.pic_toolbar_logo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pic_toolbar_logo.Image = global::LEShare.Client.Properties.Resources.maf_share;
            this.pic_toolbar_logo.Location = new System.Drawing.Point(15, 0);
            this.pic_toolbar_logo.Name = "pic_toolbar_logo";
            this.pic_toolbar_logo.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.pic_toolbar_logo.Size = new System.Drawing.Size(123, 60);
            this.pic_toolbar_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_toolbar_logo.TabIndex = 1;
            this.pic_toolbar_logo.TabStop = false;
            this.pic_toolbar_logo.Click += new System.EventHandler(this.PictureBox1_Click);
            this.pic_toolbar_logo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_toolbar_MouseMove);
            this.pic_toolbar_logo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_MouseUp);
            // 
            // pnl_Toolbar_close
            // 
            this.pnl_Toolbar_close.Controls.Add(this.lbl_header_close);
            this.pnl_Toolbar_close.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnl_Toolbar_close.Location = new System.Drawing.Point(522, 0);
            this.pnl_Toolbar_close.Name = "pnl_Toolbar_close";
            this.pnl_Toolbar_close.Size = new System.Drawing.Size(60, 60);
            this.pnl_Toolbar_close.TabIndex = 0;
            this.pnl_Toolbar_close.Click += new System.EventHandler(this.lbl_header_close_Click);
            // 
            // lbl_header_close
            // 
            this.lbl_header_close.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_header_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_header_close.ForeColor = System.Drawing.Color.White;
            this.lbl_header_close.Location = new System.Drawing.Point(0, 0);
            this.lbl_header_close.Name = "lbl_header_close";
            this.lbl_header_close.Size = new System.Drawing.Size(60, 60);
            this.lbl_header_close.TabIndex = 0;
            this.lbl_header_close.Text = "-";
            this.lbl_header_close.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_header_close.Click += new System.EventHandler(this.lbl_header_close_Click);
            // 
            // frmShare
            // 
            this.BackgroundImage = global::LEShare.Client.Properties.Resources.m_footer;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(582, 518);
            this.Controls.Add(this.pnl_toolbar);
            this.Controls.Add(this.pnl_window);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmShare";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmShare_FormClosing);
            this.Load += new System.EventHandler(this.FrmShare_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.pnl_earn_burn.ResumeLayout(false);
            this.pnl_profile.ResumeLayout(false);
            this.pnl_memberProfile.ResumeLayout(false);
            this.pnl_wait.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_wait)).EndInit();
            this.pnl_memberName.ResumeLayout(false);
            this.pnl_memberName.PerformLayout();
            this.pnl_Burn.ResumeLayout(false);
            this.pnl_Burn.PerformLayout();
            this.pnl_balance.ResumeLayout(false);
            this.pnl_balance.PerformLayout();
            this.pnl_result.ResumeLayout(false);
            this.pnl_result_points.ResumeLayout(false);
            this.pnl_result_points.PerformLayout();
            this.pn_result_info.ResumeLayout(false);
            this.pn_result_info.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_success)).EndInit();
            this.pnl_Burn_amount.ResumeLayout(false);
            this.pnl_Burn_amount.PerformLayout();
            this.pnl_scan_barcode.ResumeLayout(false);
            this.pnl_scan_barcode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.pnl_refund.ResumeLayout(false);
            this.pnl_refundForm.ResumeLayout(false);
            this.pnl_refundForm.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.pnl_refundSummary.ResumeLayout(false);
            this.pnl_refundSummary.PerformLayout();
            this.pnl_refundResult.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_refundSuccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_refundFailed)).EndInit();
            this.pnl_approval.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnl_loader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_loader)).EndInit();
            this.pnl_window.ResumeLayout(false);
            this.pnl_toolbar.ResumeLayout(false);
            this.pnl_toolbar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_toolbar_logo)).EndInit();
            this.pnl_Toolbar_close.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl_cashierName;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnl_earn_burn;
        private System.Windows.Forms.Panel pnl_refund;
        private System.Windows.Forms.Panel pnl_scan_barcode;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_barcode;
        private System.Windows.Forms.Panel pnl_profile;
        private System.Windows.Forms.Panel pnl_memberProfile;
        private System.Windows.Forms.Panel pnl_balance;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_balance_cash;
        private System.Windows.Forms.Label lbl_points;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnl_memberName;
        private System.Windows.Forms.Label lbl_shareid;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Panel pnl_Burn_amount;
        private System.Windows.Forms.Button btn_burn;
        private System.Windows.Forms.TextBox txt_burnAmunt;
        private System.Windows.Forms.Label lbl_Amount;
        private System.Windows.Forms.Panel pnl_Burn;
        private System.Windows.Forms.Button btn_no;
        private System.Windows.Forms.Button btn_yes;
        private System.Windows.Forms.Label lbl_do_you;
        private System.Windows.Forms.Panel pnl_wait;
        private System.Windows.Forms.Label lbl_wait;
        private System.Windows.Forms.PictureBox pic_wait;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbl_refund;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbl_member;
        private System.Windows.Forms.Panel pnl_result;
        private System.Windows.Forms.Panel pn_result_info;
        private System.Windows.Forms.PictureBox pic_success;
        private System.Windows.Forms.Panel pnl_result_points;
        private System.Windows.Forms.Label lbl_newBalance;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_oldBalance;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_resultBurn;
        private System.Windows.Forms.Label lbl_resultEarn;
        private System.Windows.Forms.Panel pnl_approval;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_CloseApproval;
        private System.Windows.Forms.Label lbl_approval;
        private System.Windows.Forms.TextBox txt_password_approval;
        private System.Windows.Forms.Button btn_loginApproval;
        private System.Windows.Forms.Panel pnl_refundForm;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lbl_receiptHeader;
        private System.Windows.Forms.Label lbl_receiptNumber;
        private System.Windows.Forms.TextBox txt_receiptnumber;
        private System.Windows.Forms.Button btn_loadReceipt;
        private System.Windows.Forms.Panel pnl_refundSummary;
        private System.Windows.Forms.Label lbl_pointsBurned;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbl_pointsEarned;
        private System.Windows.Forms.Label lbl_pointsEarnedtitle;
        private System.Windows.Forms.Button btn_refund;
        private System.Windows.Forms.Panel pnl_window;
        private System.Windows.Forms.Panel pnl_loader;
        private System.Windows.Forms.PictureBox pic_loader;
        private System.Windows.Forms.Panel pnl_refundResult;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pic_refundSuccess;
        private System.Windows.Forms.PictureBox pic_refundFailed;
        private System.Windows.Forms.Label bl_refundResult;
        private System.Windows.Forms.Button btn_refundBack;
        private System.Windows.Forms.Button btn_bunCancel;
        private System.Windows.Forms.Button btn_abort;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button btnStartOver;
        private System.Windows.Forms.Label lbl_result_receipt;
        private System.Windows.Forms.Panel pnl_hg_tab_ref;
        private System.Windows.Forms.Panel pnl_hg_tab_eb;
        private System.Windows.Forms.Label PASSWORD;
        private System.Windows.Forms.Panel pnl_toolbar;
        private System.Windows.Forms.PictureBox pic_toolbar_logo;
        private System.Windows.Forms.Panel pnl_Toolbar_close;
        private System.Windows.Forms.Label lbl_header_close;
        private System.Windows.Forms.Label lbl_burn_success;
        private System.Windows.Forms.Label lblVersion;
    }
}