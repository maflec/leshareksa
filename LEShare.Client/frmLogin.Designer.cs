﻿namespace LEShare.Client
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_posname = new System.Windows.Forms.Label();
            this.PASSWORD = new System.Windows.Forms.Label();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnl_window = new System.Windows.Forms.Panel();
            this.pnl_toolbar = new System.Windows.Forms.Panel();
            this.pic_toolbar_logo = new System.Windows.Forms.PictureBox();
            this.pnl_Toolbar_close = new System.Windows.Forms.Panel();
            this.lbl_header_close = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnl_window.SuspendLayout();
            this.pnl_toolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_toolbar_logo)).BeginInit();
            this.pnl_Toolbar_close.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::LEShare.Client.Properties.Resources.login_bg;
            this.panel1.Controls.Add(this.lbl_posname);
            this.panel1.Controls.Add(this.PASSWORD);
            this.panel1.Controls.Add(this.txt_password);
            this.panel1.Controls.Add(this.btnLogin);
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(53, 135);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(550, 324);
            this.panel1.TabIndex = 0;
            // 
            // lbl_posname
            // 
            this.lbl_posname.AutoSize = true;
            this.lbl_posname.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_posname.Location = new System.Drawing.Point(21, 12);
            this.lbl_posname.Name = "lbl_posname";
            this.lbl_posname.Size = new System.Drawing.Size(114, 38);
            this.lbl_posname.TabIndex = 3;
            this.lbl_posname.Text = "POS 1";
            // 
            // PASSWORD
            // 
            this.PASSWORD.AutoSize = true;
            this.PASSWORD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(146)))), ((int)(((byte)(88)))));
            this.PASSWORD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PASSWORD.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PASSWORD.ForeColor = System.Drawing.Color.Black;
            this.PASSWORD.Location = new System.Drawing.Point(26, 104);
            this.PASSWORD.Name = "PASSWORD";
            this.PASSWORD.Padding = new System.Windows.Forms.Padding(6, 11, 2, 10);
            this.PASSWORD.Size = new System.Drawing.Size(187, 53);
            this.PASSWORD.TabIndex = 2;
            this.PASSWORD.Text = "PASSWORD";
            // 
            // txt_password
            // 
            this.txt_password.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_password.Location = new System.Drawing.Point(204, 104);
            this.txt_password.Margin = new System.Windows.Forms.Padding(3, 2, 3, 1);
            this.txt_password.MaxLength = 100;
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '●';
            this.txt_password.Size = new System.Drawing.Size(314, 55);
            this.txt_password.TabIndex = 0;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.Maroon;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(387, 216);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(129, 69);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "LOGIN";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::LEShare.Client.Properties.Resources.mp_logo;
            this.pictureBox1.InitialImage = global::LEShare.Client.Properties.Resources.mp_logo;
            this.pictureBox1.Location = new System.Drawing.Point(394, 82);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(174, 95);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pnl_window
            // 
            this.pnl_window.BackColor = System.Drawing.Color.Transparent;
            this.pnl_window.Controls.Add(this.pictureBox1);
            this.pnl_window.Controls.Add(this.panel1);
            this.pnl_window.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_window.Location = new System.Drawing.Point(0, 0);
            this.pnl_window.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pnl_window.Name = "pnl_window";
            this.pnl_window.Size = new System.Drawing.Size(650, 588);
            this.pnl_window.TabIndex = 1;
            // 
            // pnl_toolbar
            // 
            this.pnl_toolbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(38)))), ((int)(((byte)(29)))));
            this.pnl_toolbar.Controls.Add(this.lblVersion);
            this.pnl_toolbar.Controls.Add(this.pic_toolbar_logo);
            this.pnl_toolbar.Controls.Add(this.pnl_Toolbar_close);
            this.pnl_toolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_toolbar.Location = new System.Drawing.Point(0, 0);
            this.pnl_toolbar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnl_toolbar.Name = "pnl_toolbar";
            this.pnl_toolbar.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.pnl_toolbar.Size = new System.Drawing.Size(650, 75);
            this.pnl_toolbar.TabIndex = 2;
            this.pnl_toolbar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_toolbar_MouseMove);
            this.pnl_toolbar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_MouseUp);
            // 
            // pic_toolbar_logo
            // 
            this.pic_toolbar_logo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pic_toolbar_logo.Image = global::LEShare.Client.Properties.Resources.maf_share;
            this.pic_toolbar_logo.Location = new System.Drawing.Point(17, 0);
            this.pic_toolbar_logo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_toolbar_logo.Name = "pic_toolbar_logo";
            this.pic_toolbar_logo.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.pic_toolbar_logo.Size = new System.Drawing.Size(138, 75);
            this.pic_toolbar_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_toolbar_logo.TabIndex = 1;
            this.pic_toolbar_logo.TabStop = false;
            this.pic_toolbar_logo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_toolbar_MouseMove);
            this.pic_toolbar_logo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_MouseUp);
            // 
            // pnl_Toolbar_close
            // 
            this.pnl_Toolbar_close.Controls.Add(this.lbl_header_close);
            this.pnl_Toolbar_close.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnl_Toolbar_close.Location = new System.Drawing.Point(582, 0);
            this.pnl_Toolbar_close.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnl_Toolbar_close.Name = "pnl_Toolbar_close";
            this.pnl_Toolbar_close.Size = new System.Drawing.Size(68, 75);
            this.pnl_Toolbar_close.TabIndex = 0;
            this.pnl_Toolbar_close.Click += new System.EventHandler(this.lbl_header_close_Click);
            this.pnl_Toolbar_close.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // lbl_header_close
            // 
            this.lbl_header_close.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_header_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_header_close.ForeColor = System.Drawing.Color.White;
            this.lbl_header_close.Location = new System.Drawing.Point(0, 0);
            this.lbl_header_close.Name = "lbl_header_close";
            this.lbl_header_close.Size = new System.Drawing.Size(68, 75);
            this.lbl_header_close.TabIndex = 0;
            this.lbl_header_close.Text = "X";
            this.lbl_header_close.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_header_close.Click += new System.EventHandler(this.lbl_header_close_Click);
            this.lbl_header_close.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_toolbar_MouseMove);
            this.lbl_header_close.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_MouseUp);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.Color.White;
            this.lblVersion.Location = new System.Drawing.Point(170, 31);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(86, 20);
            this.lblVersion.TabIndex = 4;
            this.lblVersion.Text = "V 2020.0.0";
            this.lblVersion.Click += new System.EventHandler(this.LblVersion_Click);
            // 
            // frmLogin
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LEShare.Client.Properties.Resources.m_footer;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(650, 588);
            this.ControlBox = false;
            this.Controls.Add(this.pnl_toolbar);
            this.Controls.Add(this.pnl_window);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Login_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnl_window.ResumeLayout(false);
            this.pnl_toolbar.ResumeLayout(false);
            this.pnl_toolbar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_toolbar_logo)).EndInit();
            this.pnl_Toolbar_close.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.Label lbl_posname;
        private System.Windows.Forms.Label PASSWORD;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pnl_window;
        private System.Windows.Forms.Panel pnl_toolbar;
        private System.Windows.Forms.Panel pnl_Toolbar_close;
        private System.Windows.Forms.PictureBox pic_toolbar_logo;
        private System.Windows.Forms.Label lbl_header_close;
        private System.Windows.Forms.Label lblVersion;
    }
}