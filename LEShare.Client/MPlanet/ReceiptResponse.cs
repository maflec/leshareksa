﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEShare.Client.MPlanet.Receipt
{

    public class ReceiptResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public Data data { get; set; }
        public string currency { get; set; }
    }


    public class Data
    {
        public string earnBitId { get; set; }
        public float earnpoints { get; set; }
        public string burnBitId { get; set; }
        public float burnAmount { get; set; }
    }


}
