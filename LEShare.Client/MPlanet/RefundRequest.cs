﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEShare.Client.MPlanet
{

    public class RefundRequest
    {
        public int receipt_No { get; set; }
        public int machine_Id { get; set; }
        public int cashier_Id { get; set; }
        public string machine_Name { get; set; }
        public string cashier_Name { get; set; }
    }

}
