﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEShare.Client.MPlanet
{
    public class MPTransResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public string bit_id { get; set; }
        public int mpTrans_id { get; set; }
        public float redeemed { get; set; }
        public float rewarded { get; set; }
        public float old_balance { get; set; }
        public float new_balance { get; set; }
    }

}
