﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEShare.Client.MPlanet.Earn
{

    public class EarnRequest
    {
        public int receipt_No { get; set; }
        public float payment_Amount { get; set; }
        public int machine_Id { get; set; }
        public int cashier_Id { get; set; }
        public string machine_Name { get; set; }
        public string cashier_Name { get; set; }
        public EarnLineItem[] lines { get; set; }
        public Payment_Details[] payments { get; set; }
        public string share_Id { get; set; }
    }

    public class EarnLineItem
    {
        public string earn_Line_Id { get; set; }
        public int receipt_No { get; set; }
        public int sequence_Number { get; set; }
        public DateTime transaction_DateTime { get; set; }
        public string card_Barcode { get; set; }
        public bool new_Card_Flag { get; set; }
        public string card_Type { get; set; }
        public string product_Category { get; set; }
        public string product_Code { get; set; }
        public string product_Name { get; set; }
        public float unit_Price { get; set; }
        public float tax { get; set; }
        public int quantity { get; set; }
        public string payment_Type { get; set; }
    }

    public class Payment_Details
    {
        public string payment_method { get; set; }
        public float amount { get; set; }
    }

}
