﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;
using LEShare.Client.MPlanet.Member;
using LEShare.Client.MPlanet.Receipt;
using LEShare.Client.MPlanet.Burn;
using LEShare.Client.MPlanet.Earn;
namespace LEShare.Client.MPlanet
{
    public class MPlanetManager
    {
        public  MPlanetManager()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => { return true; };
        }
        public async Task<MemberResponse> GetMember(string memberId)
        {
            MemberResponse gcResponse = new MemberResponse();

            try
            {                
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseAddress"].ToString());
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Apitoken", ConfigurationManager.AppSettings["ApiToken"].ToString());

                HttpResponseMessage response = await client.GetAsync("api/share/get-member?memberId=" + memberId);
                
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();
                gcResponse = JsonConvert.DeserializeObject<MemberResponse>(result);

              

            }
            catch (Exception ex)
            {
                Common.LogException(ex);

            }


            return gcResponse;



        }

        public async Task<ReceiptResponse> GetReceipt(string receiptNo)
        {
            ReceiptResponse gcResponse = new ReceiptResponse();

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseAddress"].ToString());
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Apitoken", ConfigurationManager.AppSettings["ApiToken"].ToString());

                HttpResponseMessage response = await
                    client.GetAsync("api/share/get-receipt?receiptNo=" + receiptNo);

                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();
                gcResponse = JsonConvert.DeserializeObject<ReceiptResponse>(result);



            }
            catch (Exception ex)
            {
                Common.LogException(ex);

            }


            return gcResponse;



        }

        public async Task<MPTransResponse> Burn(BurnRequest model)
        {
            MPTransResponse gcResponse = new MPTransResponse();

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseAddress"].ToString());
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Apitoken", ConfigurationManager.AppSettings["ApiToken"].ToString());

                var jsonContent =  JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(jsonContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await
                    client.PostAsync("api/share/burn", byteContent);

                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();
                gcResponse = JsonConvert.DeserializeObject<MPTransResponse>(result);



            }
            catch (Exception ex)
            {
                Common.LogException(ex);

            }


            return gcResponse;



        }

        public async Task<MPTransResponse> Earn(EarnRequest model)
        {
            MPTransResponse gcResponse = new MPTransResponse();

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseAddress"].ToString());
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Apitoken", ConfigurationManager.AppSettings["ApiToken"].ToString());

                var jsonContent = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(jsonContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await
                    client.PostAsync("api/share/earn", byteContent);

                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();
                gcResponse = JsonConvert.DeserializeObject<MPTransResponse>(result);



            }
            catch (Exception ex)
            {
                Common.LogException(ex);

            }


            return gcResponse;



        }
        public async Task<MPTransResponse> Refund(RefundRequest model)
        {
            MPTransResponse gcResponse = new MPTransResponse();

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseAddress"].ToString());
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Apitoken", ConfigurationManager.AppSettings["ApiToken"].ToString());

                var jsonContent = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(jsonContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await
                    client.PostAsync("api/share/refund", byteContent);

                //response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();
                gcResponse = JsonConvert.DeserializeObject<MPTransResponse>(result);



            }
            catch (Exception ex)
            {
                Common.LogException(ex);

            }


            return gcResponse;



        }
        public async Task<MPTransResponse> UpdateBurn(UpdateBurnRequest model)
        {
            MPTransResponse gcResponse = new MPTransResponse();

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseAddress"].ToString());
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Apitoken", ConfigurationManager.AppSettings["ApiToken"].ToString());

                var jsonContent = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(jsonContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await
                    client.PostAsync("api/share/update-burn", byteContent);

                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();
                gcResponse = JsonConvert.DeserializeObject<MPTransResponse>(result);



            }
            catch (Exception ex)
            {
                Common.LogException(ex);

            }


            return gcResponse;



        }

        

    }
}
