﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEShare.Client.MPlanet.Burn
{
  
    public class BurnRequest
    {
        public float Burn_Amount { get; set; }
        public int machine_Id { get; set; }
        public int cashier_Id { get; set; }
        public string machine_Name { get; set; }
        public string cashier_Name { get; set; }
        public string share_Id { get; set; }
        public string Card_Barcode { get; set; }
        public string Payment_Type { get; set; }
    }

}
