﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.Configuration;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Windows.Forms;
using System.Security.Cryptography;

class Common
{
        public static bool isLoggedIn;
        public static bool isElevatedUser;
        public static string cashierName;
        public static int userId;
        public static int applicationId;
        public static string locationName;
        public static int locationId;
        public static string applicationName;
        public static string conStr;
        public static string country;


    public static Form frmShareParked = new Form();

    //public static List<Form> formList = new List<Form>();

    public static void ClearAccess()
    {
        isLoggedIn = false;
        isElevatedUser = false;
        cashierName = string.Empty ;
        locationName = string.Empty;
        userId = 0;
        applicationId = 0;
        locationId = 0;
        applicationName = string.Empty;
        conStr = string.Empty;

        
    }

    public static string  CreateHash( string text)
    {
        MD5 md5 = new MD5CryptoServiceProvider();
        return string.Concat(md5.ComputeHash(Encoding.UTF8.GetBytes(text)).Select(p => p.ToString("X2")));


    }
    public static void LogException(Exception ex)
        {
            try
            {
                
                string FileName = string.Format(@"{0}\logs\Error\client_error_" + DateTime.Today.ToString("yyyMMdd") + ".txt",
                    Application.StartupPath);
                if (!CheckIfLogFileExists(FileName))//log file not exists
                {
                    CreateLogFile(FileName); //create log file
                }
                if (CheckIfLogFileExists(FileName))
                {
                    SaveException(FileName, ex);
                }

            }
            catch
            {
            }
        }

        public static void LogMessage(string message)
        {
            try
            {
            string FileName = string.Format(@"{0}\logs\Error\app_error_" + DateTime.Today.ToString("yyyMMdd") + ".txt",
                 Application.StartupPath);
            if (!CheckIfLogFileExists(FileName))//log file not exists
                {
                    CreateLogFile(FileName); //create log file
                }
                if (CheckIfLogFileExists(FileName))
                {
                    SaveException(FileName, message);
                }

            }
            catch
            {
            }
        }

        private static bool SaveException(string fileName, string message)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(fileName))
                {
                    sw.WriteLine(DateTime.Now.ToString());

                    StringBuilder s = new StringBuilder();

                    sw.WriteLine("Message: " + message);

                    sw.WriteLine();
                    sw.WriteLine();

                }
                return true;
            }
            catch
            {
            }
            return false;
        }

        private static bool SaveException(string fileName, Exception ex)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(fileName))
                {
                    var errorStr = JsonConvert.SerializeObject(ex);

                    //sw.WriteLine(DateTime.Now.ToString());
                    //sw.WriteLine("ex: " + ex.ToString());
                    //sw.WriteLine("ex.Message: " + ex.Message);
                    //sw.WriteLine("ex.StackTrace: " + ex.StackTrace);
                    //sw.WriteLine("ex.Source: " + ex.Source);
                    //StringBuilder s = new StringBuilder();
                    //while (ex != null)
                    //{
                    //    s.AppendLine("Exception type: " + ex.GetType().FullName);
                    //    s.AppendLine("Message       : " + ex.Message);
                    //    s.AppendLine("Stacktrace:");
                    //    s.AppendLine(ex.StackTrace);
                    //    s.AppendLine();
                    //    ex = ex.InnerException;
                    //}
                    //sw.WriteLine("innerexceptions: " + s);

                    sw.WriteLine(DateTime.Now.ToString());
                    sw.WriteLine(errorStr);

                    sw.WriteLine();
                    sw.WriteLine();

                }
                return true;
            }
            catch
            {
            }
            return false;
        }
        private static bool CreateLogFile(string fileName)
        {
            try
            {
                File.CreateText(fileName).Close();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }
        private static bool CheckIfLogFileExists(string fileName)
        {
            try
            {
                return File.Exists(fileName);

            }
            catch
            {
            }
            return false;
        }
        static void changeLineInFile(string lineKey, string fileName, string newValue)
        {
            try
            {
                string[] readText = File.ReadAllLines(fileName);
                for (int i = 0; i < readText.Length; i++)
                {
                    string txtLine = readText[i];
                    if (txtLine.Contains('|'))
                    {
                        if (txtLine.Split('|')[0] == lineKey)
                        {
                            readText[i] = lineKey + "|" + newValue;
                            break;
                        }
                    }
                }

                File.WriteAllLines(fileName, readText);
            }
            catch
            {
            }
        }

    }

