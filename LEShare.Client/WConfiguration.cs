﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEShare.Client
{
    public class WConfiguration
    {

        public string WorkstationCode { get; set; }        
        public string WebAppBaseAddress { get; set; }        
        public string CurrencyCode { get; set; }
        public string ApiBaseAddress { get; set; }
        public string ApiToken { get; set; }
        


    }
}
