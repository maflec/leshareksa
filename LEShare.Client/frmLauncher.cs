﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LEShare.Client
{
    public partial class frmLauncher : Form
    {

        private bool ignoreClick = false;

        public frmLauncher()
        {
            InitializeComponent();
        }

        private void BtnLaunchShare_Click(object sender, EventArgs e)
        {
            try
            {

              //  if (!ignoreClick)
                {
                    if (Common.isLoggedIn)
                    {

                        frmShare frm = (frmShare)Common.frmShareParked;
                        frm.Show();
                    }
                    else
                    {
                        frmLogin frm = new frmLogin();
                        frm.Show(this);
                    }
                }

                
            }
            catch
            {

            }
        }

        private void FrmLauncher_Load(object sender, EventArgs e)
        {
            try
            {
                
                int screenWidth = Screen.PrimaryScreen.Bounds.Width;
                //int screenHeight = Screen.PrimaryScreen.Bounds.Height;

                this.Location = new System.Drawing.Point(screenWidth - this.Width , 0);
            }
            catch (Exception ex)
            {
                //Common.LogException(ex);
            }
        }

        private void FrmLauncher_MouseMove(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Left)
            //{
            //    ignoreClick = true;
            //    var Screendim = Screen.FromControl(this).Bounds;
            //    var MaxX = Screendim.Width - this.Width;
            //    var MaxY = Screendim.Height - this.Height;

            //    var X = Cursor.Position.X;
            //    var Y = Cursor.Position.Y;

            //    if (Cursor.Position.X >= MaxX)
            //    {
            //        X = MaxX;
            //    }
            //    if (Cursor.Position.Y >= MaxY)
            //    {
            //        Y = MaxY;
            //    }

            //    this.Location = new Point(X, Y);
            //}
        }
        private void btn_MouseMove(object sender, MouseEventArgs e)
        {
            
        }

        private void btn_MouseUp(object sender, MouseEventArgs e)
        {
            ignoreClick = false;
        }
    }
}
