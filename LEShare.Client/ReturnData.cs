﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    public class ReturnData
    {
        public string operationStatus { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }

