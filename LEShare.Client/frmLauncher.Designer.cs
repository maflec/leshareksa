﻿namespace LEShare.Client
{
    partial class frmLauncher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLauncher));
            this.btnLaunchShare = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLaunchShare
            // 
            this.btnLaunchShare.BackgroundImage = global::LEShare.Client.Properties.Resources.share_icon;
            this.btnLaunchShare.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLaunchShare.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLaunchShare.FlatAppearance.BorderSize = 0;
            this.btnLaunchShare.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLaunchShare.Location = new System.Drawing.Point(0, 0);
            this.btnLaunchShare.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLaunchShare.Name = "btnLaunchShare";
            this.btnLaunchShare.Size = new System.Drawing.Size(167, 42);
            this.btnLaunchShare.TabIndex = 0;
            this.btnLaunchShare.UseVisualStyleBackColor = true;
            this.btnLaunchShare.Click += new System.EventHandler(this.BtnLaunchShare_Click);
            this.btnLaunchShare.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmLauncher_MouseMove);
            this.btnLaunchShare.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_MouseUp);
            // 
            // frmLauncher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(167, 42);
            this.Controls.Add(this.btnLaunchShare);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmLauncher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "SHARE";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FrmLauncher_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLaunchShare;
    }
}