﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


public class ActionResponse
{
    public string Status { get; set; }
    public string Message { get; set; }
    public object data { get; set; }
    public string currency { get; set; }
}

