﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Api.MPlanet
{
    public class MPTransResponse
    {
        public string bit_id { get; set; }
        public int MPTrans_id { get; set; }
        public float redeemed { get; set; }
        public float rewarded { get; set; }
        public float old_balance { get; set; }
        public float new_balance { get; set; }
    }



}
