﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Api.MPlanet
{
    public class EarnRequest
    {
        //public double Location_Id { get; set; }
        public int Receipt_No { get; set; }      
        public float Payment_Amount { get; set; }
        public int Machine_Id { get; set; }
        public int Location_Id { get; set; }
        public int Cashier_Id { get; set; }
        
        public string Machine_Name { get; set; }
        public string Cashier_Name { get; set; }     
        public EarnLineItem[] lines { get; set; }

        public Payment_Details[] payments { get; set; }

        public string Share_Id { get; set; }
    }

    public class EarnLineItem
    {
        public string Earn_Line_Id { get; set; }
        public int Receipt_No { get; set; }
        public int Sequence_Number { get; set; }
        public DateTime Transaction_DateTime { get; set; }
        public string Card_Barcode { get; set; }
        public bool New_Card_Flag { get; set; }
        public string Card_Type { get; set; }
        public string Product_Category { get; set; }
        public string Product_Code { get; set; }
        public string Product_Name { get; set; }
        public float Unit_Price { get; set; }
        public float Tax { get; set; }
        public int Quantity { get; set; }
        public string Payment_Type { get; set; }
    }

    public class Payment_Details
    {
        public string payment_method { get; set; }
        public float amount { get; set; }
    }

}
