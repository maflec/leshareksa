﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Api.MPlanet
{
    public class BurnRequest
    {
        public float Burn_Amount { get; set; }

        public int Location_Id { get; set; }
        public int Machine_Id { get; set; }

        public int Cashier_Id { get; set; }

        public string Machine_Name { get; set; }
        public string Cashier_Name { get; set; }       

        public string Share_Id { get; set; }

        public string Payment_Type { get; set; }

        public string Card_Barcode { get; set; }


    }




}
