﻿using LEShare.Api.Gravity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Api.MPlanet
{
    public class MPlanetManager
    {
        public ActionResponse SaveEarnRequest(EarnRequest model,string locationId)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@ShareLocationId", SqlDbType.VarChar, locationId);
                sqlHelper.AddSetParameterToSQLCommand("@Receipt_No", SqlDbType.Int, model.Receipt_No);
                sqlHelper.AddSetParameterToSQLCommand("@Share_Id", SqlDbType.VarChar, model.Share_Id);
                sqlHelper.AddSetParameterToSQLCommand("@Earn_Burn_Flag", SqlDbType.VarChar, "EARN");
                sqlHelper.AddSetParameterToSQLCommand("@Machine_Id", SqlDbType.Int, model.Machine_Id);
                sqlHelper.AddSetParameterToSQLCommand("@Cashier_Id", SqlDbType.Int, model.Cashier_Id);
                sqlHelper.AddSetParameterToSQLCommand("@Machine_Name", SqlDbType.VarChar, model.Machine_Name);
                sqlHelper.AddSetParameterToSQLCommand("@Cashier_Name", SqlDbType.VarChar, model.Cashier_Name);

                

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_CreateShare_Transaction");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.Status = "SUCCESS";
                        response.Message = "Request saved successfully";
                        response.data = ds.Tables[0].Rows[0]["Id"].ToString();

                        sqlHelper = new SqlHelper();

                        sqlHelper.AddSetParameterToSQLCommand("@ShareLocationId", SqlDbType.VarChar, locationId);
                        sqlHelper.AddSetParameterToSQLCommand("@Receipt_No", SqlDbType.Int, model.Receipt_No);
                        sqlHelper.AddSetParameterToSQLCommand("@Posted_DateTime", SqlDbType.DateTime, DateTime.Now);
                        sqlHelper.AddSetParameterToSQLCommand("@Payment_Amount", SqlDbType.Money, model.Payment_Amount);
                        sqlHelper.AddSetParameterToSQLCommand("@Machine_Id", SqlDbType.Int, model.Machine_Id);
                        sqlHelper.AddSetParameterToSQLCommand("@Cashier_Id", SqlDbType.Int, model.Cashier_Id);
                        sqlHelper.AddSetParameterToSQLCommand("@Machine_Name", SqlDbType.VarChar, model.Machine_Name);
                        sqlHelper.AddSetParameterToSQLCommand("@Cashier_Name", SqlDbType.VarChar, model.Cashier_Name);

                        ds = sqlHelper.GetDatasetByCommand("SP_CreateEarn_Header");
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                            {
                               

                                //save line items

                                for (int i = 0; i < model.lines.Length; i++)
                                {
                                    sqlHelper = new SqlHelper();
                                    sqlHelper.AddSetParameterToSQLCommand("@Earn_Line_Id", SqlDbType.VarChar, model.lines[i].Earn_Line_Id);
                                    sqlHelper.AddSetParameterToSQLCommand("@Receipt_No", SqlDbType.Int, model.lines[i].Receipt_No);
                                    sqlHelper.AddSetParameterToSQLCommand("@Sequence_Number", SqlDbType.Int, model.lines[i].Sequence_Number);
                                    sqlHelper.AddSetParameterToSQLCommand("@Transaction_DateTime", SqlDbType.DateTime, model.lines[i].Transaction_DateTime);
                                    sqlHelper.AddSetParameterToSQLCommand("@Card_Barcode", SqlDbType.BigInt, model.lines[i].Card_Barcode);
                                    sqlHelper.AddSetParameterToSQLCommand("@New_Card_Flag", SqlDbType.Bit, model.lines[i].New_Card_Flag);
                                    sqlHelper.AddSetParameterToSQLCommand("@Card_Type", SqlDbType.VarChar, model.lines[i].Card_Type);
                                    sqlHelper.AddSetParameterToSQLCommand("@Product_Category", SqlDbType.VarChar, model.lines[i].Product_Category);
                                    sqlHelper.AddSetParameterToSQLCommand("@Product_Name", SqlDbType.VarChar, model.lines[i].Product_Name);
                                    sqlHelper.AddSetParameterToSQLCommand("@Product_Code", SqlDbType.VarChar, model.lines[i].Product_Code);
                                    sqlHelper.AddSetParameterToSQLCommand("@Unit_Price", SqlDbType.Money, model.lines[i].Unit_Price);
                                    sqlHelper.AddSetParameterToSQLCommand("@Tax", SqlDbType.Money, model.lines[i].Tax);
                                    sqlHelper.AddSetParameterToSQLCommand("@Quantity", SqlDbType.Int, model.lines[i].Quantity);
                                    sqlHelper.AddSetParameterToSQLCommand("@Payment_Type", SqlDbType.VarChar, model.lines[i].Payment_Type);

                                    ds = sqlHelper.GetDatasetByCommand("SP_CreateEarn_Line_Item");
                                }


                            }

                            else
                            {
                               
                                response.Message += "Receipt header not saved";
                            }
                        }
                        else
                        {

                            response.Message += "Receipt header not saved";
                        }
                    }
                    else
                    {
                        response.Status = "FAILURE";
                        response.Message = "Request not saved";
                    }
                }
                else
                {
                    response.Status = "FAILURE";
                    response.Message = "Request not saved";
                }



                return response;

            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }
            return response;
        }

        public ActionResponse UpdateShareRecord(AdminBitResponse model,int Id)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, Id);
                sqlHelper.AddSetParameterToSQLCommand("@Share_Rewarded", SqlDbType.Money, model.loyalty_balances[0].rewarded);
                sqlHelper.AddSetParameterToSQLCommand("@Share_Redeemed", SqlDbType.Money, model.loyalty_balances[0].redeemed);
                sqlHelper.AddSetParameterToSQLCommand("@Share_Old_Balance", SqlDbType.Money, model.loyalty_balances[0].old_balance);               
                sqlHelper.AddSetParameterToSQLCommand("@Share_New_Balance", SqlDbType.Money, model.loyalty_balances[0].new_balance);
                sqlHelper.AddSetParameterToSQLCommand("@Share_Bit_Id", SqlDbType.VarChar, model.bit_id);
                sqlHelper.AddSetParameterToSQLCommand("@Points_Rewarded_Flag", SqlDbType.Bit, model.points_rewarded);
                sqlHelper.AddSetParameterToSQLCommand("@Points_Redeemed_Flag", SqlDbType.Bit, model.points_redeemed);               
                sqlHelper.AddSetParameterToSQLCommand("@Share_Currency_Value", SqlDbType.Money, 0);
                sqlHelper.AddSetParameterToSQLCommand("@Response_Customer_Validation", SqlDbType.VarChar, "");
                sqlHelper.AddSetParameterToSQLCommand("@Response_Transaction_Status", SqlDbType.VarChar, model.status);
                sqlHelper.AddSetParameterToSQLCommand("@Response_Updated_Points", SqlDbType.VarChar, "");

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_UpdateShare_Transaction");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.Status = "SUCCESS";
                        response.Message = "Request updated successfully";
                                               
                    }
                    else
                    {
                        response.Status = "FAILURE";
                        response.Message = "Request not updated";
                    }
                }
                else
                {
                    response.Status = "FAILURE";
                    response.Message = "Request not updated";
                }



                return response;

            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }
            return response;
        }

        public ActionResponse SaveBurnRequest(BurnRequest model, string locationId)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@ShareLocationId", SqlDbType.VarChar, locationId);
                sqlHelper.AddSetParameterToSQLCommand("@Receipt_No", SqlDbType.Int, 0);
                sqlHelper.AddSetParameterToSQLCommand("@Share_Id", SqlDbType.VarChar, model.Share_Id);
                sqlHelper.AddSetParameterToSQLCommand("@Earn_Burn_Flag", SqlDbType.VarChar, "BURN");
                sqlHelper.AddSetParameterToSQLCommand("@Machine_Id", SqlDbType.Int, model.Machine_Id);
                sqlHelper.AddSetParameterToSQLCommand("@Cashier_Id", SqlDbType.Int, model.Cashier_Id);
                sqlHelper.AddSetParameterToSQLCommand("@Machine_Name", SqlDbType.VarChar, model.Machine_Name);
                sqlHelper.AddSetParameterToSQLCommand("@Cashier_Name", SqlDbType.VarChar, model.Cashier_Name);
                sqlHelper.AddSetParameterToSQLCommand("@Burn_Amount", SqlDbType.Money, model.Burn_Amount);


                DataSet ds = sqlHelper.GetDatasetByCommand("SP_CreateShare_Transaction");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.Status = "SUCCESS";
                        response.Message = "Request saved successfully";
                        response.data = ds.Tables[0].Rows[0]["Id"].ToString();

                        sqlHelper = new SqlHelper();


                        sqlHelper.AddSetParameterToSQLCommand("@ShareLocationId", SqlDbType.VarChar, locationId);
                        sqlHelper.AddSetParameterToSQLCommand("@Receipt_No", SqlDbType.Int, 0);
                        sqlHelper.AddSetParameterToSQLCommand("@Payment_DateTime", SqlDbType.DateTime, DateTime.Now);
                        sqlHelper.AddSetParameterToSQLCommand("@Burn_Amount", SqlDbType.Money, model.Burn_Amount);
                        sqlHelper.AddSetParameterToSQLCommand("@Machine_Id", SqlDbType.Int, model.Machine_Id);
                        sqlHelper.AddSetParameterToSQLCommand("@Cashier_Id", SqlDbType.Int, model.Cashier_Id);
                        sqlHelper.AddSetParameterToSQLCommand("@Machine_Name", SqlDbType.VarChar, model.Machine_Name);
                        sqlHelper.AddSetParameterToSQLCommand("@Cashier_Name", SqlDbType.VarChar, model.Cashier_Name);
                        sqlHelper.AddSetParameterToSQLCommand("@Share_Trans_Id", SqlDbType.Int, ds.Tables[0].Rows[0]["Id"].ToString());
                        sqlHelper.AddSetParameterToSQLCommand("@Payment_Type", SqlDbType.VarChar, model.Payment_Type);
                        sqlHelper.AddSetParameterToSQLCommand("@Card_Barcode", SqlDbType.VarChar, model.Card_Barcode);

                        ds = sqlHelper.GetDatasetByCommand("SP_CreateBurn_Transaction");
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                            {


                            }

                            else
                            {

                                response.Message += "Receipt header not saved";
                            }
                        }
                        else
                        {

                            response.Message += "Receipt header not saved";
                        }
                    }
                    else
                    {
                        response.Status = "FAILURE";
                        response.Message = "Request not saved";
                    }
                }
                else
                {
                    response.Status = "FAILURE";
                    response.Message = "Request not saved";
                }



                return response;

            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }
            return response;
        }

        public ActionResponse SaveRefundRequest(RefundRequest model, string locationId)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@ShareLocationId", SqlDbType.VarChar, locationId);
                sqlHelper.AddSetParameterToSQLCommand("@Receipt_No", SqlDbType.Int, model.Receipt_No);             
                sqlHelper.AddSetParameterToSQLCommand("@Earn_Burn_Flag", SqlDbType.VarChar, "REFUND");
                sqlHelper.AddSetParameterToSQLCommand("@Machine_Id", SqlDbType.Int, model.Machine_Id);
                sqlHelper.AddSetParameterToSQLCommand("@Cashier_Id", SqlDbType.Int, model.Cashier_Id);
                sqlHelper.AddSetParameterToSQLCommand("@Machine_Name", SqlDbType.VarChar, model.Machine_Name);
                sqlHelper.AddSetParameterToSQLCommand("@Cashier_Name", SqlDbType.VarChar, model.Cashier_Name);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_CreateShare_Transaction");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.Status = "SUCCESS";
                        response.Message = "Request saved successfully";
                        response.data = ds.Tables[0].Rows[0]["Id"].ToString();

                    }
                    else
                    {
                        response.Status = "FAILURE";
                        response.Message = "Request not saved";
                    }
                }
                else
                {
                    response.Status = "FAILURE";
                    response.Message = "Request not saved";
                }



                return response;

            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }
            return response;
        }

        public ActionResponse UpdateBurn(UpdateBurnRequest model)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Receipt_No", SqlDbType.Int, model.Receipt_No);
                sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, model.mpTrans_id);
                

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_UpdateBurn_Transaction");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.Status = "SUCCESS";
                        response.Message = "Request updated successfully";

                    }
                    else
                    {
                        response.Status = "FAILURE";
                        response.Message = "Request not updated";
                    }
                }
                else
                {
                    response.Status = "FAILURE";
                    response.Message = "Request not updated";
                }



                return response;

            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }
            return response;
        }

        public ActionResponse GetLocation(string  ApiToken)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@ApiToken", SqlDbType.NVarChar, ApiToken); 

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_Getlocation");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.Status = "SUCCESS";
                        response.Message = "Data retrieved successfully";

                        response.data = ds;


                    }
                    else
                    {
                        response.Status = "FAILURE";
                       
                    }
                }
                else
                {
                    response.Status = "FAILURE";
                  
                }



                return response;

            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }
            return response;
        }

        public ActionResponse GetReceipt(string ShareLocationId,int Receipt_No)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@ShareLocationId", SqlDbType.VarChar, ShareLocationId);
                sqlHelper.AddSetParameterToSQLCommand("@Receipt_No", SqlDbType.Int, Receipt_No);

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetReceipt");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow r = ds.Tables[0].Rows[0];

                        ReceiptResponse rresponse = new ReceiptResponse();
                        rresponse.burnAmount = float.Parse(r["BurnAmount"].ToString());
                        rresponse.earnpoints = float.Parse(r["Earnpoints"].ToString());
                        if (r["BurnBitId"].ToString().Length > 0)
                        {
                            rresponse.burnBitId = r["BurnBitId"].ToString();
                        }
                        if (r["EarnBitId"].ToString().Length > 0)
                        {
                            rresponse.earnBitId = r["EarnBitId"].ToString();
                        }
                       
                        rresponse.MemberId = r["MemberId"].ToString();
                        response.data = rresponse;
                        response.Status = "SUCCESS";
                        response.Message = "Data retrieved successfully";
                    }
                    else
                    {
                        response.Status = "FAILURE";
                    }
                    

                   
                }
                else
                {
                    response.Status = "FAILURE";

                }



                return response;

            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }
            return response;
        }

        public ActionResponse UpdateRefund(int Id, int receiptNo, AdminBitResponse model, 
            string refundedBitId, string locationId)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                SqlHelper sqlHelper = new SqlHelper();

                sqlHelper.AddSetParameterToSQLCommand("@Id", SqlDbType.Int, Id);
                sqlHelper.AddSetParameterToSQLCommand("@Receipt_No", SqlDbType.Int, receiptNo);
                sqlHelper.AddSetParameterToSQLCommand("@refundedBitId", SqlDbType.VarChar, refundedBitId);
                sqlHelper.AddSetParameterToSQLCommand("@ShareLocationId", SqlDbType.VarChar, locationId);              
                sqlHelper.AddSetParameterToSQLCommand("@Share_Bit_Id", SqlDbType.VarChar, model.bit_id);
              

                DataSet ds = sqlHelper.GetDatasetByCommand("SP_UpdateRefund_Transaction");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "SUCCESS")
                    {
                        response.Status = "SUCCESS";
                        response.Message = "Request updated successfully";

                    }
                    else
                    {
                        response.Status = "FAILURE";
                        response.Message = "Request not updated";
                    }
                }
                else
                {
                    response.Status = "FAILURE";
                    response.Message = "Request not updated";
                }



                return response;

            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }
            return response;
        }

    }
}
