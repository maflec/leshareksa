﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Api.MPlanet
{
   

    public class ReceiptResponse
    {
        public string MemberId { get; set; }
        
        public string earnBitId { get; set; }
        public float earnpoints { get; set; }
        public string burnBitId { get; set; }
        public float burnAmount { get; set; }
    }

}
