﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Api.Gravity
{

    public class MemberResponse
    {
        public string shareId { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string dateOfJoining { get; set; }
        public string enrollmentChannel { get; set; }
        public int membershipTenure { get; set; }
        public string tierClass { get; set; }
        public string membershipStage { get; set; }
        public float pointBalance { get; set; }
        public float amountBalance { get; set; }
        public float totalRedeemed { get; set; }
        public float totalExpired { get; set; }
        public string balanceCurrency { get; set; }
    }

}
