﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace LEShare.Api.Gravity
{
    public class GravityManager
    {
        TokenResponse globalTokenResponse = new TokenResponse();
        public AdminBitResponse RequestEarnBit(EarnBitRequest request)
        {
            AdminBitResponse adminBitResponse = new AdminBitResponse();
            try
            {
                var BaseUrl = General.GetSettingsValue("BaseUrlAdminBit");
                var xApiKey = General.GetSettingsValue("xApiKey");


                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + globalTokenResponse.access_token);
                client.DefaultRequestHeaders.Add("x-api-key", xApiKey);
                HttpResponseMessage response = client.PostAsJsonAsync
                    ("gravty/adminbit/request", request).Result;
                //response.EnsureSuccessStatusCode();

                string result = response.Content.ReadAsStringAsync().Result;


                adminBitResponse = JsonConvert.DeserializeObject<AdminBitResponse>(result);



            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return adminBitResponse;
        }
        public AdminBitResponse RequestBurnBit(BurnBitRequest request)
        {
            AdminBitResponse adminBitResponse = new AdminBitResponse();
            try
            {
                var BaseUrl = General.GetSettingsValue("BaseUrlAdminBit");
                var xApiKey = General.GetSettingsValue("xApiKey");

                //var js = JsonConvert.SerializeObject(request);

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + globalTokenResponse.access_token);
                client.DefaultRequestHeaders.Add("x-api-key", xApiKey);
                HttpResponseMessage response = client.PostAsJsonAsync
                    ("gravty/adminbit/request", request).Result;
                //response.EnsureSuccessStatusCode();

                string result = response.Content.ReadAsStringAsync().Result;

                adminBitResponse = JsonConvert.DeserializeObject<AdminBitResponse>(result);



            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return adminBitResponse;
        }

        public AdminBitResponse RequestCancelBit(CancelBitRequest request)
        {
            AdminBitResponse adminBitResponse = new AdminBitResponse();
            try
            {
                var BaseUrl = General.GetSettingsValue("BaseUrlAdminBit");
                var xApiKey = General.GetSettingsValue("xApiKey");

                //var js = JsonConvert.SerializeObject(request);

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + globalTokenResponse.access_token);
                client.DefaultRequestHeaders.Add("x-api-key", xApiKey);
                HttpResponseMessage response = client.PostAsJsonAsync
                    ("gravty/adminbit/request", request).Result;
                //response.EnsureSuccessStatusCode();

                string result = response.Content.ReadAsStringAsync().Result;

                adminBitResponse = JsonConvert.DeserializeObject<AdminBitResponse>(result);



            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return adminBitResponse;
        }
        public AdminBitResponse RequestBitSim(EarnBitRequest request)
        {
            AdminBitResponse adminBitResponse = new AdminBitResponse();
            try
            {
                var BaseUrl = General.GetSettingsValue("BaseUrlAdminBit");
                var xApiKey = General.GetSettingsValue("xApiKey");

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + globalTokenResponse.access_token);
                client.DefaultRequestHeaders.Add("x-api-key", xApiKey);
                HttpResponseMessage response = client.PostAsJsonAsync
                    ("gravty/adminbit/bitsimulation", request).Result;
                //response.EnsureSuccessStatusCode();

                string result = response.Content.ReadAsStringAsync().Result;


                adminBitResponse = JsonConvert.DeserializeObject<AdminBitResponse>(result);



            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return adminBitResponse;
        }
        public MemberResponse GetMemberDetails(string memberId,string region)
        {
            MemberResponse memberResponse = new MemberResponse();
            try
            {
                var BaseUrl = General.GetSettingsValue("BaseUrl");
                var xApiKey = General.GetSettingsValue("xApiKey");

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + globalTokenResponse.access_token);
                client.DefaultRequestHeaders.Add("x-api-key", xApiKey);
                if (region != "")
                {
                    client.DefaultRequestHeaders.Add("country", region);
                }

                HttpResponseMessage response = client.GetAsync
                    ("magicplanet/findmember?memberid=" + memberId).Result;
                response.EnsureSuccessStatusCode();

                string result = response.Content.ReadAsStringAsync().Result;


                memberResponse = JsonConvert.DeserializeObject<MemberResponse>(result);
               


            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return memberResponse;
        }
        public bool GetToken()
        {
            TokenResponse tokenResponse = new TokenResponse();
            try
            {
                var BaseUrl = General.GetSettingsValue("BaseUrl");
                var xApiKey = General.GetSettingsValue("xApiKey");
                var BasicAuthKey = General.GetSettingsValue("BasicAuthKey");
                

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic "+ BasicAuthKey);
                client.DefaultRequestHeaders.Add("x-api-key", xApiKey);

                HttpResponseMessage response = client.PostAsJsonAsync
                    ("share/dk-oauth/token?grant_type=client_credentials","").Result;
                response.EnsureSuccessStatusCode();

                string result = response.Content.ReadAsStringAsync().Result;


                tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(result);
                globalTokenResponse = tokenResponse;
                return true;

            }
            catch (Exception ex)
            {
                General.LogException(ex);
            }
            return false;
        }
    }
}
