﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Api.Gravity
{
    public class CancelBitRequest
    {

        public string h_bit_category { get; set; }
        public string h_bit_type { get; set; }
        public int h_program_id { get; set; }
        public int h_sponsor_id { get; set; }
        public string h_bit_date { get; set; }
        public string h_member_id { get; set; }
        public string cancel_bit_id { get; set; }
        
    }

    

}
