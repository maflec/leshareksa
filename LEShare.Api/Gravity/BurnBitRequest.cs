﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Api.Gravity
{
    public class BurnBitRequest
    {

        public string h_member_id { get; set; }
        public string h_bit_date { get; set; }
        public string h_bit_category { get; set; }
        public string h_bit_type { get; set; }
        public string h_location { get; set; }
        public string h_bit_source { get; set; }
        public float h_pay_in_amount { get; set; }
        public string h_bit_currency { get; set; }
        public string h_bit_source_generated_id { get; set; }
        public string h_pos_cashier_id { get; set; }
        public string h_pos_id { get; set; }
        public int h_program_id { get; set; }
        public int h_sponsor_id { get; set; }
        public List<line> lines { get; set; }
    }

    public class line
    {
        public string l_product_name { get; set; }
        public string l_product_description { get; set; }
        public string l_product_external_id { get; set; }
        public int l_quantity { get; set; }
        public float l_amount { get; set; }
    }

    

}
