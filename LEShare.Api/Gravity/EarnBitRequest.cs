﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Api.Gravity
{
    public class EarnBitRequest
    {
        public string h_bit_date { get; set; }
        public int h_sponsor_id { get; set; }
        public string h_bit_category { get; set; }
        public string h_location { get; set; }
        public string h_bit_source { get; set; }
        public string h_bit_type { get; set; }
        public float h_bit_amount { get; set; }
        public float h_pay_in_amount { get; set; }
        public float h_original_bit_amount { get; set; }
        public string h_bit_currency { get; set; }
        public int h_program_id { get; set; }
        public string h_member_id { get; set; }
        public string h_bit_source_generated_id { get; set; }
        public Payment_Details[] payment_details { get; set; }
        //public Line[] lines { get; set; }
        public List<line> lines { get; set; }
    }

    public class Payment_Details
    {
        public string payment_method { get; set; }
        public float amount { get; set; }
    }

    public class Line
    {
        public string Line_Id { get; set; }
        public int Receipt_No { get; set; }
        public int Sequence_Number { get; set; }
        public bool New_Card_Flag { get; set; }
        public string Card_Type { get; set; }
        public string Product_Category { get; set; }
        public string Product_Code { get; set; }
        public string Product_Name { get; set; }
        public float Unit_Price { get; set; }
        public float Tax { get; set; }
        public int Quantity { get; set; }
        public string Payment_Type { get; set; }
    }

}
