﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LEShare.Api.Gravity
{
    public class AdminBitResponse
    {
        public Original_Bit original_bit { get; set; }
        public Member member { get; set; }
        public Offer_Actions[] offer_actions { get; set; }
        public object[] assigned_privileges { get; set; }
        public object[] availed_privileges { get; set; }
        public Loyalty_Balances1[] loyalty_balances { get; set; }
        public DateTime processing_date { get; set; }
        public string bit_id { get; set; }
        public bool points_rewarded { get; set; }
        public bool points_redeemed { get; set; }
        public bool points_reset { get; set; }
        public string status { get; set; }
        public Error error { get; set; }
    }
    public class Error
    {
        public string code { get; set; }
        public string message { get; set; }

    }
    public class Original_Bit
    {
        public Header header { get; set; }
        public object[] lines { get; set; }
        public object[] payment_details { get; set; }
    }

    public class Header
    {
        public int h_sponsor_id { get; set; }
        public string h_member_id { get; set; }
        public string h_bit_type { get; set; }
        public string h_bit_source { get; set; }
        public string h_location { get; set; }
        public string h_pos_id { get; set; }
        public float h_pay_in_amount { get; set; }
        public string h_bit_date { get; set; }
        public string h_bit_category { get; set; }
        public int h_program_id { get; set; }
        public string h_bit_currency { get; set; }
        public string h_bit_source_generated_id { get; set; }
        public string h_pos_cashier_id { get; set; }
    }

    public class Member
    {
        public string version { get; set; }
        public string member_id { get; set; }
        public string joining_date { get; set; }
        public string last_bit_date { get; set; }
        public int sponsor_id { get; set; }
        public Attributes attributes { get; set; }
        public Multi_Value_Attributes multi_value_attributes { get; set; }
        public Mto mto { get; set; }
        public Loyalty_Balances loyalty_balances { get; set; }
        public Affiliated_Sponsors affiliated_sponsors { get; set; }
    }

    public class Attributes
    {
        public string member_type { get; set; }
        public string first_earn_date { get; set; }
        public string date_of_birth { get; set; }
        public string ety_ffp { get; set; }
        public string ety_link_status { get; set; }
        public string enrollment_channel { get; set; }
        public string enrolling_sponsor { get; set; }
        public string ety_date_linked { get; set; }
        public string family_designation { get; set; }
        public string enrollment_user { get; set; }
        public string family_head_points_share { get; set; }
        public string first_name { get; set; }
        public string last_accrual_date { get; set; }
        public DateTime last_redemption_date { get; set; }
        public string email { get; set; }
        public string member_id { get; set; }
        public string family_head { get; set; }
        public string program_opt_in { get; set; }
        public string mobile { get; set; }
        public string last_name { get; set; }
        public DateTime last_activity_date { get; set; }
        public string tier_class { get; set; }
        public string membership_stage { get; set; }
        public string auth_verified { get; set; }
        public string country_code { get; set; }
        public string ety_first_name { get; set; }
        public string nationality { get; set; }
        public string myclub_card_number { get; set; }
        public string ety_last_name { get; set; }
        public string date_of_joining { get; set; }
        public string salutation { get; set; }
        public string pseudo_tier { get; set; }
    }

    public class Multi_Value_Attributes
    {
    }

    public class Mto
    {
    }

    public class Loyalty_Balances
    {
        public float _1 { get; set; }
        public float _3 { get; set; }
    }

    public class Affiliated_Sponsors
    {
        public bool _1 { get; set; }
        public bool _12 { get; set; }
        public bool _2 { get; set; }
        public bool _3 { get; set; }
        public bool _75 { get; set; }
    }

    public class Offer_Actions
    {
        public string offer_id { get; set; }
        public string rule_id { get; set; }
        public string type { get; set; }
        public string subject { get; set; }
        public float value_n { get; set; }
        public object value_s { get; set; }
        public object _operator { get; set; }
        public int privilege_quantity { get; set; }
        public object attribute_type { get; set; }
        public string[] reward_tags { get; set; }
        public string sku { get; set; }
        public object[] privilege_codes { get; set; }
    }

    public class Loyalty_Balances1
    {
        public string loyalty_account_id { get; set; }
        public float redeemed { get; set; }
        public float old_balance { get; set; }
        public float new_balance { get; set; }
        public float rewarded { get; set; }
    }



}
