﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using LEShare.Api.Gravity;
using LEShare.Api.MPlanet;
using Newtonsoft.Json;

namespace LEShare.Api.Controllers
{
    //[ApiAuthorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ShareController : ControllerBase
    {
        private readonly IConfiguration configuration;
        bool loggGravityRequest;
        bool loggGravityResponse;
        public ShareController(IConfiguration configuration)
        {
            this.configuration = configuration;            
            loggGravityRequest = bool.Parse(configuration[$"{"Logging"}:{"loggGravityRequest"}"]);
            loggGravityResponse = bool.Parse(configuration[$"{"Logging"}:{"loggGravityResponse"}"]);
        }
        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        [HttpPost, Route("earn")]
        public ActionResponse Earn([FromBody]EarnRequest model)
         {
            ActionResponse response = new ActionResponse();
            try
            {
                //get location id from header
                MPlanetManager mpm = new MPlanetManager();

                var ApiToken = Request.Headers["ApiToken"].ToString();
               
                DataRow r = ((DataSet)mpm.GetLocation(ApiToken).data).Tables[0].Rows[0];
                
                var locationId = r["Share_Location_Id"].ToString();
                var sponsorId = r["Share_Sponsor_Id"].ToString();
                var programId = r["Share_Program_Id"].ToString();
                var currency = r["Currency"].ToString();
                var Location_Id= r["Location_Id"].ToString();



                //save record to database, earn tables, share table 
                ActionResponse aresp = mpm.SaveEarnRequest(model, locationId);
                if (aresp.Status == "SUCCESS")
                {
                    int id = Convert.ToInt32(aresp.data);

                    MPTransResponse resp = new MPTransResponse();
                    resp.MPTrans_id = id;

                    //share api call
                    float totalamount = 0;
                    Gravity.Payment_Details[] payment_DetailsArray =
                        new Gravity.Payment_Details[model.payments.Length];
                    for(int i=0;i< model.payments.Length; i++)
                    {
                        Gravity.Payment_Details payment_Details = new Gravity.Payment_Details
                        {
                            payment_method = model.payments[i].payment_method,
                            amount = model.payments[i].amount
                        };
                        totalamount += model.payments[i].amount;
                        payment_DetailsArray[i] = payment_Details;
                    }

                    Gravity.Line[] lineArray = new Gravity.Line[model.lines.Length];
                    List<line> lins = new List<line>();

                    for (int i = 0; i < model.lines.Length; i++)
                    {
                        Gravity.Line line = new Gravity.Line
                        {
                            Line_Id = model.lines[i].Earn_Line_Id,
                            Receipt_No = model.lines[i].Receipt_No,
                            Sequence_Number = model.lines[i].Sequence_Number,
                            New_Card_Flag = model.lines[i].New_Card_Flag,
                            Card_Type = model.lines[i].Card_Type,
                            Product_Category = model.lines[i].Product_Category,
                            Product_Code = model.lines[i].Product_Code,
                            Product_Name = model.lines[i].Product_Name,
                            Unit_Price = model.lines[i].Unit_Price,
                            Tax = model.lines[i].Tax,
                            Quantity = model.lines[i].Quantity,
                            Payment_Type = model.lines[i].Payment_Type

                        };

                        lineArray[i] = line;

                        line li = new line
                        {
                            l_product_name = model.lines[i].Product_Name,
                            l_product_external_id = model.lines[i].Product_Code,
                            l_quantity = model.lines[i].Quantity,
                            l_amount = model.lines[i].Unit_Price,
                        };
                        lins.Add(li);
                    }

                    EarnBitRequest adminBitRequest = new EarnBitRequest
                        {
                            h_bit_date=DateTime.Now.ToString("yyyy-MM-ddTHH:mm+04:00"),
                            h_sponsor_id=Convert.ToInt32(sponsorId),
                            h_bit_category= "ACCRUAL",
                            h_location = locationId,
                            h_bit_source = "POS",
                            h_bit_type = "SPEND",
                            h_bit_amount= model.Payment_Amount,
                            h_pay_in_amount=0,
                            h_original_bit_amount = totalamount, //newly added
                            h_bit_currency =currency,
                            h_program_id = Convert.ToInt32(programId),
                            h_member_id=model.Share_Id,
                            h_bit_source_generated_id
                            = Location_Id + "_"+model.Machine_Id.ToString()
                            + "_" + model.Receipt_No,
                            payment_details= payment_DetailsArray,
                            lines = lins
                    };
                    GravityManager gm = new GravityManager();

                    //log request to grvity
                    if (loggGravityRequest)
                    {
                       var req = JsonConvert.SerializeObject(adminBitRequest);
                        General.LogReqResp(JsonConvert.SerializeObject(adminBitRequest), "EARN REQUST");
                    }

                    if (gm.GetToken())
                    {
                        AdminBitResponse adminBitResponse = gm.RequestEarnBit(adminBitRequest);
                        if (adminBitResponse.status != null)
                        {

                            //log response from grvity
                            if (loggGravityResponse)
                            {

                                General.LogReqResp(JsonConvert.SerializeObject(adminBitResponse), "EARN RESPONSE");
                            }

                            if (adminBitResponse.status == "SUCCESS")
                            {
                                response.Status = "SUCCESS";
                                response.Message = "Request processed successfully";

                              
                                resp.bit_id = adminBitResponse.bit_id;
                                resp.rewarded = adminBitResponse.loyalty_balances[0].rewarded;
                                resp.redeemed = adminBitResponse.loyalty_balances[0].redeemed;
                                resp.old_balance = adminBitResponse.loyalty_balances[0].old_balance;
                                resp.new_balance = adminBitResponse.loyalty_balances[0].new_balance;

                                //prepare success response

                            }

                            //save response from grvity
                            mpm.UpdateShareRecord(adminBitResponse, id);
                        }
                        else
                        {
                            response.Status = "FAILURE";
                            response.Message = "Request failed";
                        }
                    }
                    else
                    {
                        response.Status = "FAILURE";
                        response.Message = "Gravity authentication  failed";
                    }

                    response.data = resp;

                }
                else
                {
                    response.Status = "FAILURE";
                    response.Message = "Requst failed";
                }


            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }

            //respond to api call
            return response;

        }

        [HttpPost, Route("burn")]
        public ActionResponse Burn([FromBody]BurnRequest model)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                //get location id from header
                MPlanetManager mpm = new MPlanetManager();

                var ApiToken = Request.Headers["ApiToken"].ToString();
               
                ActionResponse dbresp = mpm.GetLocation(ApiToken);
                if (dbresp.Status == "SUCCESS")
                {
                    DataRow r= ((DataSet)dbresp.data).Tables[0].Rows[0];
                    var locationId = r["Share_Location_Id"].ToString();
                    var sponsorId = r["Share_Sponsor_Id"].ToString();
                    var programId = r["Share_Program_Id"].ToString();
                    var currency = r["Currency"].ToString();
                    var region = r["Region"].ToString();
                    var Location_Id = r["Location_Id"].ToString();



                    //save record to database, earn tables, share table 

                    ActionResponse aresp = mpm.SaveBurnRequest(model, locationId);
                    if (aresp.Status == "SUCCESS")
                    {
                        int id = Convert.ToInt32(aresp.data);
                        MPTransResponse resp = new MPTransResponse();
                        resp.MPTrans_id = id;

                        //share api call
                        List<line> lins = new List<line>();
                        line li = new line
                        {
                            l_product_name = "MP POS Item",
                            l_product_external_id = id.ToString(),
                            l_quantity=1,
                            l_amount= model.Burn_Amount
                        };
                        lins.Add(li);

                        BurnBitRequest adminBitRequest = new BurnBitRequest
                        {
                            h_bit_date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm+04:00"),
                            h_sponsor_id = Convert.ToInt32(sponsorId),
                            h_bit_category = "ACCRUAL",
                            h_location = locationId,
                            h_bit_source = "POS",
                            h_bit_type = "PAYMENT_WITH_POINTS",
                            h_pos_cashier_id = model.Cashier_Id.ToString(),
                            h_pos_id = model.Machine_Id.ToString(),
                            h_pay_in_amount = model.Burn_Amount,
                            h_bit_currency = currency,
                            h_bit_source_generated_id = Guid.NewGuid().ToString(),
                            h_program_id = Convert.ToInt32(programId),
                            h_member_id = model.Share_Id,
                            lines = lins
                        };
                        GravityManager gm = new GravityManager();

                        //log request to grvity
                        if (loggGravityRequest)
                        {
                            var req = JsonConvert.SerializeObject(adminBitRequest);
                            General.LogReqResp(JsonConvert.SerializeObject(adminBitRequest), "BURN REQUST");
                        }

                        if (gm.GetToken())
                        {
                            AdminBitResponse adminBitResponse = gm.RequestBurnBit(adminBitRequest);
                            if (adminBitResponse.status != null)
                            {

                                //log response from grvity
                                if (loggGravityResponse)
                                {

                                    General.LogReqResp(JsonConvert.SerializeObject(adminBitResponse), "BURN RESPONSE");
                                }

                                if (adminBitResponse.status == "SUCCESS")
                                {
                                    response.Status = "SUCCESS";
                                    response.Message = "Request processed successfully";

                                    resp.bit_id = adminBitResponse.bit_id;
                                    resp.rewarded = adminBitResponse.loyalty_balances[0].rewarded;
                                    resp.redeemed = adminBitResponse.loyalty_balances[0].redeemed;
                                    resp.old_balance = adminBitResponse.loyalty_balances[0].old_balance;
                                    resp.new_balance = adminBitResponse.loyalty_balances[0].new_balance;

                                    //prepare success response

                                }
                                else
                                {
                                    response.Message = adminBitResponse.error.message;

                                }

                                //save response from grvity
                                mpm.UpdateShareRecord(adminBitResponse, id);
                            }
                            else
                            {
                                response.Status = "FAILURE";
                                response.Message = "Request failed";
                            }
                        }
                        else
                        {
                            response.Status = "FAILURE";
                            response.Message = "Gravity authentication  failed";
                        }

                        response.data = resp;
                    }
                    else
                    {
                        response.Status = "FAILURE";
                        response.Message = "Requst failed";
                    }
                }
                 

               


            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }

            //respond to api call
            return response;

        }

        [HttpPost, Route("update-burn")]
        public ActionResponse UpdateBurn([FromBody]UpdateBurnRequest model)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                //get location id from header
                MPlanetManager mpm = new MPlanetManager();


                //save record to database, earn tables, share table 

                ActionResponse aresp = mpm.UpdateBurn(model);
                if (aresp.Status == "SUCCESS")
                {
                    response.Status = "SUCCESS";
                    response.Message = "Request processed successfully";


                }
                else
                {
                    response.Status = "FAILURE";
                    response.Message = "Requst failed";
                }


            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }

            //respond to api call
            return response;

        }

        [HttpGet, Route("get-member")]
        public ActionResponse GetMember(string memberId)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                //Get Currency from LocationMaster Table- Start
                MPlanetManager mpm = new MPlanetManager();
                var ApiToken = Request.Headers["ApiToken"].ToString();
                DataRow r = ((DataSet)mpm.GetLocation(ApiToken).data).Tables[0].Rows[0];
                var region = r["Region"].ToString();
                //Get Currency from LocationMaster Table- End

                GravityManager gm = new GravityManager();

                if (gm.GetToken())
                {
                    MemberResponse memberResponse = gm.GetMemberDetails(memberId, region);
                    if (memberResponse!= null)
                    {   if(memberResponse.shareId != null)
                        {
                            response.Status = "SUCCESS";
                            response.Message = "Request processed successfully";
                            response.data = memberResponse;
                        }
                        else
                        {
                            response.Status = "FAILURE";
                            response.Message = "No member";
                        }
                       
                    }
                    else
                    {
                        response.Status = "FAILURE";
                        response.Message = "Request failed";
                    }
                }
                else
                {
                    response.Status = "FAILURE";
                    response.Message = "Gravity authentication  failed";
                }



            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }

            //respond to api call
            return response;

        }

        [HttpGet, Route("get-receipt")]
        public ActionResponse GetReceipt(int receiptNo)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                MPlanetManager mpm = new MPlanetManager();

                var ApiToken = Request.Headers["ApiToken"].ToString();

                DataRow r = ((DataSet)mpm.GetLocation(ApiToken).data).Tables[0].Rows[0];

                var locationId = r["Share_Location_Id"].ToString();
                var currency = r["currency"].ToString();

                ActionResponse receiptData= mpm.GetReceipt(locationId, receiptNo);
                response.Status = receiptData.Status;
                response.currency = currency;
                if (receiptData.Status == "SUCCESS")
                {
                    response.data = receiptData.data;
                }
                else
                {
                    response.Message = "Could not load receipt";
                }
                


            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }

            //respond to api call
            return response;

        }

        [HttpPost, Route("refund")]
        public ActionResponse Refund([FromBody]RefundRequest model)
        {
            ActionResponse response = new ActionResponse();
            try
            {
                //get location id from header
                MPlanetManager mpm = new MPlanetManager();

                var ApiToken = Request.Headers["ApiToken"].ToString();

                DataRow r = ((DataSet)mpm.GetLocation(ApiToken).data).Tables[0].Rows[0];

                var locationId = r["Share_Location_Id"].ToString();
                var sponsorId = r["Share_Sponsor_Id"].ToString();
                var programId = r["Share_Program_Id"].ToString();
                var currency = r["Currency"].ToString();
                var Location_Id = r["Location_Id"].ToString();

                //save record to database, earn tables, share table 

                ActionResponse aresp = mpm.SaveRefundRequest(model, locationId);
                if (aresp.Status == "SUCCESS")
                {
                    int id = Convert.ToInt32(aresp.data);
                    MPTransResponse resp = new MPTransResponse();
                    resp.MPTrans_id = id;

                    //get bit_id using receipt number

                    ActionResponse aResponse= mpm.GetReceipt(locationId, model.Receipt_No);
                    GravityManager gm = new GravityManager();
                    if (aResponse.Status == "SUCCESS")
                    {
                        ReceiptResponse rResponse = (ReceiptResponse)aResponse.data;
                        if (gm.GetToken())
                        {
                            CancelBitRequest adminBitRequest;
                            AdminBitResponse adminBitResponse;
                            bool success1 = false;
                            bool success2 = false;

                            response.Message = string.Empty;
                            resp.bit_id = string.Empty;
                            if (rResponse.earnBitId != null)
                            {
                                adminBitRequest = new CancelBitRequest
                                {
                                    h_bit_date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm+04:00"),
                                    h_sponsor_id = Convert.ToInt32(sponsorId),
                                    h_bit_category = "CANCELLATION",
                                    h_bit_type = "REVERSAL",
                                    cancel_bit_id = rResponse.earnBitId,
                                    h_program_id = Convert.ToInt32(programId),
                                    h_member_id = rResponse.MemberId
                                };

                                if (loggGravityRequest)
                                {
                                    var req = JsonConvert.SerializeObject(adminBitRequest);
                                    General.LogReqResp(JsonConvert.SerializeObject(adminBitRequest), "CANCEL REQUST1");
                                }
                                adminBitResponse = gm.RequestCancelBit(adminBitRequest);

                                if (loggGravityResponse)
                                {

                                    General.LogReqResp(JsonConvert.SerializeObject(adminBitResponse), "CANCEL RESPONSE1");
                                }

                                if (adminBitResponse.status == "SUCCESS")
                                {
                                    success1 = true;
                                    response.Message += ", Earn cancelled";
                                    resp.bit_id += adminBitResponse.bit_id;
                                    //update refund status agains earn 
                                    mpm.UpdateRefund(id, model.Receipt_No, adminBitResponse, rResponse.earnBitId,locationId);
                                }
                                else
                                {
                                    response.Message += "," + adminBitResponse.error.message;
                                }
                            }
                            else
                            {
                                success1 = true;
                                goto NextBit;

                            }

                            NextBit:
                            if (rResponse.burnBitId != null)
                            {
                                adminBitRequest = new CancelBitRequest
                                {
                                    h_bit_date = DateTime.Now.ToString("yyyy-MM-ddTHH:mm+04:00"),
                                    h_sponsor_id = Convert.ToInt32(sponsorId),
                                    h_bit_category = "CANCELLATION",
                                    h_bit_type = "REVERSAL",
                                    cancel_bit_id = rResponse.burnBitId,
                                    h_program_id = Convert.ToInt32(programId),
                                    h_member_id = rResponse.MemberId
                                };

                                if (loggGravityRequest)
                                {
                                    var req = JsonConvert.SerializeObject(adminBitRequest);
                                    General.LogReqResp(JsonConvert.SerializeObject(adminBitRequest), "CANCEL REQUST2");
                                }
                                adminBitResponse = gm.RequestCancelBit(adminBitRequest);

                                if (loggGravityResponse)
                                {

                                    General.LogReqResp(JsonConvert.SerializeObject(adminBitResponse), "CANCEL RESPONSE2");
                                }

                                if (adminBitResponse.status == "SUCCESS")
                                {
                                    success2 = true;
                                    response.Message += ", Burn cancelled";
                                    resp.bit_id += ","+adminBitResponse.bit_id;
                                    //update refund status agains burn 
                                    mpm.UpdateRefund(id, model.Receipt_No, adminBitResponse, rResponse.burnBitId, locationId);
                                }
                                else
                                {
                                    response.Message += ","+adminBitResponse.error.message;
                                }
                            }
                            else
                            {
                                success2 = true;
                            }

                            if(success1 && success2)
                            {
                                response.Status = "SUCCESS";
                                response.Message = "Request processed successfully, "+ response.Message;//combines message

                               
                               
                            }

                        }
                        else
                        {
                            response.Status = "FAILURE";
                            response.Message = "Gravity authentication  failed";
                        }
                    }
                    else
                    {
                        response.Message = "Could not load receipt";
                    }




                    response.data = resp;

                }
                else
                {
                    response.Status = "FAILURE";
                    response.Message = "Requst failed";
                }


            }
            catch (Exception ex)
            {
                response.Status = "EXCEPTION";
                response.Message = "Something went wrong";
                General.LogException(ex);
            }

            //respond to api call
            return response;

        }

        //function to update burn record with receipt
    }
}
