﻿
// Custom authorization with token. written by ABDUL NASSAR
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Http;
public class ApiAuthorize : AuthorizeAttribute, IAuthorizationFilter
{
    public  void OnAuthorization(AuthorizationFilterContext filterContext)
    {
        if (Authorize(filterContext))
        {
            return;
        }
       
    }   

    private bool Authorize(AuthorizationFilterContext filterContext)
    { 
        bool returnValue = false;
        try
        {

            //exception tester
            //int a = 0;
            //int b = 2 / a;

            var apitoken = filterContext.HttpContext.Request.Headers["apitoken"];
           
            SqlHelper sqlHelper = new SqlHelper();
           
            sqlHelper.AddSetParameterToSQLCommand("@ApiToken", SqlDbType.NVarChar, apitoken);

            DataSet ds = sqlHelper.GetDatasetByCommand("SP_ValidateToken");

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    if (ds.Tables[0].Rows[0][0].ToString().ToUpper() == "SUCCESS")
                    {
                        returnValue = true;
                    }

                }

            }
        }
        catch (Exception ex)
        {
            General.LogException(ex);

        }
        return returnValue;

    }
}


