﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

public class General
{
    public static string passEncKey = "dcd90e2b34dfd343d58c002c7af0bc";
    public static void LogException(Exception ex)
    {
        try
        {

            
            string FileName = string.Format(
                @"{0}\Logs\error_" + DateTime.Today.ToString("yyyMMdd") + ".txt"
                , LEShare.Api.Startup.contentRoot);
            if (!CheckIfLogFileExists(FileName))//log file not exists
            {
                CreateLogFile(FileName); //create log file
            }
            if (CheckIfLogFileExists(FileName))
            {
                SaveException(FileName, ex);
            }

        }
        catch
        {
        }
    }

    public static void LogReqResp(string content, string type)
    {
        try
        {
            string FileName = string.Format(
               @"{0}\Logs\reqresp_" + DateTime.Today.ToString("yyyMMdd") + ".txt"
               , LEShare.Api.Startup.contentRoot);
           
            if (!CheckIfLogFileExists(FileName))//log file not exists
            {
                CreateLogFile(FileName); //create log file
            }
            if (CheckIfLogFileExists(FileName))
            {
                SaveReqResp(FileName, content,type);
            }

        }
        catch
        {
        }
    }
    private static bool SaveReqResp(string fileName, string content, string type)
    {
        try
        {
            using (StreamWriter sw = File.AppendText(fileName))
            {
                sw.WriteLine(DateTime.Now.ToString());

                StringBuilder s = new StringBuilder();

                sw.WriteLine("type:"+type+ "\n"+"DateTime: "+DateTime.Now.ToString()
                    +"\nLog: " + content);

                sw.WriteLine();
                sw.WriteLine();

            }
            return true;
        }
        catch
        {
        }
        return false;
    }
    private static bool SaveException(string fileName, Exception ex)
    {
        try
        {
            using (StreamWriter sw = File.AppendText(fileName))
            {
                sw.WriteLine(DateTime.Now.ToString());
                sw.WriteLine("ex: " + ex.ToString());
                sw.WriteLine("ex.Message: " + ex.Message);
                sw.WriteLine("ex.StackTrace: " + ex.StackTrace);
                sw.WriteLine("ex.Source: " + ex.Source);
                StringBuilder s = new StringBuilder();
                while (ex != null)
                {
                    s.AppendLine("Exception type: " + ex.GetType().FullName);
                    s.AppendLine("Message       : " + ex.Message);
                    s.AppendLine("Stacktrace:");
                    s.AppendLine(ex.StackTrace);
                    s.AppendLine();
                    ex = ex.InnerException;
                }
                sw.WriteLine("innerexceptions: " + s);

                sw.WriteLine();
                sw.WriteLine();

            }
            return true;
        }
        catch
        {
        }
        return false;
    }
    private static bool CreateLogFile(string fileName)
    {
        try
        {
            File.CreateText(fileName).Close();
            return true;
        }
        catch (Exception ex)
        {
        }
        return false;
    }
    private static bool CheckIfLogFileExists(string fileName)
    {
        try
        {
            return File.Exists(fileName);

        }
        catch
        {
        }
        return false;
    }

    public static string GetSettingsValue(string key)
    {
        try
        {
            SqlHelper sqlHelper = new SqlHelper();
            sqlHelper.AddSetParameterToSQLCommand("@settingsName", SqlDbType.VarChar, key);
            DataSet ds = sqlHelper.GetDatasetByCommand("SP_GetSettingsValue");


            if (ds.Tables.Count > 0)
            {

                return ds.Tables[0].Rows[0]["settingsValue"].ToString();
            }
            return null;
        }
        catch (Exception ex)
        {
            // General.LogException(ex);
            return null;
        }
    }

    public static string SetSettingsValue(string key, string value)
    {
        try
        {
            SqlHelper sqlHelper = new SqlHelper();

            sqlHelper.AddSetParameterToSQLCommand("@SettingsName", SqlDbType.VarChar, key);
            sqlHelper.AddSetParameterToSQLCommand("@SettingsValue", SqlDbType.VarChar, value);
            sqlHelper.GetExecuteNonQueryByCommand("SP_SetSettingsValue");



            return "success";
        }
        catch (Exception ex)
        {
            // General.LogException(ex);

            return null;
        }
    }

    public static string Encrypt(string text, string key)
    {
        var _key = Encoding.UTF8.GetBytes(key);

        using (var aes = Aes.Create())
        {
            using (var encryptor = aes.CreateEncryptor(_key, aes.IV))
            {
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        using (var sw = new StreamWriter(cs))
                        {
                            sw.Write(text);
                        }
                    }

                    var iv = aes.IV;

                    var encrypted = ms.ToArray();

                    var result = new byte[iv.Length + encrypted.Length];

                    Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                    Buffer.BlockCopy(encrypted, 0, result, iv.Length, encrypted.Length);

                    return Convert.ToBase64String(result);
                }
            }
        }
    }

    public static string Decrypt(string encrypted, string key)
    {
        var b = Convert.FromBase64String(encrypted);

        var iv = new byte[16];
        var cipher = new byte[16];

        Buffer.BlockCopy(b, 0, iv, 0, iv.Length);
        Buffer.BlockCopy(b, iv.Length, cipher, 0, iv.Length);

        var _key = Encoding.UTF8.GetBytes(key);

        using (var aes = Aes.Create())
        {
            using (var decryptor = aes.CreateDecryptor(_key, iv))
            {
                var result = string.Empty;
                using (var ms = new MemoryStream(cipher))
                {
                    using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        using (var sr = new StreamReader(cs))
                        {
                            result = sr.ReadToEnd();
                        }
                    }
                }

                return result;
            }
        }
    }
}